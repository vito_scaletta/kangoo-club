-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 19 2018 г., 12:39
-- Версия сервера: 5.7.20
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kangoo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `as_articles`
--

CREATE TABLE `as_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_articles`
--

INSERT INTO `as_articles` (`id`, `slug`, `image`, `visible`, `created_at`, `updated_at`) VALUES
(3, 'testovaya-statya-1', '1526245888_stockitem.jpg', 1, '2018-05-13 18:11:28', '2018-05-13 18:11:55');

-- --------------------------------------------------------

--
-- Структура таблицы `as_articles_localization`
--

CREATE TABLE `as_articles_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annotation` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_articles_localization`
--

INSERT INTO `as_articles_localization` (`id`, `post_id`, `lang`, `name`, `h1`, `annotation`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(7, 3, 'ru', 'Тестовая статья #1', 'Тестовый заголовок H1 статьи #1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>', 'Тестовая статья #1', '', '', '2018-05-13 18:11:28', '2018-05-13 18:11:28'),
(8, 3, 'ua', 'Тестова стаття #1', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>', 'Тестова стаття #1', '', '', '2018-05-13 18:11:28', '2018-05-13 18:13:39'),
(9, 3, 'en', 'Test article #1', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>', 'Test article #1', '', '', '2018-05-13 18:11:28', '2018-05-13 18:14:08');

-- --------------------------------------------------------

--
-- Структура таблицы `as_certificates`
--

CREATE TABLE `as_certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_certificates`
--

INSERT INTO `as_certificates` (`id`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(8, '1526568550_bebelya-prilozhenie.jpg', 1, 1, '2018-05-17 11:49:11', '2018-05-17 12:02:57'),
(9, '1526568551_uralskaya-prilozhenie.jpg', 1, 2, '2018-05-17 11:49:11', '2018-05-17 12:02:57');

-- --------------------------------------------------------

--
-- Структура таблицы `as_certificates_localization`
--

CREATE TABLE `as_certificates_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `certificate_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_certificates_localization`
--

INSERT INTO `as_certificates_localization` (`id`, `certificate_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(22, 8, 'ru', 'Сертификат #1', '2018-05-17 11:49:11', '2018-05-17 11:49:57'),
(23, 8, 'ua', 'Сертификат #1', '2018-05-17 11:49:11', '2018-05-17 11:49:57'),
(24, 8, 'en', 'Certificate #1', '2018-05-17 11:49:11', '2018-05-17 11:49:57'),
(25, 9, 'ru', 'Сертификат #3', '2018-05-17 11:49:11', '2018-05-17 11:50:55'),
(26, 9, 'ua', 'Сертификат #3', '2018-05-17 11:49:11', '2018-05-17 11:50:55'),
(27, 9, 'en', 'Certificate #3', '2018-05-17 11:49:11', '2018-05-17 11:50:55');

-- --------------------------------------------------------

--
-- Структура таблицы `as_feedbacks`
--

CREATE TABLE `as_feedbacks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_feedbacks`
--

INSERT INTO `as_feedbacks` (`id`, `name`, `phone`, `visible`, `created_at`, `updated_at`) VALUES
(1, '111', '+38 (232) 323-23-23', 0, '2018-05-21 05:22:15', '2018-05-21 05:22:15');

-- --------------------------------------------------------

--
-- Структура таблицы `as_languages`
--

CREATE TABLE `as_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_languages`
--

INSERT INTO `as_languages` (`id`, `code`, `name`, `alias`, `visible`, `created_at`, `updated_at`) VALUES
(1, 'ru', 'Русский', 'rus', 1, NULL, '2018-05-22 10:44:10'),
(2, 'ua', 'Українська', 'ukr', 1, NULL, '2018-05-22 10:44:10'),
(3, 'en', 'English', 'eng', 1, NULL, '2018-05-22 10:44:10');

-- --------------------------------------------------------

--
-- Структура таблицы `as_main_slider`
--

CREATE TABLE `as_main_slider` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_main_slider`
--

INSERT INTO `as_main_slider` (`id`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(14, '1526068102_hslideritem.jpg', 1, 1, '2018-05-11 16:48:22', '2018-05-14 10:23:06'),
(17, '1526068680_serviceitem1.jpg', 1, 2, '2018-05-11 16:58:00', '2018-05-14 10:23:06'),
(18, '1526068680_serviceitem2.jpg', 1, 3, '2018-05-11 16:58:00', '2018-05-11 16:59:00');

-- --------------------------------------------------------

--
-- Структура таблицы `as_menu`
--

CREATE TABLE `as_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_menu`
--

INSERT INTO `as_menu` (`id`, `parent_id`, `lft`, `rgt`, `depth`, `slug`, `visible`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 2, 0, '', 1, '2018-05-10 16:39:12', '2018-05-10 16:41:35'),
(3, NULL, 3, 4, 0, 'services', 1, '2018-05-10 16:40:54', '2018-05-11 16:25:22'),
(4, NULL, 7, 22, 0, 'about', 1, '2018-05-10 16:41:27', '2018-05-21 10:08:17'),
(5, 4, 8, 9, 1, 'team', 1, '2018-05-10 16:42:18', '2018-05-11 16:25:19'),
(6, 4, 10, 11, 1, 'reviews', 1, '2018-05-10 16:43:47', '2018-05-11 16:25:19'),
(7, 4, 12, 13, 1, 'news', 1, '2018-05-10 16:45:15', '2018-05-11 16:25:19'),
(8, 4, 14, 15, 1, 'articles', 1, '2018-05-10 16:46:04', '2018-05-11 16:25:19'),
(9, 4, 16, 17, 1, 'partners', 1, '2018-05-10 16:46:55', '2018-05-17 10:08:19'),
(10, 4, 18, 19, 1, 'certificates', 1, '2018-05-10 16:48:34', '2018-05-17 10:42:31'),
(11, NULL, 23, 24, 0, 'photo-gallery', 1, '2018-05-10 18:25:14', '2018-05-21 10:08:17'),
(12, NULL, 25, 26, 0, 'video-gallery', 1, '2018-05-10 18:26:25', '2018-05-21 10:08:17'),
(13, NULL, 27, 28, 0, 'stock', 1, '2018-05-10 18:27:21', '2018-05-21 10:08:17'),
(14, NULL, 29, 30, 0, 'price', 1, '2018-05-10 18:28:11', '2018-05-21 10:08:17'),
(15, NULL, 31, 32, 0, 'contacts', 1, '2018-05-10 18:29:11', '2018-05-21 10:08:17'),
(16, NULL, 5, 6, 0, 'products', 1, '2018-05-11 07:08:18', '2018-05-11 07:08:30'),
(17, 4, 20, 21, 1, 'schedule', 1, '2018-05-21 10:08:17', '2018-05-21 10:08:17');

-- --------------------------------------------------------

--
-- Структура таблицы `as_menu_localization`
--

CREATE TABLE `as_menu_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_menu_localization`
--

INSERT INTO `as_menu_localization` (`id`, `item_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ru', 'Главная', '2018-05-10 16:39:12', '2018-05-10 16:39:12'),
(2, 1, 'ua', 'Головна', '2018-05-10 16:39:12', '2018-05-10 16:39:12'),
(3, 1, 'en', 'Home', '2018-05-10 16:39:12', '2018-05-10 16:39:12'),
(7, 3, 'ru', 'Услуги', '2018-05-10 16:40:54', '2018-05-10 16:40:54'),
(8, 3, 'ua', 'Послуги', '2018-05-10 16:40:54', '2018-05-10 16:40:54'),
(9, 3, 'en', 'Services', '2018-05-10 16:40:54', '2018-05-10 16:40:54'),
(10, 4, 'ru', 'О компании', '2018-05-10 16:41:27', '2018-05-10 16:41:27'),
(11, 4, 'ua', 'Про компанію', '2018-05-10 16:41:27', '2018-05-10 16:41:27'),
(12, 4, 'en', 'About company', '2018-05-10 16:41:27', '2018-05-10 16:41:27'),
(13, 5, 'ru', 'Наша команда', '2018-05-10 16:42:18', '2018-05-10 16:42:18'),
(14, 5, 'ua', 'Наша команда', '2018-05-10 16:42:18', '2018-05-10 16:42:18'),
(15, 5, 'en', 'Our team', '2018-05-10 16:42:18', '2018-05-10 16:42:18'),
(16, 6, 'ru', 'Отзывы', '2018-05-10 16:43:47', '2018-05-10 16:43:47'),
(17, 6, 'ua', 'Відгуки', '2018-05-10 16:43:47', '2018-05-10 16:43:47'),
(18, 6, 'en', 'Reviews', '2018-05-10 16:43:47', '2018-05-10 16:43:47'),
(19, 7, 'ru', 'Новости', '2018-05-10 16:45:15', '2018-05-10 16:45:15'),
(20, 7, 'ua', 'Новини', '2018-05-10 16:45:15', '2018-05-10 16:45:15'),
(21, 7, 'en', 'News', '2018-05-10 16:45:15', '2018-05-10 16:45:15'),
(22, 8, 'ru', 'Статьи', '2018-05-10 16:46:04', '2018-05-10 16:46:04'),
(23, 8, 'ua', 'Статті', '2018-05-10 16:46:04', '2018-05-10 16:46:04'),
(24, 8, 'en', 'Articles', '2018-05-10 16:46:04', '2018-05-10 16:46:04'),
(25, 9, 'ru', 'Партнёры', '2018-05-10 16:46:55', '2018-05-10 16:46:55'),
(26, 9, 'ua', 'Партнери', '2018-05-10 16:46:55', '2018-05-10 16:46:55'),
(27, 9, 'en', 'Partners', '2018-05-10 16:46:55', '2018-05-10 16:46:55'),
(28, 10, 'ru', 'Лицензии', '2018-05-10 16:48:35', '2018-05-10 16:48:35'),
(29, 10, 'ua', 'Ліцензії', '2018-05-10 16:48:35', '2018-05-10 16:48:35'),
(30, 10, 'en', 'Licenses', '2018-05-10 16:48:35', '2018-05-10 16:48:35'),
(31, 11, 'ru', 'Фотогалерея', '2018-05-10 18:25:14', '2018-05-10 18:25:14'),
(32, 11, 'ua', 'Фотогалерея', '2018-05-10 18:25:14', '2018-05-10 18:25:14'),
(33, 11, 'en', 'Photo gallery', '2018-05-10 18:25:14', '2018-05-10 18:25:14'),
(34, 12, 'ru', 'Видеогалерея', '2018-05-10 18:26:25', '2018-05-10 18:26:25'),
(35, 12, 'ua', 'Відеогалерея', '2018-05-10 18:26:25', '2018-05-10 18:26:25'),
(36, 12, 'en', 'Video gallery', '2018-05-10 18:26:25', '2018-05-10 18:26:25'),
(37, 13, 'ru', 'Акции', '2018-05-10 18:27:21', '2018-05-10 18:27:21'),
(38, 13, 'ua', 'Акції', '2018-05-10 18:27:21', '2018-05-10 18:27:21'),
(39, 13, 'en', 'Stock', '2018-05-10 18:27:21', '2018-05-10 18:27:21'),
(40, 14, 'ru', 'Цены', '2018-05-10 18:28:11', '2018-05-10 18:28:11'),
(41, 14, 'ua', 'Ціни', '2018-05-10 18:28:11', '2018-05-10 18:28:11'),
(42, 14, 'en', 'Price', '2018-05-10 18:28:11', '2018-05-10 18:28:11'),
(43, 15, 'ru', 'Контакты', '2018-05-10 18:29:11', '2018-05-10 18:29:11'),
(44, 15, 'ua', 'Контакти', '2018-05-10 18:29:11', '2018-05-10 18:29:11'),
(45, 15, 'en', 'Contacts', '2018-05-10 18:29:11', '2018-05-10 18:29:11'),
(46, 16, 'ru', 'Товары', '2018-05-11 07:08:18', '2018-05-11 07:08:18'),
(47, 16, 'ua', 'Товари', '2018-05-11 07:08:18', '2018-05-11 07:08:18'),
(48, 16, 'en', 'Products', '2018-05-11 07:08:18', '2018-05-11 07:08:18'),
(49, 17, 'ru', 'Расписание', '2018-05-21 10:08:17', '2018-05-21 10:08:17'),
(50, 17, 'ua', 'Розклад', '2018-05-21 10:08:17', '2018-05-21 10:08:17'),
(51, 17, 'en', 'Schedule', '2018-05-21 10:08:17', '2018-05-21 10:08:17');

-- --------------------------------------------------------

--
-- Структура таблицы `as_news`
--

CREATE TABLE `as_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_news`
--

INSERT INTO `as_news` (`id`, `slug`, `image`, `date`, `visible`, `created_at`, `updated_at`) VALUES
(4, 'testovaya-novost-1', '1526238190_postitem.jpg', '2018-05-13 19:03:10', 1, '2018-05-13 15:53:47', '2018-05-13 16:03:11'),
(5, 'testovaya-novost-2', '1526238274_galleryitem2.jpg', '2018-05-02 19:05:09', 1, '2018-05-13 16:04:34', '2018-05-13 16:05:09'),
(6, 'testovaya-novost-3', '1526238446_galleryitem1.jpg', '2018-05-23 19:08:28', 1, '2018-05-13 16:07:26', '2018-05-13 16:08:28'),
(7, 'testovaya-novost-4', '', '2018-05-13 19:09:56', 1, '2018-05-13 16:09:56', '2018-05-13 16:09:56');

-- --------------------------------------------------------

--
-- Структура таблицы `as_news_localization`
--

CREATE TABLE `as_news_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annotation` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_news_localization`
--

INSERT INTO `as_news_localization` (`id`, `post_id`, `lang`, `name`, `h1`, `annotation`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(10, 4, 'ru', 'Тестовая новость #1', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Тестовая новость #1', '', '', '2018-05-13 15:53:47', '2018-05-13 15:55:04'),
(11, 4, 'ua', 'Тестова новына #1', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Тестова новына #1', '', '', '2018-05-13 15:53:47', '2018-05-13 15:55:04'),
(12, 4, 'en', 'Test news #1', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Test news #1', '', '', '2018-05-13 15:53:47', '2018-05-13 15:55:04'),
(13, 5, 'ru', 'Тестовая новость #2', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Тестовая новость #2', '', '', '2018-05-13 16:04:34', '2018-05-13 16:05:09'),
(14, 5, 'ua', 'Тестова новина #2', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Тестова новина #2', '', '', '2018-05-13 16:04:34', '2018-05-13 16:05:09'),
(15, 5, 'en', 'Test news #2', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Test news #2', '', '', '2018-05-13 16:04:34', '2018-05-13 16:05:09'),
(16, 6, 'ru', 'Тестовая новость #3', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Тестовая новость #3', '', '', '2018-05-13 16:07:26', '2018-05-13 16:08:28'),
(17, 6, 'ua', 'Тестова новина #3', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Тестова новина #3', '', '', '2018-05-13 16:07:26', '2018-05-13 16:08:28'),
(18, 6, 'en', 'Test news #3', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris</p>', 'Test news #3', '', '', '2018-05-13 16:07:26', '2018-05-13 16:08:28'),
(19, 7, 'ru', 'Тестовая новость #4', '', '', '', 'Тестовая новость #4', '', '', '2018-05-13 16:09:56', '2018-05-13 16:09:56'),
(20, 7, 'ua', 'Тестова новина #4', '', '', '', 'Тестова новина #4', '', '', '2018-05-13 16:09:56', '2018-05-13 16:09:56'),
(21, 7, 'en', 'Test news #4', '', '', '', 'Test news #4', '', '', '2018-05-13 16:09:56', '2018-05-13 16:09:56');

-- --------------------------------------------------------

--
-- Структура таблицы `as_pages`
--

CREATE TABLE `as_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_pages`
--

INSERT INTO `as_pages` (`id`, `slug`, `created_at`, `updated_at`) VALUES
(2, '', '2018-05-10 15:54:11', '2018-05-10 15:54:11'),
(3, '404', '2018-05-10 15:57:29', '2018-05-10 15:57:29'),
(4, 'services', '2018-05-12 07:34:13', '2018-05-12 07:34:19'),
(5, 'products', '2018-05-13 08:17:20', '2018-05-13 08:17:20'),
(6, 'news', '2018-05-13 15:28:36', '2018-05-13 15:28:36'),
(7, 'articles', '2018-05-13 18:07:50', '2018-05-13 18:07:50'),
(8, 'price', '2018-05-13 18:26:22', '2018-05-13 18:26:22'),
(9, 'contacts', '2018-05-13 18:40:38', '2018-05-13 18:40:38'),
(10, 'about', '2018-05-13 18:52:39', '2018-05-13 18:52:39'),
(11, 'reviews', '2018-05-14 05:06:31', '2018-05-14 05:06:31'),
(12, 'team', '2018-05-14 08:33:24', '2018-05-14 08:33:24'),
(13, 'stock', '2018-05-14 10:33:34', '2018-05-14 10:33:34'),
(14, 'photo-gallery', '2018-05-14 20:14:13', '2018-05-14 20:14:13'),
(15, 'video-gallery', '2018-05-17 07:23:19', '2018-05-17 07:23:19'),
(16, 'partners', '2018-05-17 08:31:54', '2018-05-17 08:31:54'),
(17, 'certificates', '2018-05-17 10:47:28', '2018-05-17 10:47:28'),
(18, 'sitemap', '2018-05-18 05:49:25', '2018-05-18 05:49:25'),
(19, 'search', '2018-05-18 05:52:01', '2018-05-18 05:52:01'),
(20, 'schedule', '2018-05-21 07:35:40', '2018-05-21 07:35:40');

-- --------------------------------------------------------

--
-- Структура таблицы `as_pages_localization`
--

CREATE TABLE `as_pages_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_pages_localization`
--

INSERT INTO `as_pages_localization` (`id`, `page_id`, `lang`, `name`, `h1`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(4, 2, 'ru', 'Главная', '', '<h1>Kangoo jumps club в Запорожье</h1>\r\n<p>\"Быстрее, выше, сильнее!\" - девиз возрождённых из небытия в начале прошлого века Олимпийских игр не утратил своей актуальности и в наши дни. Благодаря новым видам спорта этот олимпийский девиз приобретает другое, более современное звучание. Kangoo Jumps привлекают в свои ряды всё больше поклонников спорта, здорового образа жизни и... экстрима.</p>\r\n<p>Научиться ходить, бегать и совершать прыжки в обуви Kangoo Jumps можно довольно быстро. Обычно первая тренировка занимает всего несколько минут. Утром после тренировки встать с кровати очень тяжело, всё тело ужасно болит, а передвигаться получается с трудом. Но, испытав однажды волшебное чувство полёта, от Kangoo Jumps уже просто невозможно отказаться.</p>\r\n<p>По всему миру набирает популярность новый вид фитнеса под названием \"Kangoo jumping\". Данная система гарантирует сжигание в два раза большего количества калорий, чем при беге. При этом во время тренировки суставы меньше нагружаются за счет использования особого приспособления, напоминающего лыжный ботинок, к подошве которого прикреплена овальная пружинистая часть. Итак, посетителям занятия по \"Kangoo jumping\" предлагается для начала надеть эти ботинки, а затем приготовиться к интенсивной 45-минутной кардио-тренировке, которая совмещает в себе танцевальные движения, бег и прыжки по залу под ритмичную музыку. Разработчики системы заявляют: помимо проработки мышц ног, тренировки затрагивают все основные мышцы. Кстати, данные чудо-ботинки NASA использовало для тренировки космонавтов. Но, чтобы воспользоваться ими, никакой дополнительной одготовки не требуется. По словам экспертов, ботинки очень устойчивые, поэтому равновесие потерять трудно и в ходе их практики еще никто не падал.</p>', 'Главная', '', '', '2018-05-10 15:54:11', '2018-05-10 15:57:00'),
(5, 2, 'ua', 'Головна', '', '<h1>Kangoo jumps club у Запоріжжі</h1>\r\n<p>\"Швидше вище сильніше!\" - девіз відроджених з небуття на початку минулого століття Олімпійських ігор не втратив своєї актуальності і в наші дні. Завдяки новим видам спорту цей олімпійський девіз набуває іншого, більш сучасне звучання. Kangoo Jumps залучають до своїх лав все більше шанувальників спорту, здорового способу життя і ... екстриму.</p>\r\n<p>Навчитися ходити, бігати і здійснювати стрибки у взутті Kangoo Jumps можна досить швидко. Зазвичай перше тренування займає всього кілька хвилин. Вранці після тренування встати з ліжка дуже важко, все тіло страшенно болить, а пересуватися виходить з працею. Але, відчувши одного разу чарівне відчуття польоту, від Kangoo Jumps вже просто неможливо відмовитися.</p>\r\n<p>По всьому світу набирає популярність новий вид фітнесу під назвою \"Kangoo jumping\". Дана система гарантує спалювання в два рази більшої кількості калорій, ніж при бігу. При цьому під час тренування суглоби менше навантажуються за рахунок використання особливого пристосування, що нагадує лижний черевик, до підошви якого прикріплена овальна пружиниста частина. Отже, відвідувачам заняття по \"Kangoo jumping\" пропонується для початку надіти ці черевики, а потім приготуватися до інтенсивної 45-хвилинної кардіо-тренування, яка поєднує в собі танцювальні рухи, біг і стрибки по залу під ритмічну музику. Розробники системи заявляють: крім опрацювання м\'язів ніг, тренування зачіпають всі основні м\'язи. До речі, дані диво-черевики NASA використовувало для тренування космонавтів. Але, щоб скористатися ними, ніякої додаткової одготовкі не потрібно. За словами експертів, черевики дуже стійкі, тому рівновага втратити важко і в ході їх практики ще ніхто не падав.</p>', 'Головна', '', '', '2018-05-10 15:54:11', '2018-05-10 15:57:00'),
(6, 2, 'en', 'Home', '', '<h1>Kangoo jumps club in Zaporozhye</h1>\r\n<p>\"Faster, higher, stronger!\" - the motto of the Olympic Games, revived from non-existence at the beginning of the last century, has not lost its relevance in our days. Thanks to new sports, this Olympic motto acquires a different, more modern sound. Kangoo Jumps attracts in its ranks more fans of sports, a healthy lifestyle and ... extreme.</p>\r\n<p>Learn how to walk, run and jump in shoes Kangoo Jumps can be pretty quickly. Usually the first training takes only a few minutes. In the morning after training to get out of bed it\'s very hard, the whole body is awfully sore, and it\'s hard to move around. But having once experienced a magical flight experience, it\'s already impossible to refuse Kangoo Jumps.</p>\r\n<p>A new kind of fitness called \"Kangoo jumping\" is gaining popularity all over the world. This system guarantees burning twice as many calories as when running. At the same time during training, joints are less loaded by using a special device resembling a ski boot, to the sole of which an oval springy part is attached. So, visitors to the \"Kangoo jumping\" classes are encouraged to wear these shoes first, and then prepare for an intensive 45-minute cardio workout that combines dance moves, jogging and jumping around the hall to rhythmic music. The developers of the system say: in addition to working out the muscles of the legs, training affects all the main muscles. By the way, these miraculous boots NASA used to train cosmonauts. But to take advantage of them, no additional preparation is required. According to experts, the shoes are very stable, so the balance is difficult to lose and in the course of their practice, no one has ever fallen.</p>', 'Home', '', '', '2018-05-10 15:54:11', '2018-05-10 15:58:57'),
(7, 3, 'ru', '404', '', '', '404', '', '', '2018-05-10 15:57:29', '2018-05-10 15:57:29'),
(8, 3, 'ua', '404', '', '', '404', '', '', '2018-05-10 15:57:29', '2018-05-10 15:57:29'),
(9, 3, 'en', '404', '', '', '404', '', '', '2018-05-10 15:57:29', '2018-05-10 15:57:29'),
(10, 4, 'ru', 'Услуги', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Услуги', '', '', '2018-05-12 07:34:13', '2018-05-12 08:34:21'),
(11, 4, 'ua', 'Послуги', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Послуги', '', '', '2018-05-12 07:34:13', '2018-05-12 08:21:55'),
(12, 4, 'en', 'Services', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Services', '', '', '2018-05-12 07:34:13', '2018-05-12 08:21:55'),
(13, 5, 'ru', 'Товары', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Товары', '', '', '2018-05-13 08:17:20', '2018-05-13 08:18:52'),
(14, 5, 'ua', 'Товари', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Товари', '', '', '2018-05-13 08:17:20', '2018-05-13 08:18:52'),
(15, 5, 'en', 'Products', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Products', '', '', '2018-05-13 08:17:20', '2018-05-13 08:18:52'),
(16, 6, 'ru', 'Новости', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Новости', '', '', '2018-05-13 15:28:36', '2018-05-13 15:29:02'),
(17, 6, 'ua', 'Новини', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Новини', '', '', '2018-05-13 15:28:36', '2018-05-13 15:29:07'),
(18, 6, 'en', 'News', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'News', '', '', '2018-05-13 15:28:36', '2018-05-13 15:29:12'),
(19, 7, 'ru', 'Статьи', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Статьи', '', '', '2018-05-13 18:07:50', '2018-05-13 18:08:49'),
(20, 7, 'ua', 'Статті', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Статті', '', '', '2018-05-13 18:07:50', '2018-05-13 18:08:49'),
(21, 7, 'en', 'Articles', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Articles', '', '', '2018-05-13 18:07:50', '2018-05-13 18:08:49'),
(22, 8, 'ru', 'Цены', '', '<table class=\"p-table\">\r\n<tbody>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>1500</span>/мес</td>\r\n<td><span>150</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>3000</span>/мес</td>\r\n<td><span>300</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>4500</span>/мес</td>\r\n<td><span>450</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>6000</span>/мес</td>\r\n<td><span>600</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>7500</span>/мес</td>\r\n<td><span>750</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>9000</span>/мес</td>\r\n<td><span>900</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>10500</span>/мес</td>\r\n<td><span>1050</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>12000</span>/мес</td>\r\n<td><span>1200</span>/разовая</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Цены', '', '', '2018-05-13 18:26:22', '2018-05-13 18:37:10'),
(23, 8, 'ua', 'Ціни', '', '<table class=\"p-table\">\r\n<tbody>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>1500</span>/мес</td>\r\n<td><span>150</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>3000</span>/мес</td>\r\n<td><span>300</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>4500</span>/мес</td>\r\n<td><span>450</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>6000</span>/мес</td>\r\n<td><span>600</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>7500</span>/мес</td>\r\n<td><span>750</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>9000</span>/мес</td>\r\n<td><span>900</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>10500</span>/мес</td>\r\n<td><span>1050</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>12000</span>/мес</td>\r\n<td><span>1200</span>/разовая</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Ціни', '', '', '2018-05-13 18:26:22', '2018-05-13 18:37:21'),
(24, 8, 'en', 'Price', '', '<table class=\"p-table\">\r\n<tbody>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>1500</span>/мес</td>\r\n<td><span>150</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>3000</span>/мес</td>\r\n<td><span>300</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>4500</span>/мес</td>\r\n<td><span>450</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>6000</span>/мес</td>\r\n<td><span>600</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>7500</span>/мес</td>\r\n<td><span>750</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>9000</span>/мес</td>\r\n<td><span>900</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>10500</span>/мес</td>\r\n<td><span>1050</span>/разовая</td>\r\n</tr>\r\n<tr>\r\n<td>Тренировка с личным тренером</td>\r\n<td>Взрослый (16-70)</td>\r\n<td>С арендованым оборудованием</td>\r\n<td><span>12000</span>/мес</td>\r\n<td><span>1200</span>/разовая</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'Price', '', '', '2018-05-13 18:26:22', '2018-05-13 18:37:28'),
(25, 9, 'ru', 'Контакты', '', '', 'Контакты', '', '', '2018-05-13 18:40:38', '2018-05-13 18:48:18'),
(26, 9, 'ua', 'Контакти', '', '', 'Контакти', '', '', '2018-05-13 18:40:38', '2018-05-13 18:48:18'),
(27, 9, 'en', 'Contacts', '', '', 'Contacts', '', '', '2018-05-13 18:40:38', '2018-05-13 18:48:18'),
(28, 10, 'ru', 'О компании', '', '<div class=\"ap-title\">Команда <span>Kangoo club</span></div>\r\n<p>Кangoo jumps &ndash; последняя тенденция в спорте и фитнесе, которая набирает популярность и в Украине. Это один из лучших способов зарядится энергией и сбросить лишние килограммы. Главная фишка этого вида фитнеса &ndash; ботинки Кangoo jumps. С их помощью ваши тренировки превратятся в веселое времяпрепровождение, но польза от них будет в несколько раз выше!</p>\r\n<p>Ботинки на пружинах kangoo jumps, разработанные в 1994 году канадским ортопедом Грегори Легкхтман исключительно для реабилитации после операционного периода на суставы колен, голени и спины, буквально взорвали мировую фитнес-индустрию! Оснащенные уникальной пружинистой системой, они защищают спину и суставы на 80 %, дают и другие немаловажные результаты! Занятия и бег в них не только позволяют сжечь на 30% калорий больше, чем это происходит при занятиях в обычной обуви, но и позволяют подтянуть ягодицы и живот за рекордное время! Кроме того, тренировки в этих ботинках проходят очень динамично и весело.</p>', 'О компании', '', '', '2018-05-13 18:52:39', '2018-05-15 05:32:42'),
(29, 10, 'ua', 'Про компанію', '', '<div class=\"ap-title\">Команда <span>Kangoo club</span></div>\r\n<p>Кangoo jumps - остання тенденція в спорті і фітнесі, яка набирає популярність і в Україні. Це один з кращих способів зарядиться енергією і скинути зайві кілограми. Головна фішка цього виду фітнесу - черевики Кangoo jumps. З їх допомогою ваші тренування перетворяться в веселе проведення часу, але користь від них буде в кілька разів вище!</p>\r\n<p>Черевики на пружинах kangoo jumps, розроблені в 1994 році канадським ортопедом Грегорі Легкхтман виключно для реабілітації після операційного періоду на суглоби колін, гомілки і спини, буквально підірвали світову фітнес-індустрію! Оснащені унікальною пружною системою, вони захищають спину і суглоби на 80%, дають і інші важливі результати! Заняття і біг в них не тільки дозволяють спалити на 30% калорій більше, ніж це відбувається при заняттях в звичайному взутті, але і дозволяють підтягнути сідниці і живіт за рекордний час! Крім того, тренування в цих черевиках проходять дуже динамічно і весело.</p>', 'Про компанію', '', '', '2018-05-13 18:52:39', '2018-05-15 05:32:42'),
(30, 10, 'en', 'About company', '', '<div class=\"ap-title\"><span>The Kangoo club</span></div>\r\n<p>Kangoo jumps - the latest trend in sports and fitness, which is gaining popularity in Ukraine. This is one of the best ways to charge energy and lose weight. The main feature of this type of fitness is Kangoo jumps boots. With their help, your workouts will turn into a fun pastime, but the benefits from them will be several times higher!</p>\r\n<p>Boots on kangoo jumps springs, developed in 1994 by Canadian orthopedist Gregory Legkhtman exclusively for rehabilitation after the operational period on knee joints, shin and back, literally blew up the world fitness industry! Equipped with a unique springy system, they protect the back and joints by 80%, give and other important results! Classes and running in them not only allow you to burn up to 30% more calories than what happens when practicing in ordinary shoes, but also allow you to tighten the buttocks and stomach in a record time! In addition, the training in these shoes is very dynamic and fun.</p>', 'About company', '', '', '2018-05-13 18:52:39', '2018-05-15 05:36:54'),
(31, 11, 'ru', 'Отзывы', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Отзывы', '', '', '2018-05-14 05:06:31', '2018-05-14 05:06:59'),
(32, 11, 'ua', 'Відгуки', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Відгуки', '', '', '2018-05-14 05:06:31', '2018-05-14 05:06:59'),
(33, 11, 'en', 'Reviews', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Reviews', '', '', '2018-05-14 05:06:31', '2018-05-14 05:06:59'),
(34, 12, 'ru', 'Наша команда', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Наша команда', '', '', '2018-05-14 08:33:24', '2018-05-14 08:33:51'),
(35, 12, 'ua', 'Наша команда', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Наша команда', '', '', '2018-05-14 08:33:24', '2018-05-14 08:33:51'),
(36, 12, 'en', 'Team', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Team', '', '', '2018-05-14 08:33:24', '2018-05-14 08:33:51'),
(37, 13, 'ru', 'Акции', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Акции', '', '', '2018-05-14 10:33:34', '2018-05-14 10:34:02'),
(38, 13, 'ua', 'Акції', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Акції', '', '', '2018-05-14 10:33:34', '2018-05-14 10:34:02'),
(39, 13, 'en', 'Stock', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Stock', '', '', '2018-05-14 10:33:34', '2018-05-14 10:34:02'),
(40, 14, 'ru', 'Фотогалерея', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Фотогалерея', '', '', '2018-05-14 20:14:13', '2018-05-14 20:14:28'),
(41, 14, 'ua', 'Фотогалерея', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Фотогалерея', '', '', '2018-05-14 20:14:13', '2018-05-14 20:14:28'),
(42, 14, 'en', 'Photo gallery', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Photo gallery', '', '', '2018-05-14 20:14:13', '2018-05-14 20:14:28'),
(43, 15, 'ru', 'Видеогалерея', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Видеогалерея', '', '', '2018-05-17 07:23:19', '2018-05-17 07:23:44'),
(44, 15, 'ua', 'Відеогалерея', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Відеогалерея', '', '', '2018-05-17 07:23:19', '2018-05-17 07:23:44'),
(45, 15, 'en', 'Video gallery', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Video gallery', '', '', '2018-05-17 07:23:19', '2018-05-17 07:23:44'),
(46, 16, 'ru', 'Партнёры', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Партнёры', '', '', '2018-05-17 08:31:54', '2018-05-17 08:33:08'),
(47, 16, 'ua', 'Партнери', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Партнери', '', '', '2018-05-17 08:31:54', '2018-05-17 08:33:08');
INSERT INTO `as_pages_localization` (`id`, `page_id`, `lang`, `name`, `h1`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(48, 16, 'en', 'Partners', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Partners', '', '', '2018-05-17 08:31:54', '2018-05-17 08:33:08'),
(49, 17, 'ru', 'Лицензии и сертификаты', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>', 'Лицензии и сертификаты', '', '', '2018-05-17 10:47:28', '2018-05-17 10:48:12'),
(50, 17, 'ua', 'Ліцензії та сертифікати', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>', 'Ліцензії та сертифікати', '', '', '2018-05-17 10:47:28', '2018-05-17 10:48:12'),
(51, 17, 'en', 'Licenses and certificates', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>', 'Licenses and certificates', '', '', '2018-05-17 10:47:28', '2018-05-17 10:48:12'),
(52, 18, 'ru', 'Карта сайта', '', '', 'Карта сайта', '', '', '2018-05-18 05:49:25', '2018-05-18 07:09:26'),
(53, 18, 'ua', 'Мапа сайту', '', '', 'Мапа сайту', '', '', '2018-05-18 05:49:25', '2018-05-18 05:49:25'),
(54, 18, 'en', 'Sitemap', '', '', 'Sitemap', '', '', '2018-05-18 05:49:25', '2018-05-18 05:49:25'),
(55, 19, 'ru', 'Поиск', '', '<p style=\"text-align: center;\"><span style=\"font-size: 24pt;\">Ничего не найдено</span></p>', 'Поиск', '', '', '2018-05-18 05:52:01', '2018-05-18 10:06:51'),
(56, 19, 'ua', 'Пошук', '', '<p style=\"text-align: center;\"><span style=\"font-size: 24pt;\">Нічого не знайдено</span></p>', 'Пошук', '', '', '2018-05-18 05:52:01', '2018-05-18 10:06:51'),
(57, 19, 'en', 'Search', '', '<p style=\"text-align: center;\"><span style=\"font-size: 24pt;\">Nothing found</span></p>', 'Search', '', '', '2018-05-18 05:52:01', '2018-05-18 10:07:17'),
(58, 20, 'ru', 'Расписание', '', '', 'Расписание', '', '', '2018-05-21 07:35:40', '2018-05-21 07:35:40'),
(59, 20, 'ua', 'Розклад', '', '', 'Розклад', '', '', '2018-05-21 07:35:40', '2018-05-21 07:35:40'),
(60, 20, 'en', 'Schedule', '', '', 'Schedule', '', '', '2018-05-21 07:35:40', '2018-05-21 07:35:40');

-- --------------------------------------------------------

--
-- Структура таблицы `as_partners`
--

CREATE TABLE `as_partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_partners`
--

INSERT INTO `as_partners` (`id`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(8, '1526563327_partneritem.png', 1, 0, '2018-05-17 10:22:07', '2018-05-17 10:22:07'),
(9, '1526563334_partneritem.png', 1, 0, '2018-05-17 10:22:14', '2018-05-17 10:22:14'),
(10, '1526563343_partneritem.png', 1, 0, '2018-05-17 10:22:23', '2018-05-17 10:22:23'),
(11, '1526563349_partneritem.png', 1, 0, '2018-05-17 10:22:29', '2018-05-17 10:22:29');

-- --------------------------------------------------------

--
-- Структура таблицы `as_partners_localization`
--

CREATE TABLE `as_partners_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `partner_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_partners_localization`
--

INSERT INTO `as_partners_localization` (`id`, `partner_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(19, 8, 'ru', 'Партнёр #1', '2018-05-17 10:22:07', '2018-05-17 10:23:03'),
(20, 8, 'ua', 'Партнер #1', '2018-05-17 10:22:07', '2018-05-17 10:23:03'),
(21, 8, 'en', 'Partner #1', '2018-05-17 10:22:07', '2018-05-17 10:23:03'),
(22, 9, 'ru', 'Партнёр #2', '2018-05-17 10:22:14', '2018-05-17 10:23:33'),
(23, 9, 'ua', 'Партнер #2', '2018-05-17 10:22:14', '2018-05-17 10:23:33'),
(24, 9, 'en', 'Partner #2', '2018-05-17 10:22:14', '2018-05-17 10:23:33'),
(25, 10, 'ru', 'Партнёр #3', '2018-05-17 10:22:23', '2018-05-17 10:24:05'),
(26, 10, 'ua', 'Партнер #3', '2018-05-17 10:22:23', '2018-05-17 10:24:05'),
(27, 10, 'en', 'Partner #3', '2018-05-17 10:22:23', '2018-05-17 10:24:05'),
(28, 11, 'ru', 'Партнёр #4', '2018-05-17 10:22:29', '2018-05-17 10:24:45'),
(29, 11, 'ua', 'Партнер #4', '2018-05-17 10:22:29', '2018-05-17 10:24:45'),
(30, 11, 'en', 'Partner #4', '2018-05-17 10:22:29', '2018-05-17 10:24:45');

-- --------------------------------------------------------

--
-- Структура таблицы `as_photo_gallery_albums`
--

CREATE TABLE `as_photo_gallery_albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_photo_gallery_albums`
--

INSERT INTO `as_photo_gallery_albums` (`id`, `slug`, `image`, `visible`, `active`, `position`, `created_at`, `updated_at`) VALUES
(2, 'album-1', '1526328934_galleryalbum.jpg', 1, 1, 1, '2018-05-14 17:15:34', '2018-05-15 03:57:01'),
(3, 'album-2', '', 1, 0, 2, '2018-05-14 17:18:29', '2018-05-15 03:57:06');

-- --------------------------------------------------------

--
-- Структура таблицы `as_photo_gallery_albums_localization`
--

CREATE TABLE `as_photo_gallery_albums_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_photo_gallery_albums_localization`
--

INSERT INTO `as_photo_gallery_albums_localization` (`id`, `album_id`, `lang`, `name`, `h1`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(4, 2, 'ru', 'Альбом #1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Альбом #1', '', '', '2018-05-14 17:15:34', '2018-05-14 17:16:02'),
(5, 2, 'ua', 'Альбом #1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Альбом #1', '', '', '2018-05-14 17:15:34', '2018-05-14 17:15:34'),
(6, 2, 'en', 'Album #1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p></p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Album #1', '', '', '2018-05-14 17:15:34', '2018-05-14 17:15:38'),
(7, 3, 'ru', 'Альбом #2', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Альбом #2', '', '', '2018-05-14 17:18:29', '2018-05-14 17:18:29'),
(8, 3, 'ua', 'Альбом #2', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Альбом #2', '', '', '2018-05-14 17:18:29', '2018-05-14 17:18:29'),
(9, 3, 'en', 'Album #2', '', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Album #2', '', '', '2018-05-14 17:18:29', '2018-05-14 17:18:29');

-- --------------------------------------------------------

--
-- Структура таблицы `as_photo_gallery_images`
--

CREATE TABLE `as_photo_gallery_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_photo_gallery_images`
--

INSERT INTO `as_photo_gallery_images` (`id`, `album_id`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(25, 2, '1526368328_photo-66.jpg', 1, 2, '2018-05-15 04:12:09', '2018-06-06 05:48:54'),
(26, 2, '1526368329_photo-72.jpg', 1, 3, '2018-05-15 04:12:10', '2018-06-06 05:48:54'),
(27, 2, '1526368330_photo-82.jpg', 1, 1, '2018-05-15 04:12:11', '2018-06-06 05:48:51'),
(29, 2, '1526368388_photo-103.jpg', 1, 4, '2018-05-15 04:13:09', '2018-05-15 04:13:20'),
(30, 2, '1526368389_photo-104.jpg', 1, 5, '2018-05-15 04:13:10', '2018-05-15 04:13:20');

-- --------------------------------------------------------

--
-- Структура таблицы `as_photo_gallery_images_localization`
--

CREATE TABLE `as_photo_gallery_images_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_photo_gallery_images_localization`
--

INSERT INTO `as_photo_gallery_images_localization` (`id`, `image_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(52, 25, 'ru', '', '2018-05-15 04:12:09', '2018-05-15 04:12:09'),
(53, 25, 'ua', '', '2018-05-15 04:12:09', '2018-05-15 04:12:09'),
(54, 25, 'en', '', '2018-05-15 04:12:09', '2018-05-15 04:12:09'),
(55, 26, 'ru', 'Тест - #1', '2018-05-15 04:12:10', '2018-05-15 04:24:24'),
(56, 26, 'ua', 'Тест - #1', '2018-05-15 04:12:10', '2018-05-15 04:24:24'),
(57, 26, 'en', 'Тest - #1', '2018-05-15 04:12:10', '2018-05-15 04:24:24'),
(58, 27, 'ru', 'Тест - #2', '2018-05-15 04:12:11', '2018-05-15 04:24:49'),
(59, 27, 'ua', 'Тест - #2', '2018-05-15 04:12:11', '2018-05-15 04:24:49'),
(60, 27, 'en', 'Test - #2', '2018-05-15 04:12:11', '2018-05-15 04:24:49'),
(64, 29, 'ru', '', '2018-05-15 04:13:09', '2018-05-15 04:13:09'),
(65, 29, 'ua', '', '2018-05-15 04:13:09', '2018-05-15 04:13:09'),
(66, 29, 'en', '', '2018-05-15 04:13:09', '2018-05-15 04:13:09'),
(67, 30, 'ru', '', '2018-05-15 04:13:10', '2018-05-15 04:13:10'),
(68, 30, 'ua', '', '2018-05-15 04:13:10', '2018-05-15 04:13:10'),
(69, 30, 'en', '', '2018-05-15 04:13:10', '2018-05-15 04:13:10');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products`
--

CREATE TABLE `as_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products`
--

INSERT INTO `as_products` (`id`, `price`, `visible`, `active`, `position`, `created_at`, `updated_at`) VALUES
(5, '6048.00', 1, 1, 0, '2018-05-12 14:59:50', '2018-05-12 18:47:17'),
(6, '1254.00', 1, 1, 0, '2018-05-12 18:57:24', '2018-05-12 18:58:13'),
(7, '940.00', 1, 1, 0, '2018-05-13 08:12:56', '2018-05-13 08:12:59'),
(8, '1300.00', 1, 1, 0, '2018-05-13 08:14:58', '2018-05-13 08:14:58');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_features`
--

CREATE TABLE `as_products_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_features`
--

INSERT INTO `as_products_features` (`id`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(7, 1, 1, '2018-05-12 15:03:02', '2018-05-13 05:56:04'),
(8, 1, 3, '2018-05-12 17:43:28', '2018-05-13 05:56:04'),
(9, 1, 2, '2018-05-12 18:52:26', '2018-05-13 05:56:04'),
(10, 1, 4, '2018-05-13 05:53:52', '2018-05-13 05:56:04');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_features_localization`
--

CREATE TABLE `as_products_features_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `feature_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_features_localization`
--

INSERT INTO `as_products_features_localization` (`id`, `feature_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(19, 7, 'ru', 'Размер', '2018-05-12 15:03:02', '2018-05-12 15:03:02'),
(20, 7, 'ua', 'Розмір', '2018-05-12 15:03:02', '2018-05-12 15:03:02'),
(21, 7, 'en', 'Size', '2018-05-12 15:03:02', '2018-05-12 15:03:02'),
(22, 8, 'ru', 'Рекомендуемый вес', '2018-05-12 17:43:28', '2018-05-12 18:50:23'),
(23, 8, 'ua', 'Рекомендована вага', '2018-05-12 17:43:28', '2018-05-12 18:50:23'),
(24, 8, 'en', 'Recommended weight', '2018-05-12 17:43:28', '2018-05-12 18:50:23'),
(25, 9, 'ru', 'Пол', '2018-05-12 18:52:26', '2018-05-12 18:52:26'),
(26, 9, 'ua', 'Стать', '2018-05-12 18:52:26', '2018-05-12 18:52:26'),
(27, 9, 'en', 'Sex', '2018-05-12 18:52:26', '2018-05-12 18:52:26'),
(28, 10, 'ru', 'Страна производитель', '2018-05-13 05:53:52', '2018-05-13 05:53:52'),
(29, 10, 'ua', 'Країна виробник', '2018-05-13 05:53:52', '2018-05-13 05:53:52'),
(30, 10, 'en', 'Manufacturer country', '2018-05-13 05:53:52', '2018-05-13 05:53:52');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_features_options`
--

CREATE TABLE `as_products_features_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `feature_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_features_options`
--

INSERT INTO `as_products_features_options` (`id`, `product_id`, `feature_id`, `option_id`) VALUES
(83, 7, 7, 3),
(84, 7, 7, 4),
(85, 7, 9, 6),
(86, 7, 10, 9),
(87, 5, 7, 3),
(88, 5, 7, 4),
(89, 5, 9, 6),
(90, 5, 8, 5),
(91, 6, 7, 4),
(92, 6, 9, 7),
(93, 6, 10, 9),
(94, 8, 9, 6),
(95, 8, 10, 9);

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_images`
--

CREATE TABLE `as_products_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_images`
--

INSERT INTO `as_products_images` (`id`, `product_id`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(6, 5, '1526148091_51euj6lnvdlsy500.jpg', 1, 4, '2018-05-12 15:01:31', '2018-05-12 17:26:03'),
(7, 5, '1526148091_61mkoyn9xxlsy355.jpg', 1, 5, '2018-05-12 15:01:31', '2018-05-12 17:26:05'),
(8, 5, '1526148091_powershoe-kids-titanium-rood.jpg', 1, 3, '2018-05-12 15:01:31', '2018-05-12 17:32:09'),
(9, 5, '1526148091_powershoe-kids-zilver-rose.jpg', 1, 2, '2018-05-12 15:01:32', '2018-05-12 17:32:08'),
(11, 6, '1526162546_8e7cc55c0a2711e780c6901b0e95a2a8c78ea63e0a3f11e780c6901b0e95a2a8.jpg', 1, 0, '2018-05-12 19:02:26', '2018-05-12 19:02:26'),
(12, 7, '1526209984_uprugaya-pruzhina-ts6-xr-yellow8f1ff579324ee34a70f715789dfb655b.jpg', 1, 0, '2018-05-13 08:13:04', '2018-05-13 08:13:04'),
(13, 8, '1526210114_kj-bag-red8dfe34735dce35e20246086a0e53a0fb.jpg', 1, 0, '2018-05-13 08:15:14', '2018-05-13 08:15:14');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_localization`
--

CREATE TABLE `as_products_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_localization`
--

INSERT INTO `as_products_localization` (`id`, `product_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(13, 5, 'ru', 'KJ Power Shoe', '2018-05-12 14:59:50', '2018-05-12 14:59:50'),
(14, 5, 'ua', 'KJ Power Shoe', '2018-05-12 14:59:50', '2018-05-12 14:59:50'),
(15, 5, 'en', 'KJ Power Shoe', '2018-05-12 14:59:50', '2018-05-12 14:59:50'),
(16, 6, 'ru', 'Внутренний ботинок KJ (пара)', '2018-05-12 18:57:24', '2018-05-12 18:59:02'),
(17, 6, 'ua', 'Внутрішній черевик KJ (пара)', '2018-05-12 18:57:24', '2018-05-12 18:59:02'),
(18, 6, 'en', 'Internal shoe KJ (pair)', '2018-05-12 18:57:24', '2018-05-12 18:59:02'),
(19, 7, 'ru', 'Упругая пружина TS6 XR (пара)', '2018-05-13 08:12:56', '2018-05-13 08:12:56'),
(20, 7, 'ua', 'Пружна пружина TS6 XR (пара)', '2018-05-13 08:12:56', '2018-05-13 08:12:56'),
(21, 7, 'en', 'Spring spring TS6 XR (pair)', '2018-05-13 08:12:56', '2018-05-13 08:12:56'),
(22, 8, 'ru', 'KJ Bag', '2018-05-13 08:14:58', '2018-05-13 08:14:58'),
(23, 8, 'ua', 'KJ Bag', '2018-05-13 08:14:58', '2018-05-13 08:14:58'),
(24, 8, 'en', 'KJ Bag', '2018-05-13 08:14:58', '2018-05-13 08:14:58');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_options`
--

CREATE TABLE `as_products_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `feature_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_options`
--

INSERT INTO `as_products_options` (`id`, `feature_id`, `created_at`, `updated_at`) VALUES
(3, 7, '2018-05-12 15:03:32', '2018-05-12 15:03:32'),
(4, 7, '2018-05-12 15:04:13', '2018-05-12 15:04:13'),
(5, 8, '2018-05-12 17:44:21', '2018-05-12 17:44:21'),
(6, 9, '2018-05-12 18:53:04', '2018-05-12 18:53:04'),
(7, 9, '2018-05-13 05:22:11', '2018-05-13 05:22:11'),
(8, 9, '2018-05-13 05:23:19', '2018-05-13 05:23:19'),
(9, 10, '2018-05-13 05:54:16', '2018-05-13 05:54:16');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_options_localization`
--

CREATE TABLE `as_products_options_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_options_localization`
--

INSERT INTO `as_products_options_localization` (`id`, `option_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(7, 3, 'ru', 'XS (34-36)', '2018-05-12 15:03:32', '2018-05-12 15:03:58'),
(8, 3, 'ua', 'XS (34-36)', '2018-05-12 15:03:32', '2018-05-12 15:03:58'),
(9, 3, 'en', 'XS (34-36)', '2018-05-12 15:03:32', '2018-05-12 15:03:58'),
(10, 4, 'ru', 'S (36-38)', '2018-05-12 15:04:13', '2018-05-12 15:06:18'),
(11, 4, 'ua', 'S (36-38)', '2018-05-12 15:04:13', '2018-05-12 15:04:13'),
(12, 4, 'en', 'S (36-38)', '2018-05-12 15:04:13', '2018-05-12 15:04:13'),
(13, 5, 'ru', '70-90 кг.', '2018-05-12 17:44:21', '2018-05-12 18:50:53'),
(14, 5, 'ua', '70-90 кг.', '2018-05-12 17:44:21', '2018-05-12 18:50:53'),
(15, 5, 'en', '70-90 kg.', '2018-05-12 17:44:21', '2018-05-12 18:50:53'),
(16, 6, 'ru', 'Унисекс', '2018-05-12 18:53:04', '2018-05-12 18:53:04'),
(17, 6, 'ua', 'Унісекс', '2018-05-12 18:53:04', '2018-05-12 18:53:04'),
(18, 6, 'en', 'Unisex', '2018-05-12 18:53:04', '2018-05-12 18:53:04'),
(19, 7, 'ru', 'Мужской', '2018-05-13 05:22:11', '2018-05-13 05:22:11'),
(20, 7, 'ua', 'Чоловічий', '2018-05-13 05:22:11', '2018-05-13 05:22:11'),
(21, 7, 'en', 'Male', '2018-05-13 05:22:11', '2018-05-13 05:22:11'),
(22, 8, 'ru', 'Женский', '2018-05-13 05:23:19', '2018-05-13 05:23:19'),
(23, 8, 'ua', 'Жіночій', '2018-05-13 05:23:19', '2018-05-13 05:23:19'),
(24, 8, 'en', 'Female', '2018-05-13 05:23:19', '2018-05-13 05:23:19'),
(25, 9, 'ru', 'Китай', '2018-05-13 05:54:16', '2018-05-13 05:54:16'),
(26, 9, 'ua', 'Китай', '2018-05-13 05:54:16', '2018-05-13 05:54:16'),
(27, 9, 'en', 'China', '2018-05-13 05:54:16', '2018-05-13 05:54:16');

-- --------------------------------------------------------

--
-- Структура таблицы `as_products_orders`
--

CREATE TABLE `as_products_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` decimal(10,2) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_products_orders`
--

INSERT INTO `as_products_orders` (`id`, `user_name`, `user_phone`, `product_name`, `product_price`, `visible`, `created_at`, `updated_at`) VALUES
(11, 'Иван', '+38 (012) 345-67-89', 'Упругая пружина TS6 XR (пара)', '940.00', 0, '2018-05-14 07:16:56', '2018-05-14 07:16:56');

-- --------------------------------------------------------

--
-- Структура таблицы `as_reviews`
--

CREATE TABLE `as_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_reviews`
--

INSERT INTO `as_reviews` (`id`, `first_name`, `last_name`, `comment`, `visible`, `created_at`, `updated_at`) VALUES
(3, 'Иван', 'Иванов', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', 1, '2018-05-14 07:40:13', '2018-05-14 07:45:12'),
(5, 'Arnold', 'Shwarc', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', 1, '2018-05-14 07:50:10', '2018-05-14 07:50:17');

-- --------------------------------------------------------

--
-- Структура таблицы `as_services`
--

CREATE TABLE `as_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_services`
--

INSERT INTO `as_services` (`id`, `slug`, `background`, `image_name`, `active`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(5, 'kangoo-jumps', '1526118240_serviceitem1.jpg', '1526118240_servicename1.png', 1, 1, 1, '2018-05-12 06:44:00', '2018-05-12 07:40:43'),
(6, 'fly-stretching', '1526118592_serviceitem2.jpg', '1526118592_servicename2.png', 1, 1, 2, '2018-05-12 06:49:52', '2018-05-12 07:40:37');

-- --------------------------------------------------------

--
-- Структура таблицы `as_services_localization`
--

CREATE TABLE `as_services_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annotation` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_services_localization`
--

INSERT INTO `as_services_localization` (`id`, `service_id`, `lang`, `name`, `annotation`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(13, 5, 'ru', 'Кangoo Jumps', 'Кangoo Jumps – последняя тенденция в спорте и фитнесе, которая набирает популярность и в Украине. Это один из лучших способов зарядится энергией и сбросить лишние килограммы. Главная фишка этого вида фитнеса – ботинки Кangoo Jumps. С их помощью ваши тренировки превратятся в веселое времяпрепровождение, но польза от них будет в несколько раз выше!', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', '', '', '', '2018-05-12 06:44:00', '2018-06-19 06:21:06'),
(14, 5, 'ua', 'Кangoo Jumps', 'Кangoo Jumps - остання тенденція в спорті і фітнесі, яка набирає популярність і в Україні. Це один з кращих способів зарядиться енергією і скинути зайві кілограми. Головна фішка цього виду фітнесу - черевики Кangoo Jumps. З їх допомогою ваші тренування перетворяться в веселе проведення часу, але користь від них буде в кілька разів вище!', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', '', '', '', '2018-05-12 06:44:00', '2018-06-19 06:21:06'),
(15, 5, 'en', 'Кangoo Jumps', 'Kangoo Jumps - the latest trend in sports and fitness, which is gaining popularity in Ukraine. This is one of the best ways to charge energy and lose weight. The main feature of this type of fitness is Kangoo Jumps boots. With their help, your workouts will turn into a fun pastime, but the benefits from them will be several times higher!', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', '', '', '', '2018-05-12 06:44:00', '2018-06-19 06:21:06'),
(16, 6, 'ru', 'Fly Stretching', 'У этого вида тренировок несколько имен. Иногда эти занятия называют на английский манер fly stretching или AeroStretching. Встречается даже игривое название Monkey Dance, что в переводе означает «танец обезьян». Что ж, если немного напрячь воображение, то растяжка в гамаках и вправду может напомнить игру обезьянок, которые любят забавы ради раскачиваться на лианах. Но как бы не называли этот относительно новый вид физической активности, уже сегодня поклонников у растяжки в гамаках найдется немало.', '<p>У этого вида тренировок несколько имен. Иногда эти занятия называют на английский манер fly stretching или AeroStretching. Встречается даже игривое название Monkey Dance, что в переводе означает &laquo;танец обезьян&raquo;. Что ж, если немного напрячь воображение, то растяжка в гамаках и вправду может напомнить игру обезьянок, которые любят забавы ради раскачиваться на лианах. Но как бы не называли этот относительно новый вид физической активности, уже сегодня поклонников у растяжки в гамаках найдется немало.</p>\r\n<h1>Что такое fly stretching?</h1>\r\n<p>Для занятий, безусловно необходимы специальные гамаки. Это широкие полотна ткани, закрепленные под потолком. По виду они скорее напоминают качели.</p>\r\n<p>Использование подобного вспомогательного инвентаря позволяет прорабатывать даже самые глубокие мышцы, так называемые стабилизаторы. Достигается такой эффект благодаря тому, что во время выполнения определенных упражнений спортсмену необходимо добиться устойчивости. Стоит ли говорить, что, паря над полом в гамаке, найти опору весьма непросто.</p>\r\n<p>Такие условия делают выполнение даже простейших упражнений сложной задачей, и помогают прорабатывать все группы мышц наиболее эффективно. При этом ударные нагрузки на позвоночник исключаются, ведь сила притяжения становится меньше.</p>\r\n<p>Примечательно, что даже самые неспортивные новички уже с первых fly stretching занятий могут гордиться своими достижениями. Программа упражнений построена таким образом, чтобы определенных успехов можно было добиться уже на начальном этапе тренировок, исходя из индивидуальных физиологических возможностей. С каждым занятием нагрузки постепенно возрастают, усложняется и техника выполнения упражнений.</p>\r\n<p>Мышцы на fly stretching занятиях растягиваются в фазе полного расслабления. Находясь долгое время в определенной позиции, мышцы и сухожилия привыкают к этому положению. В результате чего можно добиться стабильной и устойчивой растяжки.</p>\r\n<p>\"Быстрее, выше, сильнее!\" - девиз возрождённых из небытия в начале прошлого века Олимпийских игр не утратил своей актуальности и в наши дни. Благодаря новым видам спорта этот олимпийский девиз приобретает другое, более современное звучание. Kangoo Jumps привлекают в свои ряды всё больше поклонников спорта, здорового образа жизни и... экстрима.</p>\r\n<p>Научиться ходить, бегать и совершать прыжки в обуви Kangoo Jumps можно довольно быстро. Обычно первая тренировка занимает всего несколько минут. Утром после тренировки встать с кровати очень тяжело, всё тело ужасно болит, а передвигаться получается с трудом. Но, испытав однажды волшебное чувство полёта, от Kangoo Jumps уже просто невозможно отказаться.</p>\r\n<p>По всему миру набирает популярность новый вид фитнеса под названием \"Kangoo jumping\". Данная система гарантирует сжигание в два раза большего количества калорий, чем при беге. При этом во время тренировки суставы меньше нагружаются за счет использования особого приспособления, напоминающего лыжный ботинок, к подошве которого прикреплена овальная пружинистая часть. Итак, посетителям занятия по \"Kangoo jumping\" предлагается для начала надеть эти ботинки, а затем приготовиться к интенсивной 45-минутной кардио-тренировке, которая совмещает в себе танцевальные движения, бег и прыжки по залу под ритмичную музыку. Разработчики системы заявляют: помимо проработки мышц ног, тренировки затрагивают все основные мышцы. Кстати, данные чудо-ботинки NASA использовало для тренировки космонавтов. Но, чтобы воспользоваться ими, никакой дополнительной подготовки не требуется. По словам экспертов, ботинки очень устойчивые, поэтому равновесие потерять трудно и в ходе их практики еще никто не падал.</p>', '', '', '', '2018-05-12 06:49:52', '2018-05-12 06:51:42'),
(17, 6, 'ua', 'Fly Stretching', 'У цього виду тренувань кілька імен. Іноді ці заняття називають на англійський манер fly stretching або AeroStretching. Зустрічається навіть грайливий назву Monkey Dance, що в перекладі означає «танець мавп». Що ж, якщо трохи напружити уяву, то розтяжка в гамаках і справді може нагадати гру мавпочок, які люблять забави заради розгойдуватися на ліанах. Але як би не називали цей відносно новий вид фізичної активності, вже сьогодні шанувальників у розтяжки в гамаках знайдеться чимало.', '<p>У цього виду тренувань кілька імен. Іноді ці заняття називають на англійський манер fly stretching або AeroStretching. Зустрічається навіть грайливий назву Monkey Dance, що в перекладі означає &laquo;танець мавп&raquo;. Що ж, якщо трохи напружити уяву, то розтяжка в гамаках і справді може нагадати гру мавпочок, які люблять забави заради розгойдуватися на ліанах. Але як би не називали цей відносно новий вид фізичної активності, вже сьогодні шанувальників у розтяжки в гамаках знайдеться чимало.</p>\r\n<h1>Що таке fly stretching?</h1>\r\n<p>Для занять, безумовно необхідні спеціальні гамаки. Це широкі полотна тканини, закріплені під стелею. По виду вони швидше нагадують гойдалки.</p>\r\n<p>Використання подібного допоміжного інвентарю дозволяє опрацьовувати навіть найглибші м\'язи, так звані стабілізатори. Досягається такий ефект завдяки тому, що під час виконання певних вправ спортсмену необхідно домогтися стійкості. Чи варто говорити, що, хлопче над підлогою в гамаку, знайти опору вельми непросто.</p>\r\n<p>Такі умови роблять виконання навіть найпростіших вправ складним завданням, і допомагають опрацьовувати всі групи м\'язів найбільш ефективно. При цьому ударні навантаження на хребет виключаються, адже сила тяжіння стає менше.</p>\r\n<p>Примітно, що навіть самі неспортивні новачки вже з перших fly stretching занять можуть пишатися своїми досягненнями. Програма вправ побудована таким чином, щоб певних успіхів можна було домогтися вже на початковому етапі тренувань, виходячи з індивідуальних фізіологічних можливостей. З кожним заняттям навантаження поступово зростають, ускладнюється і техніка виконання вправ.</p>\r\n<p>М\'язи на fly stretching заняттях розтягуються в фазі повного розслаблення. Перебуваючи довгий час в певній позиції, м\'язи та сухожилля звикають до такого стану речей. В результаті чого можна досягти стабільної та стійкої розтяжки.</p>\r\n<p>\"Швидше вище сильніше!\" - девіз відроджених з небуття на початку минулого століття Олімпійських ігор не втратив своєї актуальності і в наші дні. Завдяки новим видам спорту цей олімпійський девіз набуває іншого, більш сучасне звучання. Kangoo Jumps залучають до своїх лав все більше шанувальників спорту, здорового способу життя і ... екстриму.</p>\r\n<p>Навчитися ходити, бігати і здійснювати стрибки у взутті Kangoo Jumps можна досить швидко. Зазвичай перше тренування займає всього кілька хвилин. Вранці після тренування встати з ліжка дуже важко, все тіло страшенно болить, а пересуватися виходить з працею. Але, відчувши одного разу чарівне відчуття польоту, від Kangoo Jumps вже просто неможливо відмовитися.</p>\r\n<p>По всьому світу набирає популярність новий вид фітнесу під назвою \"Kangoo jumping\". Дана система гарантує спалювання в два рази більшої кількості калорій, ніж при бігу. При цьому під час тренування суглоби менше навантажуються за рахунок використання особливого пристосування, що нагадує лижний черевик, до підошви якого прикріплена овальна пружиниста частина. Отже, відвідувачам заняття по \"Kangoo jumping\" пропонується для початку надіти ці черевики, а потім приготуватися до інтенсивної 45-хвилинної кардіо-тренування, яка поєднує в собі танцювальні рухи, біг і стрибки по залу під ритмічну музику. Розробники системи заявляють: крім опрацювання м\'язів ніг, тренування зачіпають всі основні м\'язи. До речі, дані диво-черевики NASA використовувало для тренування космонавтів. Але, щоб скористатися ними, ніякої додаткової підготовки не потрібно. За словами експертів, черевики дуже стійкі, тому рівновага втратити важко і в ході їх практики ще ніхто не падав.</p>', '', '', '', '2018-05-12 06:49:52', '2018-05-12 06:51:42'),
(18, 6, 'en', 'Fly Stretching', 'This type of training has several names. Sometimes these classes are called in English as fly stretching or AeroStretching. There is even the playful name Monkey Dance, which means \"dance of monkeys\". Well, if you strain your imagination a little, stretching in hammocks can really remind the game of monkeys who like to play for fun on the vines. But no matter how this relatively new type of physical activity is called, there are already a lot of admirers at the stretch in the hammocks.', '<p>This type of training has several names. Sometimes these classes are called in English as fly stretching or AeroStretching. There is even the playful name Monkey Dance, which means \"dance of monkeys\". Well, if you strain your imagination a little, stretching in hammocks can really remind the game of monkeys who like to play for fun on the vines. But no matter how this relatively new type of physical activity is called, there are already a lot of admirers at the stretch in the hammocks.</p>\r\n<h1>What is fly stretching?</h1>\r\n<p>For classes, you definitely need special hammocks. This is a wide fabric, fixed to the ceiling. By appearance they are more like a swing.</p>\r\n<p>The use of such ancillary equipment makes it possible to study even the deepest muscles, the so-called stabilizers. This effect is achieved due to the fact that during the performance of certain exercises the athlete needs to achieve sustainability. Needless to say, hovering over the floor in a hammock, it is very difficult to find support.</p>\r\n<p>Such conditions make performing even the simplest exercises a difficult task, and help to work out all muscle groups most effectively. In this case shock loads on the spine are excluded, because the force of attraction becomes smaller.</p>\r\n<p>It is noteworthy that even the most unsportsmanlike beginners already from the first fly stretching classes can be proud of their achievements. The program of exercises is constructed in such a way that certain successes could be achieved already at the initial stage of training, proceeding from individual physiological possibilities. With each occupation of the load gradually increase, the technique of performing exercises becomes more complicated.</p>\r\n<p>Muscles on fly stretching exercises stretch in the phase of complete relaxation. For a long time in a certain position, the muscles and tendons get used to this situation. As a result, it is possible to achieve a stable and stable stretching.</p>\r\n<p>\"Faster, higher, stronger!\" - the motto of the Olympic Games, revived from non-existence at the beginning of the last century, has not lost its relevance in our days. Thanks to new sports, this Olympic motto acquires a different, more modern sound. Kangoo Jumps attracts in its ranks more fans of sports, a healthy lifestyle and ... extreme.</p>\r\n<p>Learn how to walk, run and jump in shoes Kangoo Jumps can be pretty quickly. Usually the first training takes only a few minutes. In the morning after training to get out of bed it\'s very hard, the whole body is awfully sore, and it\'s hard to move around. But having once experienced a magical flight experience, it\'s already impossible to refuse Kangoo Jumps.</p>\r\n<p>A new kind of fitness called \"Kangoo jumping\" is gaining popularity all over the world. This system guarantees burning twice as many calories as when running. At the same time during training, joints are less loaded by using a special device resembling a ski boot, to the sole of which an oval springy part is attached. So, visitors to the \"Kangoo jumping\" classes are encouraged to wear these shoes first, and then prepare for an intensive 45-minute cardio workout that combines dance moves, jogging and jumping around the hall to rhythmic music. The developers of the system say: in addition to working out the muscles of the legs, training affects all the main muscles. By the way, these miraculous boots NASA used to train cosmonauts. But to take advantage of them, no additional training is required. According to experts, the shoes are very stable, so the balance is difficult to lose and in the course of their practice, no one has ever fallen.</p>', '', '', '', '2018-05-12 06:49:52', '2018-05-12 06:51:42');

-- --------------------------------------------------------

--
-- Структура таблицы `as_settings`
--

CREATE TABLE `as_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_settings`
--

INSERT INTO `as_settings` (`id`, `setting_name`, `setting_value`, `created_at`, `updated_at`) VALUES
(1, 'phone', '+38 066 123 45 67', '2018-05-10 04:24:33', '2018-05-10 04:25:09'),
(2, 'email', 'kangoo.zp@info', '2018-05-10 04:24:33', '2018-05-10 04:24:33'),
(3, 'address_1_ru', '<span>1 Зал:</span> г. Запорожье, Волгоградская ул., 26а', '2018-05-10 04:24:33', '2018-05-10 04:25:33'),
(4, 'address_2_ru', '<span>2 Зал:</span> г. Запорожье, Металлургов пр., 8a', '2018-05-10 04:24:33', '2018-05-10 04:25:33'),
(5, 'address_1_ua', '<span>1 Зал:</span> м. Запоріжжя, Волгоградска вул., 26а', '2018-05-10 04:24:33', '2018-05-10 04:27:20'),
(6, 'address_2_ua', '<span>2 Зал:</span> м. Запоріжжя, Металургів пр., 8a', '2018-05-10 04:24:33', '2018-05-10 04:27:20'),
(7, 'address_1_en', '<span>1 Hall:</span> Zaporozhye, Volgogradskaya str. 26a', '2018-05-10 04:24:33', '2018-05-10 04:31:35'),
(8, 'address_2_en', '<span>2 Hall:</span> Zaporozhye, Metallurgists ave, 8a', '2018-05-10 04:24:33', '2018-05-10 04:31:35'),
(9, 'viber', '+38 066 123 45 67', '2018-05-10 04:24:33', '2018-05-10 06:40:05'),
(10, 'telegram', '#', '2018-05-10 04:24:33', '2018-05-10 06:35:40'),
(11, 'facebook', '#', '2018-05-10 04:24:33', '2018-05-10 04:34:00'),
(12, 'instagram', '#', '2018-05-10 04:24:33', '2018-05-10 04:34:00'),
(13, 'h_slogan_ru', 'Найди время,<br/> а не оправдание', '2018-05-10 04:24:33', '2018-05-10 04:36:56'),
(14, 'h_text_ru', 'Первый клуб джампинга в Запорожье. Здесь Вы можете<br/> потренироваться, отдохнуть и просто провести время с пользой<br/> в хорошей компании', '2018-05-10 04:24:33', '2018-05-10 04:36:56'),
(15, 'h_slogan_ua', 'Знайди час, а не<br/> виправдання', '2018-05-10 04:24:33', '2018-05-10 06:16:22'),
(16, 'h_text_ua', 'Перший клуб джампінгу в Запоріжжі. Тут Ви можете<br/> потренуватися, відпочити і просто провести час з користю<br/> в хорошій компанії', '2018-05-10 04:24:33', '2018-05-10 04:38:41'),
(17, 'h_slogan_en', 'Find time,<br/> not justification', '2018-05-10 04:24:33', '2018-05-10 04:40:44'),
(18, 'h_text_en', 'The first jumping club in Zaporozhye. Here you can<br/> practice, relax and just spend time with the benefit<br/> of a good company', '2018-05-10 04:24:33', '2018-05-10 04:40:44'),
(19, 's_what_title_ru', '<span>Что такое</span>Kangoo jumps?', '2018-05-10 04:53:42', '2018-05-10 04:53:42'),
(20, 's_what_text_ru', 'Самая главная особенность такого направления фитнеса – использование специальной обуви. К подошве ботинок прикреплены полусферические детали, имеющие посередине перемычку, которая обеспечивает сферам подвижность и позволяет занимающимся пружинить в такой обуви. Верх представляет собой нечто похожее на основу роликовых коньков, но, конечно, вместо колёсиков здесь пружинящие платформы.', '2018-05-10 04:53:42', '2018-05-10 04:53:42'),
(21, 's_what_title_ua', '<span>Що таке</span> Kangoo jumps?', '2018-05-10 04:53:42', '2018-05-10 05:00:13'),
(22, 's_what_text_ua', 'Найголовніша особливість такого напряму фітнесу - використання спеціального взуття. До підошви черевиків прикріплені напівсферичні деталі, що мають посередині перемичку, яка забезпечує сферам рухливість і дозволяє, тому хто займається у них, пружинити в такому взутті. Верх являє собою щось схоже на основу роликових ковзанів, але, звичайно, замість роликів тут пружні платформи.', '2018-05-10 04:53:42', '2018-05-10 05:00:13'),
(23, 's_what_title_en', '<span>What is</span> Kangoo jumps?', '2018-05-10 04:53:42', '2018-05-10 05:02:26'),
(24, 's_what_text_en', 'The most important feature of this area of fitness is the use of special footwear. To the sole of the shoes hemispherical parts are attached, having in the middle a web that provides mobility to the spheres and allows those engaged to spring in such shoes. The top is something similar to the base of roller skates, but, of course, instead of the wheels there are sprung platforms.', '2018-05-10 04:53:42', '2018-05-10 05:02:26'),
(25, 's_area_first_title_ru', 'Носок-вкладыш', '2018-05-10 05:20:48', '2018-05-10 08:42:08'),
(26, 's_area_first_text_ru', 'Тонкий внутренний носок обеспечивает комфортную защиту ступни и голеностопа', '2018-05-10 05:20:48', '2018-05-10 08:42:08'),
(27, 's_area_second_title_ru', 'Крепления', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(28, 's_area_second_text_ru', 'Позволяют быстро надевать и снимать KJ', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(29, 's_area_third_title_ru', 'Поддерживающая пластина', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(30, 's_area_third_text_ru', 'Обеспечивают комфорт и стабильность при прыжках', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(31, 's_area_fourth_title_ru', 'Внешний ботинок', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(32, 's_area_fourth_text_ru', 'Изготовлен из прочного полиуретана', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(33, 's_area_fifth_title_ru', 'Нескользящая подошва', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(34, 's_area_fifth_text_ru', 'Прочная подошва для занятий на любой поверхности', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(35, 's_area_sixth_title_ru', 'Дуги', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(36, 's_area_sixth_text_ru', 'Изготовлены из уникального пластика \"Space age\"', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(37, 's_area_seventh_title_ru', 'Т-Пружина', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(38, 's_area_seventh_text_ru', 'Позволяет отрегулировать KJ в зависимости от веса и способа применения', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(39, 's_area_eighth_title_ru', 'Двойной ускоритель', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(40, 's_area_eighth_text_ru', 'Предназначен для стабилизации прыжков', '2018-05-10 05:20:48', '2018-05-10 05:22:54'),
(41, 's_area_first_title_ua', 'Носок-вкладиш', '2018-05-10 05:20:48', '2018-05-10 05:29:53'),
(42, 's_area_first_text_ua', 'Тонкий внутрішній носок забезпечує комфортний захист ступні і гомілки', '2018-05-10 05:20:48', '2018-05-10 10:07:33'),
(43, 's_area_second_title_ua', 'Кріплення', '2018-05-10 05:20:48', '2018-05-10 05:29:53'),
(44, 's_area_second_text_ua', 'Дозволяють швидко надягати та знімати KJ', '2018-05-10 05:20:48', '2018-05-10 05:29:53'),
(45, 's_area_third_title_ua', 'Підтримуюча пластина', '2018-05-10 05:20:48', '2018-05-10 05:31:22'),
(46, 's_area_third_text_ua', 'Забезпечують комфорт і стабільність при стрибках', '2018-05-10 05:20:48', '2018-05-10 05:31:22'),
(47, 's_area_fourth_title_ua', 'Зовнішній черевик', '2018-05-10 05:20:48', '2018-05-10 05:33:49'),
(48, 's_area_fourth_text_ua', 'Виготовлений з міцного поліуретану', '2018-05-10 05:20:48', '2018-05-10 05:33:49'),
(49, 's_area_fifth_title_ua', 'Нековзаюча підошва', '2018-05-10 05:20:48', '2018-05-10 05:33:49'),
(50, 's_area_fifth_text_ua', 'Міцна підошва для занять на будь-якій поверхні', '2018-05-10 05:20:48', '2018-05-10 05:33:49'),
(51, 's_area_sixth_title_ua', 'Дуги', '2018-05-10 05:20:48', '2018-05-10 05:35:54'),
(52, 's_area_sixth_text_ua', 'Виготовлені з унікального пластика \"Space age\"', '2018-05-10 05:20:48', '2018-05-10 05:35:54'),
(53, 's_area_seventh_title_ua', 'Т-Пружина', '2018-05-10 05:20:48', '2018-05-10 05:35:54'),
(54, 's_area_seventh_text_ua', 'Дозволяє відрегулювати KJ в залежності від ваги і способу застосування', '2018-05-10 05:20:48', '2018-05-10 10:07:33'),
(55, 's_area_eighth_title_ua', 'Подвійний прискорювач', '2018-05-10 05:20:48', '2018-05-10 05:36:41'),
(56, 's_area_eighth_text_ua', 'Призначений для стабілізації стрибків', '2018-05-10 05:20:48', '2018-05-10 05:36:41'),
(57, 's_area_first_title_en', 'Sock liner', '2018-05-10 05:20:48', '2018-05-10 05:38:41'),
(58, 's_area_first_text_en', 'Slim inner toe provides comfortable foot and ankle protection', '2018-05-10 05:20:48', '2018-05-10 10:07:33'),
(59, 's_area_second_title_en', 'Mountings', '2018-05-10 05:20:48', '2018-05-10 05:38:41'),
(60, 's_area_second_text_en', 'Allow to quickly put on and remove KJ', '2018-05-10 05:20:48', '2018-05-10 05:38:41'),
(61, 's_area_third_title_en', 'Support plate', '2018-05-10 05:20:48', '2018-05-10 05:39:49'),
(62, 's_area_third_text_en', 'Provide comfort and stability when jumping', '2018-05-10 05:20:48', '2018-05-10 05:39:49'),
(63, 's_area_fourth_title_en', 'External shoe', '2018-05-10 05:20:48', '2018-05-10 05:39:49'),
(64, 's_area_fourth_text_en', 'Made of durable polyurethane', '2018-05-10 05:20:48', '2018-05-10 10:07:33'),
(65, 's_area_fifth_title_en', 'Non-slip sole', '2018-05-10 05:20:48', '2018-05-10 05:40:30'),
(66, 's_area_fifth_text_en', 'Sturdy sole for use on any surface', '2018-05-10 05:20:48', '2018-05-10 05:40:30'),
(67, 's_area_sixth_title_en', 'Arcs', '2018-05-10 05:20:48', '2018-05-10 05:42:18'),
(68, 's_area_sixth_text_en', 'Made of unique plastic \"Space age\"', '2018-05-10 05:20:48', '2018-05-10 05:42:18'),
(69, 's_area_seventh_title_en', 'T-Spring', '2018-05-10 05:20:48', '2018-05-10 05:43:09'),
(70, 's_area_seventh_text_en', 'Allows you to adjust the KJ according to the weight and method of application', '2018-05-10 05:20:48', '2018-05-10 05:43:09'),
(71, 's_area_eighth_title_en', 'Double accelerator', '2018-05-10 05:20:48', '2018-05-10 05:44:03'),
(72, 's_area_eighth_text_en', 'Designed to stabilize jumps', '2018-05-10 05:20:48', '2018-05-10 05:44:03'),
(73, 'achievements_1_ru', '10 лет опыта работы в сфере спорта', '2018-05-15 05:46:48', '2018-05-15 05:48:59'),
(74, 'achievements_2_ru', 'Все тренера профессионалы своего дела', '2018-05-15 05:46:48', '2018-05-15 05:48:59'),
(75, 'achievements_3_ru', '10 лет опыта работы в сфере спорта', '2018-05-15 05:46:48', '2018-05-15 05:48:59'),
(76, 'achievements_4_ru', 'Все тренера профессионалы своего дела', '2018-05-15 05:46:48', '2018-05-15 05:48:59'),
(77, 'achievements_1_ua', '10 років досвіду роботи в сфері спорту', '2018-05-15 05:46:48', '2018-05-15 05:48:59'),
(78, 'achievements_2_ua', 'Усі тренери професіонали свого діла', '2018-05-15 05:46:48', '2018-05-15 05:55:25'),
(79, 'achievements_3_ua', '10 років досвіду роботи в сфері спорту', '2018-05-15 05:46:48', '2018-05-15 05:48:59'),
(80, 'achievements_4_ua', 'Усі тренери професіонали свого діла', '2018-05-15 05:46:48', '2018-05-15 05:55:25'),
(81, 'achievements_1_en', '10 years of experience in the field of sports', '2018-05-15 05:46:48', '2018-05-15 05:49:33'),
(82, 'achievements_2_en', 'All coaches are professionals of their business', '2018-05-15 05:46:48', '2018-05-15 05:49:33'),
(83, 'achievements_3_en', '10 years of experience in the field of sports', '2018-05-15 05:46:48', '2018-05-15 05:49:33'),
(84, 'achievements_4_en', 'All coaches are professionals of their business', '2018-05-15 05:46:48', '2018-05-15 05:49:33'),
(85, 'about_text_1_ru', '<p>Кangoo jumps &ndash; последняя тенденция в спорте и фитнесе, которая набирает популярность и в Украине. Это один из лучших способов зарядится энергией и сбросить лишние килограммы. Главная фишка этого вида фитнеса &ndash; ботинки Кangoo jumps. С их помощью ваши тренировки превратятся в веселое времяпрепровождение, но польза от них будет в несколько раз выше!</p>\r\n<p>Ботинки на пружинах kangoo jumps, разработанные в 1994 году канадским ортопедом Грегори Легкхтман исключительно для реабилитации после операционного периода на суставы колен, голени и спины, буквально взорвали мировую фитнес-индустрию! Оснащенные уникальной пружинистой системой, они защищают спину и суставы на 80 %, дают и другие немаловажные результаты! Занятия и бег в них не только позволяют сжечь на 30% калорий больше, чем это происходит при занятиях в обычной обуви, но и позволяют подтянуть ягодицы и живот за рекордное время! Кроме того, тренировки в этих ботинках проходят очень динамично и весело.</p>', '2018-05-15 06:09:17', '2018-05-15 06:09:17'),
(86, 'about_text_1_ua', '<p>Кangoo jumps - остання тенденція в спорті і фітнесі, яка набирає популярність і в Україні. Це один з кращих способів зарядиться енергією і скинути зайві кілограми. Головна фішка цього виду фітнесу - черевики Кangoo jumps. З їх допомогою ваші тренування перетворяться в веселе проведення часу, але користь від них буде в кілька разів вище!</p>\r\n<p>Черевики на пружинах kangoo jumps, розроблені в 1994 році канадським ортопедом Грегорі Легкхтман виключно для реабілітації після операційного періоду на суглоби колін, гомілки і спини, буквально підірвали світову фітнес-індустрію! Оснащені унікальною пружною системою, вони захищають спину і суглоби на 80%, дають і інші важливі результати! Заняття і біг в них не тільки дозволяють спалити на 30% калорій більше, ніж це відбувається при заняттях в звичайному взутті, але і дозволяють підтягнути сідниці і живіт за рекордний час! Крім того, тренування в цих черевиках проходять дуже динамічно і весело.</p>', '2018-05-15 06:09:17', '2018-05-15 06:09:17'),
(87, 'about_text_1_en', '<p>Kangoo jumps - the latest trend in sports and fitness, which is gaining popularity in Ukraine. This is one of the best ways to charge energy and lose weight. The main feature of this type of fitness is Kangoo jumps boots. With their help, your workouts will turn into a fun pastime, but the benefits from them will be several times higher!</p>\r\n<p>Boots on kangoo jumps springs, developed in 1994 by Canadian orthopedist Gregory Legkhtman exclusively for rehabilitation after the operational period on knee joints, shin and back, literally blew up the world fitness industry! Equipped with a unique springy system, they protect the back and joints by 80%, give and other important results! Classes and running in them not only allow you to burn up to 30% more calories than what happens when practicing in ordinary shoes, but also allow you to tighten the buttocks and stomach in a record time! In addition, the training in these shoes is very dynamic and fun.</p>', '2018-05-15 06:09:17', '2018-05-15 06:09:17'),
(88, 'about_text_2_ru', '<p>Кangoo jumps &ndash; последняя тенденция в спорте и фитнесе, которая набирает популярность и в Украине. Это один из лучших способов зарядится энергией и сбросить лишние килограммы. Главная фишка этого вида фитнеса &ndash; ботинки Кangoo jumps. С их помощью ваши тренировки превратятся в веселое времяпрепровождение, но польза от них будет в несколько раз выше!</p>\r\n<p>Ботинки на пружинах kangoo jumps, разработанные в 1994 году канадским ортопедом Грегори Легкхтман исключительно для реабилитации после операционного периода на суставы колен, голени и спины, буквально взорвали мировую фитнес-индустрию! Оснащенные уникальной пружинистой системой, они защищают спину и суставы на 80 %, дают и другие немаловажные результаты! Занятия и бег в них не только позволяют сжечь на 30% калорий больше, чем это происходит при занятиях в обычной обуви, но и позволяют подтянуть ягодицы и живот за рекордное время! Кроме того, тренировки в этих ботинках проходят очень динамично и весело.</p>', '2018-05-15 06:47:15', '2018-05-15 06:47:15'),
(89, 'about_text_2_ua', '<p>Кangoo jumps - остання тенденція в спорті і фітнесі, яка набирає популярність і в Україні. Це один з кращих способів зарядиться енергією і скинути зайві кілограми. Головна фішка цього виду фітнесу - черевики Кangoo jumps. З їх допомогою ваші тренування перетворяться в веселе проведення часу, але користь від них буде в кілька разів вище!</p>\r\n<p>Черевики на пружинах kangoo jumps, розроблені в 1994 році канадським ортопедом Грегорі Легкхтман виключно для реабілітації після операційного періоду на суглоби колін, гомілки і спини, буквально підірвали світову фітнес-індустрію! Оснащені унікальною пружною системою, вони захищають спину і суглоби на 80%, дають і інші важливі результати! Заняття і біг в них не тільки дозволяють спалити на 30% калорій більше, ніж це відбувається при заняттях в звичайному взутті, але і дозволяють підтягнути сідниці і живіт за рекордний час! Крім того, тренування в цих черевиках проходять дуже динамічно і весело.</p>', '2018-05-15 06:47:15', '2018-05-15 06:48:23'),
(90, 'about_text_2_en', '<p>Kangoo jumps - the latest trend in sports and fitness, which is gaining popularity in Ukraine. This is one of the best ways to charge energy and lose weight. The main feature of this type of fitness is Kangoo jumps boots. With their help, your workouts will turn into a fun pastime, but the benefits from them will be several times higher!</p>\r\n<p>Boots on kangoo jumps springs, developed in 1994 by Canadian orthopedist Gregory Legkhtman exclusively for rehabilitation after the operational period on knee joints, shin and back, literally blew up the world fitness industry! Equipped with a unique springy system, they protect the back and joints by 80%, give and other important results! Classes and running in them not only allow you to burn up to 30% more calories than what happens when practicing in ordinary shoes, but also allow you to tighten the buttocks and stomach in a record time! In addition, the training in these shoes is very dynamic and fun.</p>', '2018-05-15 06:47:15', '2018-05-15 06:48:23'),
(91, 's_main_video', 'https://www.youtube.com/watch?v=ulTi8j1x_08', '2018-05-22 10:18:11', '2018-05-22 10:44:10');

-- --------------------------------------------------------

--
-- Структура таблицы `as_stock`
--

CREATE TABLE `as_stock` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `discount` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_stock`
--

INSERT INTO `as_stock` (`id`, `slug`, `image`, `date`, `discount`, `visible`, `created_at`, `updated_at`) VALUES
(2, 'skidki-na-botinki', '1526311362_stockitem.jpg', '2018-05-08 15:22:42', 30, 1, '2018-05-14 12:22:42', '2018-05-14 12:23:20'),
(3, 'skidki-na-zanyatiya', '1526319646_galleryitem2.jpg', '2018-05-31 17:41:47', 50, 1, '2018-05-14 14:40:46', '2018-05-14 14:41:47');

-- --------------------------------------------------------

--
-- Структура таблицы `as_stock_localization`
--

CREATE TABLE `as_stock_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `annotation` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_stock_localization`
--

INSERT INTO `as_stock_localization` (`id`, `post_id`, `lang`, `name`, `h1`, `annotation`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(4, 2, 'ru', 'Скидки на ботинки', '', 'Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач. Товарищи! укрепление и развитие структуры играет важную роль в формировании новых предложений. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности влечет за собой процесс внедрения и модернизации направлений', '<p><span>Не следует, однако забывать, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач. Товарищи! укрепление и развитие структуры играет важную роль в формировании новых предложений. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности влечет за собой процесс внедрения и модернизации направлений</span></p>', 'Скидки на ботинки', '', '', '2018-05-14 12:22:42', '2018-05-14 12:22:42'),
(5, 2, 'ua', 'Знижки на черевики', '', 'Не слід, однак забувати, що реалізація намічених планових завдань тягне за собою процес впровадження і модернізації позицій, займаних учасниками щодо поставлених завдань. Товариші! зміцнення і розвиток структури відіграє важливу роль у формуванні нових пропозицій. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності тягне за собою процес впровадження і модернізації напрямків', '<p>Не слід, однак забувати, що реалізація намічених планових завдань тягне за собою процес впровадження і модернізації позицій, займаних учасниками щодо поставлених завдань. Товариші! зміцнення і розвиток структури відіграє важливу роль у формуванні нових пропозицій. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності тягне за собою процес впровадження і модернізації напрямків</p>', 'Знижки на черевики', '', '', '2018-05-14 12:22:42', '2018-05-14 12:22:42'),
(6, 2, 'en', 'Discounts for shoes', '', 'We should not, however, forget that the implementation of the planned targets assigns the process of introducing and modernizing the positions occupied by the participants in relation to the tasks assigned. Comrades! strengthening and development of the structure plays an important role in the formation of new proposals. The task of the organization, especially the constant quantitative growth and scope of our activity entails the process of introducing and modernizing the directions', '<p>We should not, however, forget that the implementation of the planned targets assigns the process of introducing and modernizing the positions occupied by the participants in relation to the tasks assigned. Comrades! strengthening and development of the structure plays an important role in the formation of new proposals. The task of the organization, especially the constant quantitative growth and scope of our activity entails the process of introducing and modernizing the directions</p>', 'Discounts for shoes', '', '', '2018-05-14 12:22:42', '2018-05-14 12:22:42'),
(7, 3, 'ru', 'Скидки на занятия', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>\r\n<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Скидки на занятия', '', '', '2018-05-14 14:40:46', '2018-05-14 14:41:47'),
(8, 3, 'ua', 'Знижки на заняття', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>\r\n<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Знижки на заняття', '', '', '2018-05-14 14:40:46', '2018-05-14 14:41:47'),
(9, 3, 'en', 'Discounts for classes', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>\r\n<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</span></p>', 'Discounts for classes', '', '', '2018-05-14 14:40:46', '2018-05-14 14:41:47');

-- --------------------------------------------------------

--
-- Структура таблицы `as_subscribers`
--

CREATE TABLE `as_subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_subscribers`
--

INSERT INTO `as_subscribers` (`id`, `email`, `visible`, `created_at`, `updated_at`) VALUES
(1, 'test@test.com', 1, '2018-05-18 11:42:58', '2018-05-18 12:29:47'),
(9, 'semenovfedor@mail.ua', 1, '2018-05-18 12:18:52', '2018-05-18 12:29:46'),
(10, 'scaletta.dev@gmail.com', 1, '2018-05-21 05:22:22', '2018-05-21 05:22:22');

-- --------------------------------------------------------

--
-- Структура таблицы `as_team`
--

CREATE TABLE `as_team` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_team`
--

INSERT INTO `as_team` (`id`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(2, '1526303975_teamitem.jpg', 1, 1, '2018-05-14 10:19:10', '2018-05-14 10:24:11'),
(3, '1526304229_silachev-evgeniy.jpg', 1, 2, '2018-05-14 10:22:24', '2018-05-14 10:25:16');

-- --------------------------------------------------------

--
-- Структура таблицы `as_team_localization`
--

CREATE TABLE `as_team_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_team_localization`
--

INSERT INTO `as_team_localization` (`id`, `post_id`, `lang`, `first_name`, `last_name`, `role`, `created_at`, `updated_at`) VALUES
(4, 2, 'ru', 'Елена', 'Павлова', 'Тренер', '2018-05-14 10:19:10', '2018-05-14 10:19:10'),
(5, 2, 'ua', 'Олена', 'Павлова', 'Тренер', '2018-05-14 10:19:10', '2018-05-14 10:19:10'),
(6, 2, 'en', 'Elena', 'Pavlova', 'Trainer', '2018-05-14 10:19:10', '2018-05-14 10:19:10'),
(7, 3, 'ru', 'Максим', 'Иванов', 'Фитнес инструктор', '2018-05-14 10:22:24', '2018-05-14 10:22:24'),
(8, 3, 'ua', 'Максим', 'Іванов', 'Фітнес інструктор', '2018-05-14 10:22:24', '2018-05-14 10:22:24'),
(9, 3, 'en', 'Max', 'Ivanov', 'Fitness instructor', '2018-05-14 10:22:24', '2018-05-14 10:22:24');

-- --------------------------------------------------------

--
-- Структура таблицы `as_video_gallery_albums`
--

CREATE TABLE `as_video_gallery_albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_video_gallery_albums`
--

INSERT INTO `as_video_gallery_albums` (`id`, `slug`, `image`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(3, 'album-1', '1526549144_galleryalbum.jpg', 1, 2, '2018-05-16 17:38:17', '2018-05-17 07:47:28');

-- --------------------------------------------------------

--
-- Структура таблицы `as_video_gallery_albums_localization`
--

CREATE TABLE `as_video_gallery_albums_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_video_gallery_albums_localization`
--

INSERT INTO `as_video_gallery_albums_localization` (`id`, `album_id`, `lang`, `name`, `h1`, `body`, `meta_title`, `meta_keywords`, `meta_description`, `created_at`, `updated_at`) VALUES
(7, 3, 'ru', 'Альбом #1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Альбом #1', '', '', '2018-05-16 17:38:17', '2018-05-17 06:26:32'),
(8, 3, 'ua', 'Альбом #1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Альбом #1', '', '', '2018-05-16 17:38:17', '2018-05-17 06:26:32'),
(9, 3, 'en', 'Album #1', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum turpis mauris, ultricies at lectus at, facilisis finibus nunc. Phasellus tellus turpis, fringilla sit amet finibus eu, sollicitudin et nibh. Praesent fringilla suscipit arcu, sit amet auctor felis bibendum et. Sed at leo elit. Fusce id sapien convallis, porttitor ante non, bibendum tellus. Aenean ut lectus tellus. Nulla eget enim vel augue gravida iaculis. In finibus sapien at tortor tincidunt viverra. Sed aliquet a nisi et maximus. Morbi eget egestas sem. Suspendisse ut vehicula augue, at ultrices mauris.</p>\r\n<p>Mauris ac ligula vitae ante faucibus pharetra. Proin suscipit diam condimentum maximus pulvinar. Donec tempus auctor augue a feugiat. Nunc nec velit leo. Sed in nibh condimentum, lobortis dui dapibus, maximus odio. Nullam vitae quam vel leo sagittis aliquet. Etiam ut nisl nec nisi luctus rutrum. Mauris interdum nec lacus vel accumsan. Ut mattis eros velit, in malesuada lorem vestibulum eu. Quisque lacus tellus, pretium sit amet sem quis, convallis faucibus leo. Mauris efficitur ante nibh. Pellentesque varius, nunc in imperdiet iaculis, mi ligula dapibus felis, nec porttitor massa orci sed sapien.</p>', 'Album #1', '', '', '2018-05-16 17:38:17', '2018-05-17 06:26:32');

-- --------------------------------------------------------

--
-- Структура таблицы `as_video_gallery_items`
--

CREATE TABLE `as_video_gallery_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_video_gallery_items`
--

INSERT INTO `as_video_gallery_items` (`id`, `album_id`, `image`, `video`, `visible`, `position`, `created_at`, `updated_at`) VALUES
(10, 3, '1526554173_photo-1.jpg', '', 1, 2, '2018-05-17 07:49:33', '2018-05-17 08:01:23'),
(11, 3, '1526554173_photo-2.jpg', 'https://www.youtube.com/watch?v=xV4a2if4Plk', 1, 1, '2018-05-17 07:49:34', '2018-05-17 08:01:23');

-- --------------------------------------------------------

--
-- Структура таблицы `as_video_gallery_items_localization`
--

CREATE TABLE `as_video_gallery_items_localization` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `as_video_gallery_items_localization`
--

INSERT INTO `as_video_gallery_items_localization` (`id`, `item_id`, `lang`, `name`, `created_at`, `updated_at`) VALUES
(10, 10, 'ru', '', '2018-05-17 07:49:33', '2018-05-17 07:49:33'),
(11, 10, 'ua', '', '2018-05-17 07:49:33', '2018-05-17 07:49:33'),
(12, 10, 'en', '', '2018-05-17 07:49:33', '2018-05-17 07:49:33'),
(13, 11, 'ru', 'Видео #1', '2018-05-17 07:49:34', '2018-05-17 07:58:38'),
(14, 11, 'ua', 'Відео #1', '2018-05-17 07:49:34', '2018-05-17 07:58:38'),
(15, 11, 'en', 'Video #1', '2018-05-17 07:49:34', '2018-05-17 07:58:38');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_12_12_082237_create_languages_table', 1),
(4, '2018_01_22_112702_create_settings_table', 1),
(5, '2018_01_24_114715_create_pages_table', 1),
(6, '2018_01_24_115223_create_pages_localization_table', 1),
(7, '2017_12_23_140827_create_menu_table', 2),
(8, '2018_01_23_123614_create_menu_localization_table', 2),
(9, '2017_06_12_132311_create_slider_table', 3),
(10, '2018_01_25_112113_create_services_table', 4),
(11, '2018_01_25_112654_create_services_localization_table', 4),
(20, '2017_06_12_140844_create_products_table', 5),
(21, '2017_06_12_141646_create_products_images_table', 5),
(22, '2017_06_12_142021_create_products_features', 5),
(23, '2017_06_12_142400_create_products_options_table', 5),
(24, '2017_06_12_142727_create_products_features_options', 5),
(25, '2018_05_12_115708_create_products_localization_table', 5),
(26, '2018_05_12_120012_create_products_features_localization_table', 5),
(27, '2018_05_12_120339_create_products_options_localization_table', 5),
(28, '2018_05_13_114258_create_products_orders_table', 6),
(29, '2018_01_26_111037_create_news_table', 7),
(30, '2018_01_26_111511_create_news_localization_table', 7),
(33, '2018_01_26_111037_create_articles_table', 8),
(34, '2018_01_26_111511_create_articles_localization_table', 8),
(35, '2017_08_19_124503_create_reviews_table', 9),
(36, '2018_05_14_115642_create_team_table', 10),
(37, '2018_05_14_115717_create_team_localization_table', 10),
(38, '2018_05_14_135218_create_stock_table', 11),
(39, '2018_05_14_135254_create_stock_localization_table', 11),
(44, '2018_05_14_182136_create_photo_gallery_albums', 12),
(45, '2018_05_14_183633_create_photo_gallery_albums_localization', 12),
(46, '2018_05_14_202011_create_photo_gallery_images', 13),
(47, '2018_05_14_202050_create_photo_gallery_images_localization', 13),
(48, '2017_06_12_144314_create_feedbacks_table', 14),
(49, '2018_05_14_182136_create_video_gallery_albums', 14),
(50, '2018_05_14_183633_create_video_gallery_albums_localization', 14),
(51, '2018_05_14_202011_create_video_gallery_items', 14),
(52, '2018_05_14_202050_create_video_gallery_items_localization', 14),
(55, '2018_05_17_112430_create_partners_table', 15),
(56, '2018_05_17_113418_create_partners_localization_table', 15),
(57, '2018_05_17_133655_create_certificates_table', 16),
(58, '2018_05_17_133721_create_certificates_localization_table', 16),
(59, '2018_01_04_174131_create_subscribers_table', 17),
(60, '2018_06_19_082500_add_meta_tags_to_services_localization_table', 18);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'test@test.com', '$2y$10$MGOjhNFiRzF7mKcqy8lM..wLeDOKGSvwfkq1qnYHrtsYUGPKa1F.K', 'dUdbpr1bpazbcqa5yf4MRpdRlG9Hnz5tpTBqfqnvs5kAw7L67mBtb9CJIP82', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `as_articles`
--
ALTER TABLE `as_articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_articles_localization`
--
ALTER TABLE `as_articles_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_articles_localization_post_id_foreign` (`post_id`),
  ADD KEY `as_articles_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_certificates`
--
ALTER TABLE `as_certificates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_certificates_localization`
--
ALTER TABLE `as_certificates_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_certificates_localization_certificate_id_foreign` (`certificate_id`),
  ADD KEY `as_certificates_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_feedbacks`
--
ALTER TABLE `as_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_languages`
--
ALTER TABLE `as_languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `as_languages_code_unique` (`code`);

--
-- Индексы таблицы `as_main_slider`
--
ALTER TABLE `as_main_slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_menu`
--
ALTER TABLE `as_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_menu_localization`
--
ALTER TABLE `as_menu_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_menu_localization_item_id_foreign` (`item_id`),
  ADD KEY `as_menu_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_news`
--
ALTER TABLE `as_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_news_localization`
--
ALTER TABLE `as_news_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_news_localization_post_id_foreign` (`post_id`),
  ADD KEY `as_news_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_pages`
--
ALTER TABLE `as_pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_pages_localization`
--
ALTER TABLE `as_pages_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_pages_localization_page_id_foreign` (`page_id`),
  ADD KEY `as_pages_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_partners`
--
ALTER TABLE `as_partners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_partners_localization`
--
ALTER TABLE `as_partners_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_partners_localization_partner_id_foreign` (`partner_id`),
  ADD KEY `as_partners_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_photo_gallery_albums`
--
ALTER TABLE `as_photo_gallery_albums`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_photo_gallery_albums_localization`
--
ALTER TABLE `as_photo_gallery_albums_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_photo_gallery_albums_localization_album_id_foreign` (`album_id`),
  ADD KEY `as_photo_gallery_albums_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_photo_gallery_images`
--
ALTER TABLE `as_photo_gallery_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_photo_gallery_images_album_id_foreign` (`album_id`);

--
-- Индексы таблицы `as_photo_gallery_images_localization`
--
ALTER TABLE `as_photo_gallery_images_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_photo_gallery_images_localization_image_id_foreign` (`image_id`),
  ADD KEY `as_photo_gallery_images_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_products`
--
ALTER TABLE `as_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_products_features`
--
ALTER TABLE `as_products_features`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_products_features_localization`
--
ALTER TABLE `as_products_features_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_products_features_localization_feature_id_foreign` (`feature_id`),
  ADD KEY `as_products_features_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_products_features_options`
--
ALTER TABLE `as_products_features_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_products_features_options_product_id_foreign` (`product_id`),
  ADD KEY `as_products_features_options_feature_id_foreign` (`feature_id`),
  ADD KEY `as_products_features_options_option_id_foreign` (`option_id`);

--
-- Индексы таблицы `as_products_images`
--
ALTER TABLE `as_products_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_products_images_product_id_foreign` (`product_id`);

--
-- Индексы таблицы `as_products_localization`
--
ALTER TABLE `as_products_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_products_localization_product_id_foreign` (`product_id`),
  ADD KEY `as_products_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_products_options`
--
ALTER TABLE `as_products_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_products_options_feature_id_foreign` (`feature_id`);

--
-- Индексы таблицы `as_products_options_localization`
--
ALTER TABLE `as_products_options_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_products_options_localization_option_id_foreign` (`option_id`),
  ADD KEY `as_products_options_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_products_orders`
--
ALTER TABLE `as_products_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_reviews`
--
ALTER TABLE `as_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_services`
--
ALTER TABLE `as_services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_services_localization`
--
ALTER TABLE `as_services_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_services_localization_service_id_foreign` (`service_id`),
  ADD KEY `as_services_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_settings`
--
ALTER TABLE `as_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_stock`
--
ALTER TABLE `as_stock`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_stock_localization`
--
ALTER TABLE `as_stock_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_stock_localization_post_id_foreign` (`post_id`),
  ADD KEY `as_stock_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_subscribers`
--
ALTER TABLE `as_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `as_subscribers_email_unique` (`email`);

--
-- Индексы таблицы `as_team`
--
ALTER TABLE `as_team`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_team_localization`
--
ALTER TABLE `as_team_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_team_localization_post_id_foreign` (`post_id`),
  ADD KEY `as_team_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_video_gallery_albums`
--
ALTER TABLE `as_video_gallery_albums`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `as_video_gallery_albums_localization`
--
ALTER TABLE `as_video_gallery_albums_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_video_gallery_albums_localization_album_id_foreign` (`album_id`),
  ADD KEY `as_video_gallery_albums_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `as_video_gallery_items`
--
ALTER TABLE `as_video_gallery_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_video_gallery_items_album_id_foreign` (`album_id`);

--
-- Индексы таблицы `as_video_gallery_items_localization`
--
ALTER TABLE `as_video_gallery_items_localization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `as_video_gallery_items_localization_item_id_foreign` (`item_id`),
  ADD KEY `as_video_gallery_items_localization_lang_foreign` (`lang`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `as_articles`
--
ALTER TABLE `as_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `as_articles_localization`
--
ALTER TABLE `as_articles_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_certificates`
--
ALTER TABLE `as_certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_certificates_localization`
--
ALTER TABLE `as_certificates_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `as_feedbacks`
--
ALTER TABLE `as_feedbacks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `as_languages`
--
ALTER TABLE `as_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `as_main_slider`
--
ALTER TABLE `as_main_slider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `as_menu`
--
ALTER TABLE `as_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `as_menu_localization`
--
ALTER TABLE `as_menu_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `as_news`
--
ALTER TABLE `as_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `as_news_localization`
--
ALTER TABLE `as_news_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `as_pages`
--
ALTER TABLE `as_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `as_pages_localization`
--
ALTER TABLE `as_pages_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT для таблицы `as_partners`
--
ALTER TABLE `as_partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `as_partners_localization`
--
ALTER TABLE `as_partners_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `as_photo_gallery_albums`
--
ALTER TABLE `as_photo_gallery_albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `as_photo_gallery_albums_localization`
--
ALTER TABLE `as_photo_gallery_albums_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_photo_gallery_images`
--
ALTER TABLE `as_photo_gallery_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `as_photo_gallery_images_localization`
--
ALTER TABLE `as_photo_gallery_images_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT для таблицы `as_products`
--
ALTER TABLE `as_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `as_products_features`
--
ALTER TABLE `as_products_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `as_products_features_localization`
--
ALTER TABLE `as_products_features_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `as_products_features_options`
--
ALTER TABLE `as_products_features_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT для таблицы `as_products_images`
--
ALTER TABLE `as_products_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `as_products_localization`
--
ALTER TABLE `as_products_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `as_products_options`
--
ALTER TABLE `as_products_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_products_options_localization`
--
ALTER TABLE `as_products_options_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `as_products_orders`
--
ALTER TABLE `as_products_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `as_reviews`
--
ALTER TABLE `as_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `as_services`
--
ALTER TABLE `as_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `as_services_localization`
--
ALTER TABLE `as_services_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `as_settings`
--
ALTER TABLE `as_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT для таблицы `as_stock`
--
ALTER TABLE `as_stock`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `as_stock_localization`
--
ALTER TABLE `as_stock_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_subscribers`
--
ALTER TABLE `as_subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `as_team`
--
ALTER TABLE `as_team`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `as_team_localization`
--
ALTER TABLE `as_team_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_video_gallery_albums`
--
ALTER TABLE `as_video_gallery_albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `as_video_gallery_albums_localization`
--
ALTER TABLE `as_video_gallery_albums_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `as_video_gallery_items`
--
ALTER TABLE `as_video_gallery_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `as_video_gallery_items_localization`
--
ALTER TABLE `as_video_gallery_items_localization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `as_articles_localization`
--
ALTER TABLE `as_articles_localization`
  ADD CONSTRAINT `as_articles_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_articles_localization_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `as_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_certificates_localization`
--
ALTER TABLE `as_certificates_localization`
  ADD CONSTRAINT `as_certificates_localization_certificate_id_foreign` FOREIGN KEY (`certificate_id`) REFERENCES `as_certificates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_certificates_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_menu_localization`
--
ALTER TABLE `as_menu_localization`
  ADD CONSTRAINT `as_menu_localization_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `as_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_menu_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_news_localization`
--
ALTER TABLE `as_news_localization`
  ADD CONSTRAINT `as_news_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_news_localization_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `as_news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_pages_localization`
--
ALTER TABLE `as_pages_localization`
  ADD CONSTRAINT `as_pages_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_pages_localization_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `as_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_partners_localization`
--
ALTER TABLE `as_partners_localization`
  ADD CONSTRAINT `as_partners_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_partners_localization_partner_id_foreign` FOREIGN KEY (`partner_id`) REFERENCES `as_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_photo_gallery_albums_localization`
--
ALTER TABLE `as_photo_gallery_albums_localization`
  ADD CONSTRAINT `as_photo_gallery_albums_localization_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `as_photo_gallery_albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_photo_gallery_albums_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_photo_gallery_images`
--
ALTER TABLE `as_photo_gallery_images`
  ADD CONSTRAINT `as_photo_gallery_images_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `as_photo_gallery_albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_photo_gallery_images_localization`
--
ALTER TABLE `as_photo_gallery_images_localization`
  ADD CONSTRAINT `as_photo_gallery_images_localization_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `as_photo_gallery_images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_photo_gallery_images_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_products_features_localization`
--
ALTER TABLE `as_products_features_localization`
  ADD CONSTRAINT `as_products_features_localization_feature_id_foreign` FOREIGN KEY (`feature_id`) REFERENCES `as_products_features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_products_features_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_products_features_options`
--
ALTER TABLE `as_products_features_options`
  ADD CONSTRAINT `as_products_features_options_feature_id_foreign` FOREIGN KEY (`feature_id`) REFERENCES `as_products_features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_products_features_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `as_products_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_products_features_options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `as_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_products_images`
--
ALTER TABLE `as_products_images`
  ADD CONSTRAINT `as_products_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `as_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_products_localization`
--
ALTER TABLE `as_products_localization`
  ADD CONSTRAINT `as_products_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_products_localization_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `as_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_products_options`
--
ALTER TABLE `as_products_options`
  ADD CONSTRAINT `as_products_options_feature_id_foreign` FOREIGN KEY (`feature_id`) REFERENCES `as_products_features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_products_options_localization`
--
ALTER TABLE `as_products_options_localization`
  ADD CONSTRAINT `as_products_options_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_products_options_localization_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `as_products_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_services_localization`
--
ALTER TABLE `as_services_localization`
  ADD CONSTRAINT `as_services_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_services_localization_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `as_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_stock_localization`
--
ALTER TABLE `as_stock_localization`
  ADD CONSTRAINT `as_stock_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_stock_localization_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `as_stock` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_team_localization`
--
ALTER TABLE `as_team_localization`
  ADD CONSTRAINT `as_team_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_team_localization_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `as_team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_video_gallery_albums_localization`
--
ALTER TABLE `as_video_gallery_albums_localization`
  ADD CONSTRAINT `as_video_gallery_albums_localization_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `as_video_gallery_albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_video_gallery_albums_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_video_gallery_items`
--
ALTER TABLE `as_video_gallery_items`
  ADD CONSTRAINT `as_video_gallery_items_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `as_video_gallery_albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `as_video_gallery_items_localization`
--
ALTER TABLE `as_video_gallery_items_localization`
  ADD CONSTRAINT `as_video_gallery_items_localization_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `as_video_gallery_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `as_video_gallery_items_localization_lang_foreign` FOREIGN KEY (`lang`) REFERENCES `as_languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
