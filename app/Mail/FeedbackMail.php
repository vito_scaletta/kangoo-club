<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Illuminate\Http\Request instance.
     *
     * @var Request
     */
    protected $fields = [];
    protected $host = '';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fields, $host)
    {
        $this->fields = $fields;
        $this->host = $host;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@' . $this->host, 'Kangoo Club ZP')
                    ->subject('Обратная связь')
                    ->view('emails.feedback')
                    ->with($this->fields);
    }
}
