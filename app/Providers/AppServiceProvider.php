<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Youtube validation rule
        \Validator::extend('youtube_url', function( $attribute, $value, $parameters, $validator )
        {
            if( !preg_match("/^https\:\/\/www\.youtube\.com\/watch\?v\=.*/", $value) ) 
            {
                $validator->setCustomMessages(['youtube_url' => 'Некорректная ссылка на Youtube']);
                
                return FALSE;
            }

            return TRUE;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
