<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if( $locale = \Request::get('locale') )
            \App::setlocale( $locale );

        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            'comment'    => 'required|min:10'
        ];
    }
}
