<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\ReviewRequest;

use App\Http\Controllers\Frontend\BaseController;

use App\Mail\ReviewMail;
use Illuminate\Support\Facades\Mail;

use App\Models\Page;
use App\Models\Review;
use App\Models\Setting;

class ReviewsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'reviews')
                    ->firstOrFail();

        $reviews = Review::whereVisible(1)
                         ->orderBy('created_at', 'DESC')
                         ->paginate(12);

        return view('frontend.showReviews', compact(['page', 'reviews']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReviewRequest $request)
    {
        if( $request->ajax() )
        {
            $fields = $request->except(['_token', 'locale']);
            
            $review = Review::create($fields);

            $email = Setting::getSettings()->email;

            $host = $request->getHost();

            Mail::to($email)->send(new ReviewMail($fields, $host));

            return response()->json(['success' => __('design.message_review')], 200);
        }
    }
}
