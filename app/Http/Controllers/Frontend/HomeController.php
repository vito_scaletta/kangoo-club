<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\MainSlider;
use App\Models\Service;
use App\Models\Product;
use App\Models\PhotoGalleryAlbum;

class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', '')
                    ->firstOrFail();

        $main_slider = MainSlider::whereVisible(1)
                                 ->orderBy('position')
                                 ->get();

        $services = Service::select('as_services.*')
                           ->localName(app()->getLocale())
                           ->where('as_services.visible', 1)
                           ->where('as_services.active', 1)
                           ->orderBy('as_services.position')
                           ->take(2)
                           ->get();

        $products = Product::with(['product_images' => function($query){
                                $query->whereVisible(1)
                                      ->orderBy('position');
                             }])
                           ->with(['product_features' => function($query){
                                $query->leftJoin('as_products_features', 'as_products_features.id', '=', 'as_products_features_options.feature_id')
                                      ->leftJoin('as_products_features_localization', 'as_products_features_localization.feature_id', '=', 'as_products_features.id')
                                      ->leftJoin('as_products_options', 'as_products_options.id', '=', 'as_products_features_options.option_id')
                                      ->leftJoin('as_products_options_localization', 'as_products_options_localization.option_id', '=', 'as_products_options.id')
                                      ->select([
                                            'as_products_features_options.*',
                                            'as_products_features_localization.name AS feature_name',
                                            'as_products_options_localization.name AS option_name'
                                        ])
                                      ->where('as_products_features_localization.lang', app()->getLocale())
                                      ->where('as_products_options_localization.lang', app()->getLocale())
                                      ->where('as_products_features.visible', 1)
                                      ->orderBy('as_products_features.position');
                             }])
                           ->select('as_products.*')
                           ->localName(app()->getLocale())
                           ->where('as_products.visible', 1)
                           ->where('as_products.active', 1)
                           ->orderBy('as_products.position')
                           ->get();

        $photo_gallery = PhotoGalleryAlbum::with(['images' => function($query){
                                              $query->whereVisible(1)
                                                    ->orderBy('position');
                                            }])
                                          ->whereActive(1)
                                          ->take(1)
                                          ->get();

        $g_map_init = TRUE;

        return view('frontend.showMain', compact([
                                                    'page', 
                                                    'main_slider', 
                                                    'services', 
                                                    'products',
                                                    'photo_gallery',
                                                    'g_map_init'
                                                 ]));
    }
}
