<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;

class SearchController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'search')
                    ->firstOrFail();

        $search_results = [];

        if( $query = $request->get('s') )
        {
            // Pages
            $search_pages = $this->getResults( 'pages', 'App\Models\Page', $query );
            if( count($search_pages) )
            {
                foreach( $search_pages as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, (empty($item->slug) ? '/' : $item->slug) );
                }
            }

            // Services
            $search_services = $this->getResults( 'services', 'App\Models\Service', $query );
            if( count($search_services) )
            {
                foreach( $search_services as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, 'services/' . $item->slug );
                }
            }

            // News
            $search_news = $this->getResults( 'news', 'App\Models\News', $query );
            if( count($search_news) )
            {
                foreach( $search_news as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, 'news/' . $item->slug );
                }
            }

            // Articles
            $search_articles = $this->getResults( 'articles', 'App\Models\Article', $query );
            if( count($search_articles) )
            {
                foreach( $search_articles as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, 'articles/' . $item->slug );
                }
            }

            // Stock
            $search_stock = $this->getResults( 'stock', 'App\Models\Stock', $query );
            if( count($search_stock) )
            {
                foreach( $search_stock as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, 'stock/' . $item->slug );
                }
            }

            // Photo gallery
            $search_photo_gallery = $this->getResults( 'photo_gallery_albums', 'App\Models\PhotoGalleryAlbum', $query );
            if( count($search_photo_gallery) )
            {
                foreach( $search_photo_gallery as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, 'photo-gallery/' . $item->slug );
                }
            }

            // Video gallery
            $search_video_gallery = $this->getResults( 'video_gallery_albums', 'App\Models\VideoGalleryAlbum', $query );
            if( count($search_video_gallery) )
            {
                foreach( $search_video_gallery as $item )
                {
                    $search_results[] = $this->getContent( $item->name, $item->body, 'video-gallery/' . $item->slug );
                }
            }

            // Search results with pagination
            $search_results = $this->customPaginate( $search_results, $request );
        }

        return view('frontend.showSearchResults', compact(['page', 'search_results']));
    }

    // Generate content for search
    private function getContent( $name, $body, $slug )
    {
        $body = strip_tags($body);

        if( mb_strlen($body) > 330 )
            $body = str_limit($body, 330);

        $slug = \LaravelLocalization::getLocalizedURL(app()->getLocale(), $slug);

        return (object)['name' => $name, 'body' => $body, 'slug' => $slug];
    }

    // Get the result from the database on search query
    private function getResults( $table_name, $model_name, $search_query )
    {
        $results = app()->make($model_name)->select('as_' . $table_name . '.*')
                                           ->localAll(app()->getLocale())
                                           ->where('as_' . $table_name . '_localization.name', 'LIKE', '%' . $search_query . '%')
                                           ->orWhere('as_' . $table_name . '_localization.body', 'LIKE', '%' . $search_query . '%');
                                                               
        if( $model_name != 'App\Models\Page' )
            $results = $results->where('as_' . $table_name . '.visible', 1);
         
        $results = $results->groupBy('as_' . $table_name . '.id')
                           ->get();

        return $results;
    }

    // Custom pagination
    private function customPaginate( $data, $request ) 
    {
        if( count($data) )
        {
            // Get the current page from the url if it's not set default to 1
            $page = $request->get('page', 1);

            // Number of items per page
            $per_page = 10;

            // Start displaying items from this number;
            $offset = (($page * $per_page) - $per_page); // Start displaying items from this number

            // Get only the items you need using array_slice (only get 10 items since that's what you need)
            $items_for_current_page = array_slice($data, $offset, $per_page, true);

            // Return the paginator with only 10 items but with the count of all items and set the it on the correct page
            return new \Illuminate\Pagination\LengthAwarePaginator($items_for_current_page, count($data), $per_page, $page, ['path' => $request->url()]);
        }
        else
            return [];
    }
}
