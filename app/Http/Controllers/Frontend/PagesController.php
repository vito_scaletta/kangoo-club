<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;

class PagesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display all page
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex($slug)
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', $slug)
                    ->firstOrFail();

        return view('frontend.showPage', compact(['page']));
    }

    /**
     * Display contacts page
     *
     * @return \Illuminate\Http\Response
     */
    public function getContacts()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'contacts')
                    ->firstOrFail();

        $g_map_init = TRUE;

        return view('frontend.showPageContacts', compact(['page', 'g_map_init']));
    }

    /**
     * Display price page
     *
     * @return \Illuminate\Http\Response
     */
    public function getPrice()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'price')
                    ->firstOrFail();

        return view('frontend.showPagePrice', compact(['page']));
    }

    /**
     * Display about page
     *
     * @return \Illuminate\Http\Response
     */
    public function getAbout()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'about')
                    ->firstOrFail();

        $g_map_init = TRUE;

        return view('frontend.showPageAbout', compact(['page', 'g_map_init']));
    }

    /**
     * Display schedule page
     *
     * @return \Illuminate\Http\Response
     */
    public function getSchedule()
    {
        $page = Page::select('as_pages.*')
                    ->localAll(app()->getLocale())
                    ->where('as_pages.slug', '=', 'schedule')
                    ->firstOrFail();

        return view('frontend.showPageSchedule', compact(['page']));
    }
}
