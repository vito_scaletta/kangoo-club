<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Menu;
use App\Models\Setting;
use App\Models\Language;

class BaseController extends Controller
{
    public function __construct() 
    {
		$languages = Language::whereVisible(1)->get();

        $settings = Setting::getSettings();

        $menu = Menu::select('as_menu.*')
                    ->where('as_menu.visible', 1)
                    ->localName(app()->getLocale())
                    ->get()
                    ->toHierarchy();

        $front_page = $this->is_front_page();

        $g_map_init = FALSE;

		return view()->share(compact(['languages', 'settings', 'menu', 'front_page', 'g_map_init']));
    }

    // Is front page?
    private function is_front_page()
    {
        if( in_array(\Request::path(), ['/', app()->getLocale()]) )
            return TRUE;
        else
            return FALSE;
    }
}
