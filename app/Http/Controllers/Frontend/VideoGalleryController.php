<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\VideoGalleryAlbum;
use App\Models\VideoGalleryItem;

class VideoGalleryController extends BaseController
{
    private $video_gallery;

    public function __construct()
    {
        parent::__construct();

        $this->video_gallery = Page::select('as_pages.*')
                                   ->localAll(app()->getLocale())
                                   ->where('as_pages.slug', '=', 'video-gallery')
                                   ->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $this->video_gallery;

        $albums = VideoGalleryAlbum::select('as_video_gallery_albums.*');

        $albums_count = VideoGalleryAlbum::whereVisible(1)->count();

        if( $request->ajax() )
        {
            $locale = $request->get('locale');

            \App::setlocale($locale);

            $offset = (int)$request->get('offset');

            $albums = $albums->localAll($locale)
                             ->where('as_video_gallery_albums.visible', 1)
                             ->offset($offset)
                             ->limit(1)
                             ->orderBy('as_video_gallery_albums.position')
                             ->get();

            return response()->json([
                                'albums' => view('frontend.components._gallery_albums', ['albums' => $albums, 'type' => 'video'])->render(),
                                'offset' => ($offset + 1),
                                'albums_count' => $albums_count
                            ]);
        }
        else
        {
            $albums = $albums->localAll(app()->getLocale())
                             ->where('as_video_gallery_albums.visible', 1)
                             ->orderBy('as_video_gallery_albums.position')
                             ->take(8)
                             ->get();

            return view('frontend.showVideoGalleryAlbums', compact(['page', 'albums', 'albums_count']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = VideoGalleryAlbum::select('as_video_gallery_albums.*')
                                 ->localAll(app()->getLocale())
                                 ->where('as_video_gallery_albums.slug', '=', $slug)
                                 ->firstOrFail();

        $items = VideoGalleryItem::select('as_video_gallery_items.*')
                                 ->localName(app()->getLocale())
                                 ->where('as_video_gallery_items.album_id', '=', $page->id)
                                 ->where('as_video_gallery_items.visible', '=', 1)
                                 ->orderBy('as_video_gallery_items.position')
                                 ->paginate(18);

        $breadcrumbs = [(object)['name' => $this->video_gallery->name, 'url' => $this->video_gallery->slug]];

        return view('frontend.showVideoGalleryItems', compact(['page', 'items', 'breadcrumbs']));
    }
}
