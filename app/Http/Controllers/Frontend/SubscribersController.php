<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\SubscriberRequest;

use App\Http\Controllers\Frontend\BaseController;

use App\Mail\SubscriberMail;
use Illuminate\Support\Facades\Mail;

use App\Models\Setting;
use App\Models\Subscriber;

class SubscribersController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(SubscriberRequest $request)
    {
    	if( $request->ajax() )
        {
            $email_form = $request->get('email');

            $email_admin = Setting::getSettings()->email;

            $host = $request->getHost();

            Subscriber::create(['email' => $email_form]);

            Mail::to($email_admin)->send(new SubscriberMail($email_form, $host));

            return \Response::json(['success' => trans('design.subscribe_success')]);
        }
    }
}
