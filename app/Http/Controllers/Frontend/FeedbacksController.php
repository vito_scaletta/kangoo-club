<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\FeedbackRequest;

use App\Http\Controllers\Frontend\BaseController;

use App\Mail\FeedbackMail;
use Illuminate\Support\Facades\Mail;

use App\Models\Setting;
use App\Models\Feedback;

class FeedbacksController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(FeedbackRequest $request)
    {
        if( $request->ajax() )
        {
            $fields = $request->except(['_token', 'locale']);
            
            Feedback::create($fields);

            $email = Setting::getSettings()->email;

            $host = $request->getHost();

            Mail::to($email)->send(new FeedbackMail($fields, $host));

            return response()->json(['success' => trans('design.message_feedback')], 200);
        }
    }
}
