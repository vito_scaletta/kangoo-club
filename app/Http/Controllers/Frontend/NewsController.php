<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\BaseController;

use App\Models\Page;
use App\Models\News;

class NewsController extends BaseController
{
    private $news;

    public function __construct()
    {
        parent::__construct();

        $this->news = Page::select('as_pages.*')
                          ->localAll(app()->getLocale())
                          ->where('as_pages.slug', '=', 'news')
                          ->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->news;

        $posts = News::select('as_news.*')
                     ->localAll(app()->getLocale())
                     ->where('as_news.visible', 1)
                     ->orderBy('as_news.date', 'DESC')
                     ->paginate(24);

        $posts_type = 'news';

        return view('frontend.showPosts', compact(['page', 'posts', 'posts_type']));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = News::select('as_news.*')
                    ->localAll(app()->getLocale())
                    ->where('as_news.slug', '=', $slug)
                    ->firstOrFail();

        $breadcrumbs = [(object)['name' => $this->news->name, 'url' => $this->news->slug]];

        return view('frontend.showPage', compact(['page', 'breadcrumbs']));
    }
}
