<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\VideoGalleryItemRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\VideoGalleryItem;
use App\Models\VideoGalleryItemLocalization;
use App\Models\VideoGalleryAlbum;

class AdminVideoGalleryItemsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoGalleryItemRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'item_id'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $item = VideoGalleryItem::findOrFail((int)$request->get('item_id'));
        $item->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $item->localizations()
                      ->whereLang($language->code)
                      ->updateOrCreate(['lang' => $language->code, 'item_id' => $item->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация обновлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $album_id
     * @return \Illuminate\Http\Response
     */
    public function show($album_id)
    {
        $album_name = VideoGalleryAlbum::select('as_video_gallery_albums.*')
                                       ->localName(app()->getLocale())
                                       ->find($album_id)
                                       ->name;

        $title = 'Видеоролики альбома: "' . $album_name . '"';
        
        $items = VideoGalleryItem::whereAlbumId($album_id)
                                 ->orderBy('position')
                                 ->get();
        
        return view('admin.VideoGallery.showItems', compact(['title', 'items', 'album_name', 'album_id']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $item_id
     * @return \Illuminate\Http\Response
     */
    public function edit($item_id)
    {
        $title = 'Редактирование данных видеоролика ID - ' . $item_id;

        $post = VideoGalleryItem::whereId($item_id)->with('localizations')->first();

        $album = VideoGalleryAlbum::select('as_video_gallery_albums.*')
                                  ->localName(app()->getLocale())
                                  ->find($post->album_id);
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        return view('admin.VideoGallery.editItem', compact(['title', 'post', 'data', 'album', 'item_id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( $errors = VideoGalleryItem::upload_items($request->file('items'), $id) )
            return redirect()->back()->with('errors', $errors);
        else
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $items_ids = $request->get('check');

        $items = VideoGalleryItem::whereIn('id', $items_ids)->get();
        
        if( count($items) )
        {
            foreach( $items as $item )
            {
                \File::delete( public_path('uploads/video-gallery/images/' . $item->image) );
            }
        }

        VideoGalleryItem::destroy( $items_ids );

        return redirect()->back();
    }

    /**
     * Visible item
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $item = VideoGalleryItem::find( $id );
            
            $visible = (empty($item->visible) ? 1 : 0);
            
            $item->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable items
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                VideoGalleryItem::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}