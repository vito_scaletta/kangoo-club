<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ServiceRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Service;
use App\Models\ServiceLocalization;

class AdminServicesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Услуги';
        
        $services = Service::select('as_services.*')
                           ->localName(app()->getLocale())
                           ->orderBy('as_services.position')
                           ->get();
        
        return view('admin.Services.showServices', compact(['title', 'services']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление услуги';

        $post = new Service;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new ServiceLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/services');
        
        return view('admin.Services.editService', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image_name', 'background'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $service = Service::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $service->localizations()->create( $info );
        }

        // If upload background
        if( $background = $request->file('background') )
            Service::imageUpload( 'background', $background, $service->id );

        // If upload image_name
        if( $image_name = $request->file('image_name') )
            Service::imageUpload( 'image_name', $image_name, $service->id );

        return redirect( asset('master/services/' . $service->id) )->with('success', 'Услуга добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование услуги";

        $post = Service::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/services/' . $id);

        return view('admin.Services.editService', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image_name', 'background'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $service = Service::findOrFail($id);
        $service->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $service->localizations()
                        ->whereLang($language->code)
                        ->updateOrCreate(['lang' => $language->code, 'service_id' => $service->id], $request->{$language->code});
            }
        }

        // If upload background
        if( $background = $request->file('background') )
            Service::imageUpload( 'background', $background, $id );

        // If upload image_name
        if( $image_name = $request->file('image_name') )
            Service::imageUpload( 'image_name', $image_name, $id );

        return redirect()->back()->with('success', 'Услуга обновлена');
    }

    /**
     * Destroy service items
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $services = Service::whereIn('id', $ids)->get() )
        {
            foreach( $services as $item )
            {
                \File::delete( public_path('uploads/services/background/' . $item->background) );
                \File::delete( public_path('uploads/services/image_name/' . $item->image_name) );
            }
        }

        Service::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible/active service item
     *
     * @return JSON answer
     */
    public function toggle(Request $request)
    {
        if( $request->ajax() )
        {
            $service = Service::find( (int)$request->get('id') );
            
            $action = $request->get('action');

            $toggle = (empty($service->{$action}) ? 1 : 0);
            
            $service->update([$action => $toggle]);
            
            return \Response::json([$action => 'changed']);
        }
    }

    /**
     * Services sortable
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                Service::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
