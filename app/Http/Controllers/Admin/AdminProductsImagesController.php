<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Product;
use App\Models\ProductImage;

class AdminProductsImagesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Изображения товара';
        
        $images = ProductImage::whereProductId( $id )
                              ->orderBy('position')
                              ->get();

        $product_name = Product::select('as_products.*')
                               ->localName(app()->getLocale())
                               ->where('as_products.id', $id)
                               ->first()
                               ->name;

        return view('admin.Products.showProductImages', compact(['title', 'images', 'product_name']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( $errors = ProductImage::upload_images( $request->file('images'), $id ))
            return redirect()->back()->with('errors', $errors);
        else
            return redirect()->back();
    }

    /**
     * Remove products images
     *
     * @return redirect
     */
    public function destroy(Request $request)
    {
        $images_ids = $request->get('check');

        $images = ProductImage::whereIn('id', $images_ids)->get();
        
        if( count($images) )
        {
            foreach( $images as $item )
            {
                \File::delete( public_path('uploads/products/' . $item->image) );
                \File::delete( public_path('uploads/products/middle/' . $item->image) );
                \File::delete( public_path('uploads/products/thumbs/' . $item->image) );
            }
        }

        ProductImage::destroy( $images_ids );

        return redirect()->back();
    }

    /**
     * Visible image
     *
     * @return JSON answer
     */
    public function visible( Request $request )
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $image = ProductImage::find( $id );
            
            $visible = (empty($image->visible) ? 1 : 0);
            
            $image->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable images
     *
     * @return JSON answer
     */
    public function sortable( Request $request )
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                ProductImage::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
