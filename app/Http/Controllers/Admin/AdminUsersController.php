<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\User;

class AdminUsersController extends AdminBaseController
{
    public function __construct()
	{
        parent::__construct();
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Пользователи';
		
		$users = User::paginate(25);
		
		return view('admin.Users.showUsers', compact(['title', 'users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление пользователя';

		$post  = new User;
		
		// REST API actions
		$rest_api['method'] = 'POST';
		$rest_api['url'] = asset('master/users');

		return view('admin.Users.editUser', compact(['title', 'post', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest $request
	 * @return redirect
     */
    public function store(UserRequest $request)
    {
		$user = User::create([
			'name'			=> $request->get('name'),
			'email'   		=> $request->get('email'),
			'password' 		=> \Hash::make($request->get('password'))
		]);
		
		return redirect( asset('master/users/' . $user->id) )->with('success', 'Пользователь добавлен');
    }
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Редактирование пользователя';
		
		$post  = User::find( $id );
		
		// REST API actions
		$rest_api['method'] = 'PUT';
		$rest_api['url'] = asset('master/users/' . $id);

		return view('admin.Users.editUser', compact(['title', 'post', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest $request
     * @param  int  $id
     * @return redirect
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
		
		$user->name	 = $request->get('name');
		$user->email = $request->get('email');

		if( $request->get('password') != '' )
			$user->password = \Hash::make( $request->get('password') );

		$user->save();

		return redirect()->back()->with('success', 'Информация успешно обновлена');
    }
	
	/**
     * Remove users
     *
     * @return redirect
     */
    public function destroy(Request $request)
    {
        $users_ids = $request->get('check');

		User::destroy( $users_ids );

		return redirect()->back();
    }
}
