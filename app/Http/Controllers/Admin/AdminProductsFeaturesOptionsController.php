<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Product;
use App\Models\ProductFeature;
use App\Models\ProductFeatureOption;

class AdminProductsFeaturesOptionsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_name = Product::select('as_products.*')
                               ->localName(app()->getLocale())
                               ->where('as_products.id', $id)
                               ->first()
                               ->name;

        $title = 'Выбор характеристик для товара: "' . $product_name . '"';
        
        $features = ProductFeature::with(['parameters' => function($query) {
                                        $query->select([
                                                            'as_products_options.*', 
                                                            'as_products_options_localization.name'
                                                       ])
                                              ->join('as_products_options_localization', 'as_products_options.id', '=', 'as_products_options_localization.option_id')
                                              ->where('as_products_options_localization.lang', '=', app()->getLocale());
                                    }])
                                  ->select('as_products_features.*')
                                  ->localName(app()->getLocale())
                                  ->where('as_products_features.visible', 1)
                                  ->orderBy('as_products_features.position')
                                  ->get();

        $options = ProductFeatureOption::get_options( $id );

        return view('admin.Products.showProductFeaturesOptions', compact(['title', 'features', 'options', 'product_name']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ProductFeatureOption::save_options( $id, $request->get('feature') );
        
        return redirect()->back()->with('success', 'Опции успешно обновлены');
    }
}
