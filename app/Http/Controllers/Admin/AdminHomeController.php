<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

class AdminHomeController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $title = 'Системная информация';

        // Версия MySQL
        $mysql_ver = \DB::select('SELECT VERSION() as mysql_version');
        $mysql_ver = $mysql_ver[0]->mysql_version;
        
        // Свободное место на диске 
        $bytes = disk_free_space('.'); 
        $si_prefix = array('<', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB');
        $base = 1024;
        $class = min((int)log($bytes, $base), count($si_prefix) - 1);
        $total_free_space = sprintf('%1.2f', $bytes / pow($base, $class)) . $si_prefix[$class];

        return view('admin.showMain', compact(['title', 'mysql_ver', 'total_free_space']));
    }
}
