<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MenuRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Menu;
use App\Models\MenuLocalization;

class AdminMenuController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Меню';
        
        $all_menu = Menu::select('as_menu.*')
                        ->localName(app()->getLocale())
                        ->get()
                        ->toHierarchy()
                        ->toArray();

        $menu = json_encode(array_values( $all_menu ));
        
        $menu_count = count($all_menu);
        
        return view('admin.Menu.showMenu', compact(['title', 'menu', 'menu_count']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Добавление пункта меню";

        $post = new Menu;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new MenuLocalization;
        }

        $tree = Menu::localizationTree();
        $parent_id = 0;

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/menu');
       
        return view('admin.Menu.editMenu', compact(['title', 'post', 'data', 'tree', 'parent_id', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        if( $request->parent_id > 0 )
            $parent_id = $request->parent_id;
        else 
            $parent_id = NULL;

        $input = array_merge( $request->except(array_merge(['_token', 'parent_id'], \LaravelLocalization::getSupportedLanguagesKeys())), ['parent_id' => $parent_id]);

        $menu_item = Menu::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $menu_item->localizations()->create( $info );
        }

        return redirect( asset('master/menu/' . $menu_item->id) )->with('success', 'Пункт меню добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование пункта меню";

        $post = Menu::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        $tree = Menu::localizationTree();
        $parent_id = $post->parent_id;

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/menu/' . $id);

        return view('admin.Menu.editMenu', compact(['title', 'post', 'data', 'tree', 'parent_id', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        if( $request->parent_id > 0 )
            $parent_id = $request->parent_id;
        else 
            $parent_id = NULL;

        $input = array_merge( $request->except(array_merge(['_token', 'parent_id'], \LaravelLocalization::getSupportedLanguagesKeys())), ['parent_id' => $parent_id]);  

        $menu_item = Menu::findOrFail($id);
        $menu_item->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $menu_item->localizations()
                          ->whereLang($language->code)
                          ->updateOrCreate(['lang' => $language->code, 'item_id' => $menu_item->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Пункт меню обновлён');
    }

    /**
     * Destroy menu items
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        Menu::destroy( $request->get('check') );
        
        return redirect()->back();
    }

    /**
     * Rebuild menu tree
     *
     * @return JSON answer
     */
    public function rebuild(Request $request)
    {
        Menu::rebuildTree( $request->get('data') );
        
        return \Response::json(['rebuild' => 'rebuilded']);
    }

    /**
     * Visible menu item
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        $id = (int)$request->get('id');

        $menu_item = Menu::find( $id );
        
        $visible = (empty($menu_item->visible) ? 1 : 0);
        
        $menu_item->update(['visible' => $visible]);

        $childrens = [];
        
        if( $child_items = $menu_item->getDescendants() )
        {
            foreach( $child_items as $c_item )
            {
                $c_item->update(['visible' => $visible]);

                $childrens[]['id'] = $c_item->id;
            }
        }
        
        return \Response::json(['visible' => $visible, 'childrens' => $childrens]);
    }
}
