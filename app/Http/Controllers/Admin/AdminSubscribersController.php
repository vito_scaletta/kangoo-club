<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Subscriber;

class AdminSubscribersController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Подписчики';

        $posts = Subscriber::orderBy('created_at', 'DESC')->paginate(25);

        return view('admin.Subscribers.showSubscribers', compact(['title', 'posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Рассылка писем';

        return view('admin.Subscribers.formMailing', compact(['title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( $emails = Subscriber::whereVisible(1)->get()->pluck('email')->toArray() )
        {
            if( $request->get('subject') && $request->get('body') )
            {
                $fields = (object)['subject' => $request->get('subject'), 'body' => $request->get('body')];

                foreach( $emails as $email )
                {
                    \Mail::send([], [], function( $message ) use ( $fields, $email ){
                        $message->to( $email )
                                ->subject( $fields->subject )
                                ->setBody( $fields->body, 'text/html' );
                    });
                }

                return redirect()->back()->withInput()->withSuccess('Рассылка выполнена');
            }
            else
                return redirect()->back()->withInput()->withErrors('Текст письма или тема не заполнены!');
        }
        else
            return redirect()->back()->withInput()->withErrors('У Вас нет подписчиков. Некому отправлять письма, Карл!');
    }

    /**
     * Remove resources from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');
            
        Subscriber::destroy( $ids );

        return redirect()->back();
    }

    /**
     * Visible subscriber
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = Subscriber::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }
}
