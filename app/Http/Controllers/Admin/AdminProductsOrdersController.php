<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\ProductOrder;

class AdminProductsOrdersController extends AdminBaseController
{
	public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Опции';

        $orders = ProductOrder::paginate(25);

        return view('admin.Products.showProductsOrders', compact(['title', 'orders']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		ProductOrder::destroy( $request->get('check') );

        return redirect()->back();
    }

    /**
     * Visible order
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $order = ProductOrder::find( $id );
            
            $visible = (empty($order->visible) ? 1 : 0);
            
            $order->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }
}
