<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Review;

class AdminReviewsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Отзывы';

        $posts = Review::orderBy('created_at', 'DESC')->paginate(25);

        return view('admin.Reviews.showReviews', compact(['title', 'posts']));
    }

    /**
     * Delete reviews
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ids = $request->get('check');
        
        if( $request->get('action') == 'delete' )
            Review::destroy( $ids );

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Редактирование отзыва';
        
        $post = Review::find( $id );

        return view('admin.Reviews.editReview', compact(['title', 'post']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $review = Review::find( $id );
        $review->update( $request->except(['_token']) );

        return redirect()->back()->with('success', 'Информация успешно обновлена');
    }

    /**
     * Visible product
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $review = Review::find( $id );
            
            $visible = (empty($review->visible) ? 1 : 0);
            
            $review->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }
}
