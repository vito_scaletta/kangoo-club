<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\NewsRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\News;
use App\Models\NewsLocalization;

use Carbon\Carbon;

class AdminNewsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Новости';
        
        $posts = News::select('as_news.*')
                     ->localName(app()->getLocale())
                     ->orderBy('as_news.date', 'DESC')
                     ->paginate(25);
        
        return view('admin.News.showPosts', compact(['title', 'posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Создание записи';

        $post = new News;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new NewsLocalization;
        }

        $date = News::getDate( Carbon::now()->toDateTimeString() );

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/news');
        
        return view('admin.News.editPost', compact(['title', 'post', 'data', 'date', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));
        
        if( isset($input['date']) )
            $input['date'] = Carbon::createFromFormat('d.m.y', $input['date'])->toDateTimeString();

        $post = News::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $post->localizations()->create( $info );
        }

        // If upload image
        if( $request->file('image') )
            News::imageUpload( $request->file('image'), $post->id );

        return redirect( asset('master/news/' . $post->id) )->with('success', 'Запись добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование записи";

        $post = News::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        $date = News::getDate( $post->date );

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/news/' . $id);

        return view('admin.News.editPost', compact(['title', 'post', 'data', 'date', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));
        
        if( isset($input['date']) )
            $input['date'] = Carbon::createFromFormat('d.m.y', $input['date'])->toDateTimeString();

        $post = News::findOrFail($id);
        $post->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $post->localizations()
                     ->whereLang($language->code)
                     ->updateOrCreate(['lang' => $language->code, 'post_id' => $post->id], $request->{$language->code});
            }
        }

        // If upload image
        if( $request->file('image') )
            News::imageUpload( $request->file('image'), $id );

        return redirect()->back()->with('success', 'Запись обновлена');
    }

    /**
     * Destroy blog posts
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $posts = News::whereIn('id', $ids)->get() )
        {
            foreach( $posts as $post )
            {
                \File::delete( public_path('uploads/news/' . $post->image) );
            }
        }

        News::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible post
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = News::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }
}
