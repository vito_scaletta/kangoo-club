<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Setting;
use App\Models\Language;

class AdminSettingsController extends AdminBaseController
{
    private $settings;

    public function __construct()
    {
        parent::__construct();

        $this->settings = [
                                'phone'     => old('phone'),
                                'email'     => old('email'),
                                'viber'     => old('viber'),
                                'telegram'  => old('telegram'),
                                'facebook'  => old('facebook'),
                                'instagram' => old('instagram')
                          ];

        $fields_localization = [];
        
        foreach( $this->languages as $language ) 
        {
            // Address
            $fields_localization['address_1_' . $language->code] = old('address_1_' . $language->code);
            $fields_localization['address_2_' . $language->code] = old('address_2_' . $language->code);
           
            // Header slogan and text
            $fields_localization['h_slogan_' . $language->code] = old('h_slogan_' . $language->code);
            $fields_localization['h_text_' . $language->code] = old('h_text_' . $language->code);
            
            // What is Kangoo jumps?
            $fields_localization['s_what_title_' . $language->code] = old('s_what_title_' . $language->code);
            $fields_localization['s_what_text_' . $language->code] = old('s_what_text_' . $language->code);
            
            // What are KJ shoes?
            $areas = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth'];
            foreach( $areas as $area )
            {
                $fields_localization['s_area_' . $area . '_title_' . $language->code] = old('s_area_' . $area . '_title_' . $language->code);
                $fields_localization['s_area_' . $area . '_text_' . $language->code] = old('s_area_' . $area . '_text_' . $language->code);
            }

            // About achievements
            for( $i = 1; $i <= 5; $i++ )
            {
                $fields_localization['achievements_' . $i . '_' . $language->code] = old('achievements_' . $i . '_' . $language->code);
            }

            // A little more about us
            $fields_localization['about_text_1_' . $language->code] = old('about_text_1_' . $language->code);

            // About information
            $fields_localization['about_text_2_' . $language->code] = old('about_text_2_' . $language->code);

            // Main video
            $fields_localization['s_main_video'] = old('s_main_video');
        }  

        $this->settings = (object)array_merge($this->settings, $fields_localization);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Настройки';
        
        $all_settings = Setting::all();
        $settings = $this->settings;

        if( $all_settings )
        {
            foreach( $all_settings as $item ) 
            {
                $settings->{$item->setting_name} = $item->setting_value;
            }
        }
        
        return view('admin.showSettings', compact(['title', 'settings']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return redirect
     */
    public function store(Request $request)
    {
        $languages = $this->languages->pluck('code')->toArray();

        $except_fields_general = array_merge(['_token', 'hash'], $languages);

        Setting::updateSettings( $request->except($except_fields_general) );

        // Update visible languages
        if( $fields_languages = $request->only($languages) )
        {
            foreach( $fields_languages as $fl_key => $fl_value )
            {
                if( in_array($fl_key, \LaravelLocalization::getSupportedLanguagesKeys()) )
                {
                    Language::whereCode($fl_key)->update(['visible' => (int)$fl_value]);
                }
            }
        }

        $url = $request->fullUrl() . (!empty($request->get('hash')) ? '#' . $request->get('hash') : '');

        return redirect($url)->with('success', 'Настройки обновлены');
    }
}
