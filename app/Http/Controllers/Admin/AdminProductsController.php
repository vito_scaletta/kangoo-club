<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Product;
use App\Models\ProductLocalization;
use App\Models\ProductImage;

class AdminProductsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Товары';
        
        $products = Product::select('as_products.*')
                           ->localName(app()->getLocale())
                           ->orderBy('as_products.position')
                           ->get();
        
        return view('admin.Products.showProducts', compact(['title', 'products']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление товара';

        $post = new Product;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new ProductLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/products');

        return view('admin.Products.editProduct', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $product = Product::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $product->localizations()->create( $info );
        }
        
        return redirect( asset('master/products/' . $product->id) )->with('success', 'Товар добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Редактирование товара';
        
        $post = Product::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }
        
        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/products/' . $id);

        return view('admin.Products.editProduct', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $product = Product::find( $id );
        $product->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $product->localizations()
                        ->whereLang($language->code)
                        ->updateOrCreate(['lang' => $language->code, 'product_id' => $product->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация успешно обновлена');
    }

    /**
     * Remove products
     *
     * @return redirect
     */
    public function destroy(Request $request)
    {
        $products_ids = $request->get('check');

        $products_images = ProductImage::whereIn('product_id', $products_ids)->get();
        
        if( count($products_images) )
        {
            foreach( $products_images as $item )
            {
                \File::delete( public_path('uploads/products/' . $item->image) );
                \File::delete( public_path('uploads/products/middle/' . $item->image) );
                \File::delete( public_path('uploads/products/thumbs/' . $item->image) );
            }
        }

        Product::destroy( $products_ids );

        return redirect()->back();
    }

    /**
     * Visible/active product
     *
     * @return JSON answer
     */
    public function toggle(Request $request)
    {
        if( $request->ajax() )
        {
            $product = Product::find( (int)$request->get('id') );
            
            $action = $request->get('action');

            $toggle = (empty($product->{$action}) ? 1 : 0);
            
            $product->update([$action => $toggle]);
            
            return \Response::json([$action => 'changed']);
        }
    }

    /**
     * Sortable products
     *
     * @return JSON answer
     */
    public function sortable( Request $request )
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                Product::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
