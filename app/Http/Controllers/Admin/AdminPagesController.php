<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Page;
use App\Models\PageLocalization;

class AdminPagesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Страницы';
        
        $pages = Page::select('as_pages.*')->localName(app()->getLocale())->paginate(25);
        
        return view('admin.Pages.showPages', compact(['title', 'pages']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Создание страницы';

        $post = new Page;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new PageLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/pages');
        
        return view('admin.Pages.editPage', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $page = Page::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $page->localizations()->create( $info );
        }

        return redirect( asset('master/pages/' . $page->id) )->with('success', 'Страница добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование страницы";

        $post = Page::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/pages/' . $id);

        return view('admin.Pages.editPage', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $page = Page::findOrFail($id);
        $page->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $page->localizations()
                     ->whereLang($language->code)
                     ->updateOrCreate(['lang' => $language->code, 'page_id' => $page->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Страница обновлена');
    }

    /**
     * Destroy menu items
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        Page::destroy( $request->get('check') );
        
        return redirect()->back();
    }
}
