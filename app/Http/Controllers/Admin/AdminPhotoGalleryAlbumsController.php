<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PhotoGalleryAlbumRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\PhotoGalleryAlbum;
use App\Models\PhotoGalleryAlbumLocalization;
use App\Models\PhotoGalleryImage;

class AdminPhotoGalleryAlbumsController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Альбомы';
        
        $albums = PhotoGalleryAlbum::select('as_photo_gallery_albums.*')
                                   ->localName(app()->getLocale())
                                   ->orderBy('as_photo_gallery_albums.position')
                                   ->get();
        
        return view('admin.PhotoGallery.showAlbums', compact(['title', 'albums']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавление альбома';

        $post = new PhotoGalleryAlbum;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new PhotoGalleryAlbumLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/photo-gallery-albums');
        
        return view('admin.PhotoGallery.editAlbum', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoGalleryAlbumRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));

        $album = PhotoGalleryAlbum::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $album->localizations()->create( $info );
        }

        // If upload image
        if( $image = $request->file('image') )
            PhotoGalleryAlbum::imageUpload( $image, $album->id );

        return redirect( asset('master/photo-gallery-albums/' . $album->id) )->with('success', 'Альбом добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование альбома";

        $post = PhotoGalleryAlbum::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/photo-gallery-albums/' . $id);

        return view('admin.PhotoGallery.editAlbum', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhotoGalleryAlbumRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $album = PhotoGalleryAlbum::findOrFail($id);
        $album->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $album->localizations()
                      ->whereLang($language->code)
                      ->updateOrCreate(['lang' => $language->code, 'album_id' => $album->id], $request->{$language->code});
            }
        }

        // If upload image
        if( $image = $request->file('image') )
            PhotoGalleryAlbum::imageUpload( $image, $id );

        return redirect()->back()->with('success', 'Альбом обновлён');
    }

    /**
     * Destroy albums items
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $albums = PhotoGalleryAlbum::whereIn('id', $ids)->get() )
        {
            foreach( $albums as $album )
            {
                \File::delete( public_path('uploads/photo-gallery/albums/' . $album->image) );
            }

            if( $images = PhotoGalleryImage::whereIn('album_id', $ids)->get() )
            {
                foreach( $images as $item )
                {
                    \File::delete( public_path('uploads/photo-gallery/images/' . $item->image) );
                    \File::delete( public_path('uploads/photo-gallery/images/middle/' . $item->image) );
                }
            }
        }

        PhotoGalleryAlbum::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible/active album item
     *
     * @return JSON answer
     */
    public function toggle(Request $request)
    {
        if( $request->ajax() )
        {
            $album = PhotoGalleryAlbum::find( (int)$request->get('id') );
            
            $action = $request->get('action');

            $toggle = (empty($album->{$action}) ? 1 : 0);
            
            $album->update([$action => $toggle]);
            
            return \Response::json([$action => 'changed']);
        }
    }

    /**
     * Albums sortable
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                PhotoGalleryAlbum::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}
