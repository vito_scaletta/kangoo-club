<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;

use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\Article;
use App\Models\ArticleLocalization;

class AdminArticlesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Статьи';
        
        $posts = Article::select('as_articles.*')
                        ->localName(app()->getLocale())
                        ->orderBy('as_articles.id', 'DESC')
                        ->paginate(25);
        
        return view('admin.Articles.showPosts', compact(['title', 'posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Создание записи';

        $post = new Article;
        $post->visible = 1;

        $data = [];
        foreach( $this->languages as $language )
        {
            $data[$language->code] = new ArticleLocalization;
        }

        // REST API actions
        $rest_api['method'] = 'POST';
        $rest_api['url'] = asset('master/articles');
        
        return view('admin.Articles.editPost', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));
        
        $post = Article::create( $input );

        foreach( $this->languages as $language )
        {
            $info = $request->{$language->code};
            $info['lang'] = $language->code;
            $post->localizations()->create( $info );
        }

        // If upload image
        if( $request->file('image') )
            Article::imageUpload( $request->file('image'), $post->id );

        return redirect( asset('master/articles/' . $post->id) )->with('success', 'Запись добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Редактирование записи";

        $post = Article::whereId($id)->with('localizations')->firstOrFail();
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        // REST API actions
        $rest_api['method'] = 'PUT';
        $rest_api['url'] = asset('master/articles/' . $id);

        return view('admin.Articles.editPost', compact(['title', 'post', 'data', 'rest_api']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $input = $request->except(array_merge(['_token', 'image'], \LaravelLocalization::getSupportedLanguagesKeys()));
        
        $post = Article::findOrFail($id);
        $post->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $post->localizations()
                     ->whereLang($language->code)
                     ->updateOrCreate(['lang' => $language->code, 'post_id' => $post->id], $request->{$language->code});
            }
        }

        // If upload image
        if( $request->file('image') )
            Article::imageUpload( $request->file('image'), $id );

        return redirect()->back()->with('success', 'Запись обновлена');
    }

    /**
     * Destroy blog posts
     *
     * @return redirect back
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('check');

        if( $posts = Article::whereIn('id', $ids)->get() )
        {
            foreach( $posts as $post )
            {
                \File::delete( public_path('uploads/articles/' . $post->image) );
            }
        }

        Article::destroy( $ids );
        
        return redirect()->back();
    }

    /**
     * Visible post
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $post = Article::find( $id );
            
            $visible = (empty($post->visible) ? 1 : 0);
            
            $post->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }
}
