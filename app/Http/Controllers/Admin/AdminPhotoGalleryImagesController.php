<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;

use App\Models\PhotoGalleryImage;
use App\Models\PhotoGalleryImageLocalization;
use App\Models\PhotoGalleryAlbum;

class AdminPhotoGalleryImagesController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except(array_merge(['_token', 'image_id'], \LaravelLocalization::getSupportedLanguagesKeys()));  

        $image = PhotoGalleryImage::findOrFail((int)$request->get('image_id'));
        $image->update( $input );

        foreach( $this->languages as $language )
        {
            if( $language->visible )
            {  
                $image->localizations()
                      ->whereLang($language->code)
                      ->updateOrCreate(['lang' => $language->code, 'image_id' => $image->id], $request->{$language->code});
            }
        }

        return redirect()->back()->with('success', 'Информация обновлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $album_id
     * @return \Illuminate\Http\Response
     */
    public function show($album_id)
    {
        $album_name = PhotoGalleryAlbum::select('as_photo_gallery_albums.*')
                                       ->localName(app()->getLocale())
                                       ->find($album_id)
                                       ->name;

        $title = 'Изображения альбома: "' . $album_name . '"';
        
        $images = PhotoGalleryImage::whereAlbumId($album_id)
                                   ->orderBy('position')
                                   ->get();
        
        return view('admin.PhotoGallery.showImages', compact(['title', 'images', 'album_name', 'album_id']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $image_id
     * @return \Illuminate\Http\Response
     */
    public function edit($image_id)
    {
        $title = 'Редактирование данных изображения ID - ' . $image_id;

        $post = PhotoGalleryImage::whereId($image_id)->with('localizations')->first();

        $album = PhotoGalleryAlbum::select('as_photo_gallery_albums.*')
                                  ->localName(app()->getLocale())
                                  ->find($post->album_id);
        
        $data = [];
        foreach( $post->localizations as $loc )
        {
            $data[$loc->lang] = $loc;
        }

        return view('admin.PhotoGallery.editImage', compact(['title', 'post', 'data', 'album', 'image_id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( $errors = PhotoGalleryImage::upload_images($request->file('images'), $id) )
            return redirect()->back()->with('errors', $errors);
        else
            return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $images_ids = $request->get('check');

        $images = PhotoGalleryImage::whereIn('id', $images_ids)->get();
        
        if( count($images) )
        {
            foreach( $images as $item )
            {
                \File::delete( public_path('uploads/photo-gallery/images/' . $item->image) );
                \File::delete( public_path('uploads/photo-gallery/images/middle/' . $item->image) );
            }
        }

        PhotoGalleryImage::destroy( $images_ids );

        return redirect()->back();
    }

    /**
     * Visible image
     *
     * @return JSON answer
     */
    public function visible(Request $request)
    {
        if( $request->ajax() )
        {
            $id = (int)$request->get('id');

            $image = PhotoGalleryImage::find( $id );
            
            $visible = (empty($image->visible) ? 1 : 0);
            
            $image->update(['visible' => $visible]);
            
            return \Response::json(['visible' => 'changed']);
        }
    }

    /**
     * Sortable images
     *
     * @return JSON answer
     */
    public function sortable(Request $request)
    {
        if( $request->ajax() )
        {
            (int)$i = 1;
            
            foreach( $request->get('position') as $item )
            {
                PhotoGalleryImage::find( $item )->update(['position' => $i]);
                $i++;
            }

            return \Response::json(['sortable' => 'changed']);
        }
    }
}