<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsLocalization extends Model
{
    protected $table = 'as_news_localization';

	protected $guarded = [];
}
