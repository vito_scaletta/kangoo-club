<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoGalleryAlbumLocalization extends Model
{
    protected $table = 'as_video_gallery_albums_localization';
	
	protected $guarded = [];
}
