<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificateLocalization extends Model
{
    protected $table = 'as_certificates_localization';

	protected $guarded = [];
}
