<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductLocalization extends Model
{
    protected $table = 'as_products_localization';
	
	protected $guarded = [];
}
