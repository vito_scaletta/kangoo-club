<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerLocalization extends Model
{
    protected $table = 'as_partners_localization';

	protected $guarded = [];
}
