<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoGalleryAlbum extends Model
{
    protected $table = 'as_video_gallery_albums';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\VideoGalleryAlbumLocalization', 'album_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_video_gallery_albums_localization', 'as_video_gallery_albums.id', '=', 'as_video_gallery_albums_localization.album_id')
                     ->where('as_video_gallery_albums_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_video_gallery_albums_localization.name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_video_gallery_albums_localization.name',
    				 				'as_video_gallery_albums_localization.h1',
    				 				'as_video_gallery_albums_localization.body',
    				 				'as_video_gallery_albums_localization.meta_title',
    				 				'as_video_gallery_albums_localization.meta_keywords',
    				 				'as_video_gallery_albums_localization.meta_description'
    				 			);  
    }

    // Upload image
    public static function imageUpload( $file, $id )
    {
        $post = self::find( $id );

        if( $post->image )
            \File::delete( public_path('uploads/video-gallery/albums/' . $post->image) );
        
        $new_image = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();

        $image_gd = \Image::make( $file );

        if( $image_gd->width() > 1135 )
            $image_gd->widen( 1135 )->save( 'uploads/video-gallery/albums/' . $new_image );
        else
            $file->move( public_path('uploads/video-gallery/albums/'), $new_image );
        
        $post->update(['image' => $new_image]);
    }
}
