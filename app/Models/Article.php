<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Article extends Model
{
    protected $table = 'as_articles';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\ArticleLocalization', 'post_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_articles_localization', 'as_articles.id', '=', 'as_articles_localization.post_id')
                     ->where('as_articles_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_articles_localization.name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_articles_localization.name',
    				 				'as_articles_localization.h1',
                                    'as_articles_localization.annotation',
    				 				'as_articles_localization.body',
    				 				'as_articles_localization.meta_title',
    				 				'as_articles_localization.meta_keywords',
    				 				'as_articles_localization.meta_description'
    				 			);  
    }

    // Upload image
    public static function imageUpload( $file, $id )
    {
        $post = self::find( $id );

        if( $post->image )
            \File::delete( public_path('uploads/articles/' . $post->image) );
        
        $new_image = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();

        $image_gd = \Image::make( $file );

        if( $image_gd->width() > 255 )
            $image_gd->widen( 255 )->save( 'uploads/articles/' . $new_image );
        else
            $file->move( public_path('uploads/articles/'), $new_image );
        
        $post->update(['image' => $new_image]);
    }
}
