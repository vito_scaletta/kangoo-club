<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'as_team';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\TeamLocalization', 'post_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_team_localization', 'as_team.id', '=', 'as_team_localization.post_id')
                     ->where('as_team_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_team_localization.first_name')
                     ->addSelect('as_team_localization.last_name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_team_localization.first_name',
    				 				'as_team_localization.last_name',
    				 				'as_team_localization.role'
    				 			);  
    }

    // Upload image
    public static function imageUpload( $file, $id )
    {
        $post = self::find( $id );

        if( $post->image )
            \File::delete( public_path('uploads/team/' . $post->image) );
        
        $new_image = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();

        $image_gd = \Image::make( $file );

        if( $image_gd->width() > 350 )
            $image_gd->widen( 350 )->save( 'uploads/team/' . $new_image );
        else
            $file->move( public_path('uploads/team/'), $new_image );
        
        $post->update(['image' => $new_image]);
    }
}
