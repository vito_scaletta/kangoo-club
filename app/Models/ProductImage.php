<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'as_products_images';
	
	protected $guarded = [];

	// Upload images project
    public static function upload_images( $images, $produt_id )
    {
    	$errors  = '';
    	$counter = 1;

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				\Image::make( $image )->save( 'uploads/products/' . $image_name );
				\Image::make( $image )->heighten( 290 )->save( 'uploads/products/middle/' . $image_name );
				\Image::make( $image )->heighten( 55 )->save( 'uploads/products/thumbs/' . $image_name );

				self::create(['product_id' => $produt_id, 'image' => $image_name]);
			}
			else
				$errors = $validator->messages();

			$counter++;
		}
		
		return $errors;
	}
}
