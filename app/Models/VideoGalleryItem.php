<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Language;
use App\Models\VideoGalleryItemLocalization;

class VideoGalleryItem extends Model
{
     protected $table = 'as_video_gallery_items';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\VideoGalleryItemLocalization', 'item_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_video_gallery_items_localization', 'as_video_gallery_items.id', '=', 'as_video_gallery_items_localization.item_id')
                     ->where('as_video_gallery_items_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_video_gallery_items_localization.name');
    }

    // Upload items
    public static function upload_items( $items, $album_id )
    {
		$errors  = '';

		$languages = Language::get();

		foreach( $items as $item )
		{
			$validator = \Validator::make( ['items' => $item], ['items' => 'required|image']);
			
			if( $validator->passes() )
			{
				$item_name = time() . '_' . \Slug::make(basename($item->getClientOriginalName(), '.' . $item->getClientOriginalExtension())) . '.' . $item->getClientOriginalExtension();

				$item_gd = \Image::make( $item );

				$item_gd->widen( 360 )->save( 'uploads/video-gallery/images/' . $item_name );

				$item_current = self::create(['album_id' => $album_id, 'image' => $item_name]);

				foreach( $languages as $language )
		        {
		            VideoGalleryItemLocalization::create(['item_id' => $item_current->id, 'lang' => $language->code]);
		        }
			}
			else
				$errors = $validator->messages();
		}
		
		return $errors;
    }

    // Get youtube video code
	public static function videoCode( $video_url ) 
	{
		parse_str( parse_url($video_url, PHP_URL_QUERY), $code );
		
		return $code['v'];
	}
}
