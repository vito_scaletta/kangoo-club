<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Baum\Node;

class Menu extends Node
{
	protected $table = 'as_menu';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\MenuLocalization', 'item_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_menu_localization', 'as_menu.id', '=', 'as_menu_localization.item_id')
                     ->where('as_menu_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_menu_localization.name');
    }

	public static function localizationTree()
	{
    	$items_tree = self::getNestedList('id', NULL, '&mdash;&nbsp;');
    	$items_name = self::select('as_menu.id')->localName(app()->getLocale())->pluck('name', 'id')->toArray();
    	
    	foreach( $items_tree as $it_key => $it_val ) 
    	{
    		foreach( $items_name as $in_key => $in_val ) 
    		{
    			if( $it_key == $in_key ) 
    			{
    				$items_tree[$it_key]  = preg_replace("/\d{1,10000}/", "", $items_tree[$it_key]);
    				$items_tree[$it_key] .= $in_val;
    			}
    		}
    	}
		
		$items_tree = ['0' => '-'] + $items_tree;

        return $items_tree;
    }

    // Item menu
    public static function itemMenu( $name, $slug ) 
    {
        $localized_url = \LaravelLocalization::getLocalizedURL(app()->getLocale(), (empty($slug) ? '/' : $slug));

        if( \Request::url() == rtrim($localized_url, "/") )
            return '<span>' . $name . '</span>';
        else
            return '<a href="' . $localized_url . '">' . $name . '</a>';
    }
}
