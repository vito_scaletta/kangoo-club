<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleLocalization extends Model
{
    protected $table = 'as_articles_localization';

	protected $guarded = [];
}
