<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Stock extends Model
{
    protected $table = 'as_stock';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\StockLocalization', 'post_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_stock_localization', 'as_stock.id', '=', 'as_stock_localization.post_id')
                     ->where('as_stock_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_stock_localization.name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_stock_localization.name',
    				 				'as_stock_localization.h1',
                                    'as_stock_localization.annotation',
    				 				'as_stock_localization.body',
    				 				'as_stock_localization.meta_title',
    				 				'as_stock_localization.meta_keywords',
    				 				'as_stock_localization.meta_description'
    				 			);  
    }

    // Upload image
    public static function imageUpload( $file, $id )
    {
        $post = self::find( $id );

        if( $post->image )
            \File::delete( public_path('uploads/stock/' . $post->image) );
        
        $new_image = time() . '_' . \Slug::make(basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension())) . '.' . $file->getClientOriginalExtension();

        $image_gd = \Image::make( $file );

        if( $image_gd->width() > 278 )
            $image_gd->widen( 278 )->save( 'uploads/stock/' . $new_image );
        else
            $file->move( public_path('uploads/stock/'), $new_image );
        
        $post->update(['image' => $new_image]);
    }

    // Get correct date
    public static function getDate( $datetime )
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $datetime)->format('d.m.y');
    }
}
