<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table = 'as_products_options';
	
	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\ProductOptionLocalization', 'option_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_products_options_localization', 'as_products_options.id', '=', 'as_products_options_localization.option_id')
                     ->where('as_products_options_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_products_options_localization.name');
    }
}
