<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureLocalization extends Model
{
    protected $table = 'as_products_features_localization';
	
	protected $guarded = [];
}
