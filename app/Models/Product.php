<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'as_products';
	
	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\ProductLocalization', 'product_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_products_localization', 'as_products.id', '=', 'as_products_localization.product_id')
                     ->where('as_products_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_products_localization.name');
    }

	public function product_images()
	{
		return $this->hasMany( 'App\Models\ProductImage', 'product_id' );
	}
	
	public function product_features()
	{
		return $this->hasMany( 'App\Models\ProductFeatureOption', 'product_id' );
    }

    public static function renderProductFeatures( $features )
    {
    	$features_group = [];

    	if( count($features) )
    	{
    		foreach( $features as $f_key => $f_value )
    		{
    			if( $f_key <= 3 )
    			{
    				$features_group[$f_value->feature_name][] = $f_value->option_name;
    			}
    		}
    	}

    	$features_render = ''; 

    	if( count($features_group) )
    	{
    		foreach( $features_group as $fg_key => $fg_value )
    		{
    			$features_render .= '<li><span>' . $fg_key . ':</span> ' . implode(', ', $fg_value) . '</li>';
    		}
    	}

    	return $features_render;
    }
}
