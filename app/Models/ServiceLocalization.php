<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceLocalization extends Model
{
    protected $table = 'as_services_localization';

	protected $guarded = [];
}
