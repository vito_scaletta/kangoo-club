<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoGalleryItemLocalization extends Model
{
    protected $table = 'as_video_gallery_items_localization';
	
	protected $guarded = [];
}
