<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFeatureOption extends Model
{
    protected $table = 'as_products_features_options';
	
	protected $guarded = [];

	// Return the selected item options
	public static function get_options( $id )
	{
		$options = [];

		$options_data = self::whereProductId( $id )->get();

		foreach( $options_data as $option )
		{
			if( isset($options[$option->feature_id]) )
				array_push($options[$option->feature_id], $option->option_id );
			else
				$options[$option->feature_id] = [$option->option_id];
		}
		
		return $options;
	}
	
	// Save product options
	public static function save_options( $id, $feature )
	{
		self::whereProductId( $id )->delete();

		if( isset($feature) )
		{
			foreach( $feature as $feature_id => $option_id )
			{
				foreach($option_id as $item_id)
				{
					if( $item_id != '' )
					{
						$insert[] = [
										'product_id' => $id, 
										'feature_id' => $feature_id, 
										'option_id'	 => $item_id 
									];
					}
				}
			}

			if( isset( $insert ) )
				self::insert( $insert );
		}
	}
}
