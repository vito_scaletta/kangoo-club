<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockLocalization extends Model
{
    protected $table = 'as_stock_localization';

	protected $guarded = [];
}
