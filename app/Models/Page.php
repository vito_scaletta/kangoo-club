<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'as_pages';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\PageLocalization', 'page_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_pages_localization', 'as_pages.id', '=', 'as_pages_localization.page_id')
                     ->where('as_pages_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_pages_localization.name');
    }

	public function scopeLocalAll( $query, $lang )
	{
		return $query->local($lang)
    				 ->addSelect(
    				 				'as_pages_localization.name',
    				 				'as_pages_localization.h1',
    				 				'as_pages_localization.body',
    				 				'as_pages_localization.meta_title',
    				 				'as_pages_localization.meta_keywords',
    				 				'as_pages_localization.meta_description'
    				 			);  
    }
}
