<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
	protected $table = 'as_certificates';

	protected $guarded = [];

	public function localizations()
	{
		return $this->hasMany('App\Models\CertificateLocalization', 'certificate_id');
	}

	public function scopeLocal( $query, $lang )
    {
        return $query->join('as_certificates_localization', 'as_certificates.id', '=', 'as_certificates_localization.certificate_id')
                     ->where('as_certificates_localization.lang', '=', $lang);
    }

    public function scopeLocalName( $query, $lang )
    {
        return $query->local($lang)
                     ->addSelect('as_certificates_localization.name');
    }

    // Upload images
    public static function upload_images( $images )
    {
		$errors  = '';

		$languages = Language::get();

		foreach( $images as $image )
		{
			$validator = \Validator::make( ['images' => $image], ['images' => 'required|image']);
			
			if( $validator->passes() )
			{
				$image_name = time() . '_' . \Slug::make(basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension())) . '.' . $image->getClientOriginalExtension();

				$image_gd = \Image::make( $image );

				$image_gd->save( 'uploads/certificates/' . $image_name );
				$image_gd->widen( 360 )->save( 'uploads/certificates/middle/' . $image_name );

				$image_current = self::create(['image' => $image_name]);

				foreach( $languages as $language )
		        {
		            CertificateLocalization::create(['certificate_id' => $image_current->id, 'lang' => $language->code]);
		        }
			}
			else
				$errors = $validator->messages();
		}
		
		return $errors;
    }
}
