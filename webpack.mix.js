let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/frontend/css')
   .options({
		processCssUrls: false
   })
   .styles([
   		'resources/assets/css/normalize.css',
   		'resources/assets/css/bootstrap-grid.css',
         'resources/assets/css/magnific-popup.css',
         'resources/assets/css/sweetalert2.css',
   		'public/frontend/css/app.css'
   	], 'public/frontend/css/all.css')
   .babel([
		'resources/assets/js/jquery-3.2.1.js',
      'resources/assets/js/3dslider.js',
      'resources/assets/js/pushy.js',
      'resources/assets/js/slick.js',
      'resources/assets/js/jquery.magnific-popup.js',
      'resources/assets/js/jquery.waterwheelCarousel.js',
      'resources/assets/js/jquery.maskedinput.js',
      'resources/assets/js/jquery.scrollTo.js',
      'resources/assets/js/sweetalert2.js',
		'resources/assets/js/app.js'
	], 'public/frontend/js/all.js');