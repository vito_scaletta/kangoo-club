<?php

return [

	// Global
	'home'						=> 'Главная',
	'callback'					=> 'Заказать обратный звонок',
	'callback_title'			=> 'Заказ обратного звонка',
	'order_product'				=> 'Заказ товара',
	'sitemap'					=> 'Карта сайта',
	'all_rights_reserved'		=> 'все права защищены',
	'site_creation'				=> 'создание сайта',
	'subscribe_title'			=> 'Подпишитесь на нашу рассылку',
	'subscribe_description'		=> 'и мы каждую неделю сможем радовать вас новостями',

	// Main
	'more_information'			=> 'Больше информации',
	'more'						=> 'Подробнее',
	'shoe'						=> 'Ботинки',
	'price'						=> 'Цена',
	'currency'					=> '€',
	'section_kj_title'			=> 'Что представляют собой ботинки KJ?',
	'section_kj_description'	=> 'Наведите на интересующую область, чтобы получить информацию',
	'section_cb_title'			=> 'Интересуют какие-то вопросы?',
	'section_cb_description'	=> 'Просто напишите свои данные, мы Вам перезвоним и ответим на все вопросы',
	'section_cb_notice'			=> '* наши консультанты позвонят Вам в течении нескольких минут',
	'callback_text'				=> 'Если у Вас есть вопросы, Вы можете заказать обратный звонок и мы Вам на них ответим :)',
	'section_gallery_title'		=> 'Фото наших залов',
	'map_title'					=> 'Где мы находимся?',
	'contacts'					=> 'Контакты',
	'services'					=> 'Услуги',
	'section_video_title'		=> 'Видео презентация Kangoo Jump',
	'section_video_btn_text'	=> 'Смотреть видео',

	// About
	'about_title_1'				=> 'Наши преимущества',
	'about_title_2'				=> 'Ещё немного о нас',
	'about_cb_title'			=> 'Запишитесь на тренировку',
	'about_cb_description'		=> 'И получите море эмоций и впечатлений',
	'about_cb_before_phone'		=> 'Вы можете записаться по телефону:',
	'about_cb_after_phone'		=> 'Или закажите обратный звонок и мы перезвоним Вам',
	'about_information'			=> 'Информация',

	// Price
	'price_before'				=> 'Здесь Вы можете ознакомиться с нашим прайсом',
	'price_after'				=> '* все цены являются актуальными',
	'price_cb_text'				=> 'Теперь, когда Вы ознакомились с ценами,<br/> Вы можете заказать бесплатную консультацию по телефону и мы ответим Вам на все вопросы',

	// 404
	'error_404_title'			=> 'Простите, страница упрыгала',
	'error_404_description'		=> 'Мы тоже ее ждем, а пока Вы можете исследовать другие разделы нашего сайта',

	// Gallery
	'gallery_album'					=> 'Альбом',
	'gallery_album_select'			=> 'Перейти к выбору альбомов',
	'gallery_album_select_photo'	=> 'Выберите альбом для просмотра фотографий',
	'gallery_watch_video'			=> 'Также Вы можете посмотреть нашу видеогалерею',
	'gallery_album_select_video'	=> 'Выберите альбом для просмотра видео',
	'gallery_watch_photo'			=> 'Также Вы можете посмотреть нашу фотогалерею',

	// Reviews
	'reviews_description'		=> 'Впечатления наших довольных клиентов',
	'reviews_form_title'		=> 'Оставить отзыв',

	// Partners
	'partners_description'		=> 'Организации, которые с нами сотрудничают',

	// Search
	'search_title'				=> 'Результаты поиска по запросу',

	// Form
	'input_search'				=> 'Поиск по сайту',
	'input_email'				=> 'Ваш e-mail',
	'input_name'				=> 'Ваше имя',
	'input_first_name'			=> 'Имя',
	'input_last_name'			=> 'Фамилия',
	'input_phone'				=> 'Номер телефона',
	'textarea_comment'			=> 'Напишите текст комментария',
	'btn_consultation'			=> 'Получить консультацию',
	'btn_order_consultation'	=> 'Заказать консультацию',
	'btn_services_all'			=> 'Узнать больше о каждой услуге',
	'btn_order'					=> 'Заказать',
	'btn_products_all'			=> 'Смотреть весь ассортимент',
	'btn_gallery_all'			=> 'Перейти в фотогалерею',
	'btn_subscribe'				=> 'Подписаться',
	'btn_callback'				=> 'Перезвоните мне',
	'btn_show_information'		=> 'Раскрыть информацию',
	'btn_hide_information'		=> 'Скрыть информацию',
	'btn_roll_up'				=> 'Скрыть',
	'btn_read_more'				=> 'Читать полностью',
	'btn_back'					=> 'Вернуться на главную',
	'btn_show_more'				=> 'Показать ещё',
	'btn_show_comment'			=> 'Раскрыть комментарий',
	'btn_hide_comment'			=> 'Скрыть комментарий',
	'btn_add_comment'			=> 'Оставить комментарий',
	'btn_more'					=> 'Узнать больше',
	'btn_to_record'				=> 'К записи',

	// Messages
	'message_feedback'			=> 'Ваше сообщение отправлено. Ожидайте, с Вами свяжутся',
	'message_order'				=> 'Ваш заказ принят. Ожидайте, с Вами свяжутся',
	'message_review'			=> 'Спасибо за Ваш отзыв, он будет опубликован сразу же после проверки модератором',
	'subscribe_success'			=> 'Вы подписались на нас'
];