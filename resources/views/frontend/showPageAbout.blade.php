@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
	</div>
	<div class="about">
		<div class="a-preview">
			<div class="ap-left">
				<img src="{{ asset('frontend/img/about_img.jpg') }}" alt="" class="ap-img" />
			</div>
			<div class="ap-right">
				
				@if( $page->body  )
					<div class="user-content">
						{!! $page->body !!}
					</div>
				@endif

			</div>
		</div>
		
		@if( $settings->{'achievements_1_' . app()->getLocale()} || $settings->{'achievements_2_' . app()->getLocale()} || $settings->{'achievements_3_' . app()->getLocale()} || $settings->{'achievements_4_' . app()->getLocale()} )
			<div class="container">
		    	<div class="row">
					<div class="col-12">
					    <div class="page-title">{{ trans('design.about_title_1') }}</div>
					</div>
				</div>
			</div>
			<ul class="a-progress">
				@if( $settings->{'achievements_1_' . app()->getLocale()} )
					<li>
						<div class="apr-item icon-boots"></div>
						<div class="apr-title">{{ $settings->{'achievements_1_' . app()->getLocale()} }}</span>
					</li>
				@endif
				@if( $settings->{'achievements_2_' . app()->getLocale()} )
					<li>
						<div class="apr-item icon-location"></div>
						<div class="apr-title">{{ $settings->{'achievements_2_' . app()->getLocale()} }}</span>
					</li>
				@endif
				@if( $settings->{'achievements_3_' . app()->getLocale()} )
					<li>
						<div class="apr-item icon-sofa"></div>
						<div class="apr-title">{{ $settings->{'achievements_3_' . app()->getLocale()} }}</span>
					</li>
				@endif
				@if( $settings->{'achievements_4_' . app()->getLocale()} )
					<li>
						<div class="apr-item icon-direction"></div>
						<div class="apr-title">{{ $settings->{'achievements_4_' . app()->getLocale()} }}</span>
					</li>
				@endif
				@if( $settings->{'achievements_5_' . app()->getLocale()} )
					<li>
						<div class="apr-item icon-people"></div>
						<div class="apr-title">{{ $settings->{'achievements_5_' . app()->getLocale()} }}</span>
					</li>
				@endif
			</ul>
		@endif

		@if( $settings->{'about_text_1_' . app()->getLocale()} )
			<div class="container">
		    	<div class="row">
					<div class="col-12">
					    <div class="page-title">{{ trans('design.about_title_2') }}</div>
					    <div class="user-content with-bottom">
					    	{!! $settings->{'about_text_1_' . app()->getLocale()} !!}
					    </div>
					</div>
				</div>
			</div>
		@endif

		{!! Form::open(['url' => asset('feedback'), 'class' => 'a-callback']) !!}
			<input type="hidden" name="locale" value="{{ app()->getLocale() }}" />
			<div class="container">
	    		<div class="row">
					<div class="col-12">
						<div class="ac-title">{{ trans('design.about_cb_title') }}</div>
						<div class="ac-subtitle">{{ trans('design.about_cb_description') }}</div>
						<div class="ac-before-phone">{{ trans('design.about_cb_before_phone') }}</div>
						@if( $settings->phone )
							<a href="tel:{{ str_replace(' ', '', $settings->phone) }}" class="ac-phone">{{ $settings->phone }}</a>
						@endif
						<div class="ac-after-phone">{{ trans('design.about_cb_after_phone') }}</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-xl-4">
						<input type="text" name="name" class="input" placeholder="{{ trans('design.input_name') }}" required />
					</div>
					<div class="col-lg-4 col-xl-4">
						<input type="tel" name="phone" class="input" placeholder="{{ trans('design.input_phone') }}" required />
					</div>
					<div class="col-lg-4 col-xl-4">
						<button type="submit">{{ trans('design.btn_callback') }}</button>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12">
						<div class="ac-after-fields">
							{{ trans('design.section_cb_notice') }}
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}

		@if( $settings->{'about_text_2_' . app()->getLocale()} )
			<div class="container">
		    	<div class="row">
					<div class="col-12">
					    <div class="page-title">{{ trans('design.about_information') }}</div>
					    <div class="user-content with-bottom">
					    	{!! $settings->{'about_text_2_' . app()->getLocale()} !!}
					    </div>
					</div>
				</div>
			</div>
		@endif

	</div>

@endsection