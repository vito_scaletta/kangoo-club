@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">

				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')

				<h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>

		@if( count($certificates) )
			<div class="certificates">
				@foreach( $certificates as $c_key => $c_value )
					@if( (($c_key + 1) % 3) == 1 )
						<div class="row">
					@endif
					<div class="col-md-4 col-lg-4 col-xl-4">
						<a href="{{ asset('uploads/certificates/' . $c_value->image) }}" class="c-item">
							<img src="{{ asset('uploads/certificates/middle/' . $c_value->image) }}" alt="{{{ $c_value->name }}}" />
							@if( $c_value->name )
								<div class="c-title">
									{{ $c_value->name }}	
								</div>
							@endif
						</a>
					</div>
					@if( (($c_key + 1) % 3) == 0 || ($c_key + 1) == $certificates->count() )
						</div>
					@endif
				@endforeach
			</div>
		@endif

		@if( $page->body )
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		@endif

	</div>

@endsection