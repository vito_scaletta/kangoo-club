@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-xl-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')

			    <div class="gallery-title">
			    	<div class="gt-type">{{ trans('design.gallery_album') }}</div>
			    	<div class="gt-name">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</div>
			    </div>
			    <a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'video-gallery') }}" class="gallery-back"><span>{{ trans('design.gallery_album_select') }}</span></a>
			    
			     @if( count($items) )
				    <ul class="gallery-items">
				    	@foreach( $items as $item )
					    	<li>
					    		<a href="{{ (!empty($item->video) ? $item->video : 'https://www.youtube.com/watch?v=i9AHJkHqkpw') }}" @if( $item->name )title="{{ $item->name }}"@endif style="background-image: url({{ asset('uploads/video-gallery/images/' . $item->image) }})" class="video">
					    			<span class="gi-type"></span>
					    			@if( $item->name )
					    				<span class="gi-name">{{ $item->name }}</span>
					    			@endif
					    		</a>
					    	</li>
				    	@endforeach;
				    </ul>
			    	{!! $items->render() !!}
				@endif

				@if( $page->body )
					<div class="user-content">
						{!! $page->body !!}
					</div>
				@endif

			</div>
		</div>
	</div>
	<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'photo-gallery') }}" class="btn-items-more"><span>{{ trans('design.gallery_watch_photo') }}</span></a>

@endsection