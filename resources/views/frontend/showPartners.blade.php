@extends('frontend.layout')

@section('main')
	
	<div class="partners">
		<div class="container">
	    	<div class="row">
				<div class="col-12">
					
					<!-- Breadcrumbs -->
					@include('frontend.components._breadcrumbs')

				    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
				    <div class="page-description">{{ trans('design.partners_description') }}</div>
				</div>
			</div>

			@if( count($partners) )
				@foreach( $partners as $p_key => $p_value )
					@if( (($p_key + 1) % 4) == 1 )
						<div class="row">
					@endif
					<div class="col-md-6 col-lg-3 col-xl-3">
						<div class="p-item">
							<div class="pi-image">
								@if( $p_value->image )
									<img src="{{ asset('uploads/partners/' . $p_value->image) }}" alt="{{{ (!empty($p_value->name) ? $p_value->name : '') }}}" />
								@endif
							</div>
							@if( $p_value->name )
								<div class="pi-name">{{ $p_value->name }}</div>
							@endif
						</div>
					</div>
					@if( (($p_key + 1) % 4) == 0 || ($p_key + 1) == $partners->count() )
						</div>
					@endif
				@endforeach
			@endif

			@if( $page->body )
				<div class="row">
					<div class="col-12">
						<div class="user-content with-bottom">
							{!! $page->body !!}
						</div>
					</div>
				</div>
			@endif

		</div>
	</div>

@endsection