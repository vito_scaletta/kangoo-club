<ul class="breadcrumbs">
    <li><a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}">{{ trans('design.home') }}</a></li>
    @if( isset($breadcrumbs) && count($breadcrumbs) )
        @foreach( $breadcrumbs as $crumb )
            <li><a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), $crumb->url) }}">{{ $crumb->name }}</a></li>
        @endforeach
    @endif
    <li><span>{{ $page->name }}</span></li>
</ul>