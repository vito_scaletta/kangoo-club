@php
    $class = isset($class) ? 'section-callback align-items-center ' . $class : 'section-callback align-items-center';
@endphp
{!! Form::open(['url' => asset('feedback'), 'class' => $class]) !!}
    <input type="hidden" name="locale" value="{{ app()->getLocale() }}" />
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="title">{{ trans('design.section_cb_title') }}</div>
                <div class="description">{{ trans('design.section_cb_description') }}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-xl-4">
                <input type="text" name="name" class="input" placeholder="{{ trans('design.input_name') }}" required />
            </div>
            <div class="col-lg-4 col-xl-4">
                <input type="tel" name="phone" class="input" placeholder="{{ trans('design.input_phone') }}" required />
            </div>
            <div class="col-lg-4 col-xl-4">
                <button type="submit">{{ trans('design.btn_callback') }}</button>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="after-text">
                    {{ trans('design.section_cb_notice') }}
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}