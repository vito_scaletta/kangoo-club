<div class="h-container">
    <div class="hc-top">
        <div class="f-left">
            <!-- Logo -->
            @if( $front_page )
                <div class="logo">
            @else
                <a href="{{ LaravelLocalization::getLocalizedURL(App::getLocale(), '/') }}" class="logo">
            @endif
                @include('frontend.components._logo')
            @if( $front_page )
                </div>
            @else
                </a>
            @endif

            @if( count($menu) )
                <!-- Menu -->
                <nav class="menu">
                    <ul>
                        @foreach( $menu as $item )
                            @if( $item->slug )
                                @if( isset($item->children) && count($item->children) > 0 )
                                    <li class="sub-menu">
                                        {!! App\Models\Menu::itemMenu( $item->name, $item->slug ) !!}
                                        <ul>
                                            @foreach( $item->children as $child )
                                                <li>{!! App\Models\Menu::itemMenu( $child->name, $child->slug ) !!}</li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li>{!! App\Models\Menu::itemMenu( $item->name, $item->slug ) !!}</li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                    <button class="menu-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </nav>
            @endif

            <div class="clear-fix"></div>
            
            @if( count($languages) > 1 )
                <!-- Languages -->
                <ul class="languages main">
                    @foreach( $languages as $language )
                        @if( app()->getLocale() != $language->code )
                            <li><a href="{{ LaravelLocalization::getLocalizedURL($language->code) }}" title="{{ $language->name }}">{{ $language->alias }}</a></li>
                        @else
                            <li><span title="{{ $language->name }}">{{ $language->alias }}</span></li>
                        @endif
                    @endforeach
                </ul>
            @endif

        </div>
        <div class="f-right">
            <!-- Search -->
            {!! Form::open(['url' => LaravelLocalization::getLocalizedURL(app()->getLocale(), 'search'), 'method' => 'GET', 'class' => 'search main']) !!}
                <input type="text" name="s" placeholder="{{ trans('design.input_search') }}" @if( $search_string = Request::get('s') )value="{{ $search_string }}"@endif required />
                <button type="submit"></button>
            {!! Form::close() !!}
            <!-- Phone box -->
            <div class="phone-box main">
                @if( $settings->phone )
                    <a href="tel:{{ str_replace(' ', '', $settings->phone) }}" class="phone">{{ $settings->phone }}</a>
                @endif
                <a href="#callback" class="callback">{{ trans('design.callback') }}</a>
            </div>
        </div>
    </div>
    <div class="hc-middle">
        <div class="hcm-left">
            
            @if( $settings->{'h_slogan_' . app()->getLocale()} || $settings->{'h_text_' . app()->getLocale()} )
                <!-- Consultation box -->
                @if( $settings->{'h_slogan_' . app()->getLocale()} )
                    <div class="title">{!! $settings->{'h_slogan_' . app()->getLocale()} !!}</div>
                @endif
                @if( $settings->{'h_text_' . app()->getLocale()} )
                    <div class="description">
                        {!! $settings->{'h_text_' . app()->getLocale()} !!}
                    </div>
                @endif
            @endif

            <a href="#callback" class="btn-consultation">{{ trans('design.btn_consultation') }}</a>
        </div>
        <div class="hcm-right">
            <!-- Contacts box -->
            <div class="contacts-box">
                
                @if( $settings->{'address_1_' . app()->getLocale()} || $settings->{'address_2_' . app()->getLocale()} )
                    <ul class="address">
                        <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_1_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_1_' . app()->getLocale()} !!}</a></li>
                        <li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_2_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_2_' . app()->getLocale()} !!}</a></li>
                    </ul>
                @endif
               
                @if( $settings->viber || $settings->telegram || $settings->facebook || $settings->instagram )
                    <ul class="social">
                        @if( $settings->viber )
                            <li class="viber">
                                <a href="viber://chat?number={{ str_replace(' ', '', $settings->viber) }}" target="_blank" title="Viber"></a>
                            </li>
                        @endif
                        @if( $settings->telegram )
                            <li class="telegram">
                                <a href="tg://resolve?domain={!! $settings->telegram !!}" title="Telegram"></a>
                            </li>
                        @endif
                        @if( $settings->instagram )
                            <li class="instagram">
                                <a href="{!! $settings->instagram !!}" target="_blank" title="Instagram"></a>
                            </li>
                        @endif
                        @if( $settings->facebook )
                            <li class="facebook">
                                <a href="{!! $settings->facebook !!}" target="_blank" title="Facebook"></a>
                            </li>
                        @endif
                    </ul>
                @endif

            </div>
            
            @if( count($main_slider) )
                <!-- Slider -->
                <div class="slider">
                    @foreach( $main_slider as $k_slide => $v_slide )
                        <div class="item {{ (empty($k_slide) ? 'current' : 'before') }}">
                            <img src="{{ asset('uploads/main_slider/' . $v_slide->image) }}" alt="" />
                        </div>
                    @endforeach
                </div>
            @endif

        </div>
    </div>
    <div class="hc-bottom">
        <!-- More information -->
        <a href="#section-what" class="btn-more-info"><span>{{ trans('design.more_information') }}</span></a>
    </div>
</div>