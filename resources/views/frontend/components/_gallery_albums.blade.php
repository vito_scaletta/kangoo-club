@foreach( $albums as $album )
    <li>
        <a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $type . '-gallery/' . $album->slug) }}" @if( $album->image )style="background-image: url({{ asset('uploads/' . $type . '-gallery/albums/' . $album->image) }})"@endif>
            <span>{{ $album->name }}</span>
        </a>
    </li>
@endforeach