@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
		<div class="row">
			@if( $settings->phone )
				<div class="col-lg-6 col-xl-6">
					<div class="page-contacts-box phones">
						<div class="cb-icon"></div>
						<div class="cb-container">
							<a href="tel:{{ str_replace(' ', '', $settings->phone) }}" class="cb-phone">{{ $settings->phone }}</a>
							<a href="#callback" class="cb-callback">{{ trans('design.callback_title') }}</a>
						</div>
					</div>
				</div>
			@endif
			@if( $settings->{'address_1_' . app()->getLocale()} || $settings->{'address_2_' . app()->getLocale()} )
				<div class="col-lg-6 col-xl-6">
					<div class="page-contacts-box address">
						<div class="cb-icon"></div>
						<div class="cb-container">
							<ul class="cb-address">
								<li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_1_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_1_' . app()->getLocale()} !!}</a></li>
								<li><a href="{!! App\Models\Setting::googleMapUrl($settings->{'address_2_' . app()->getLocale()}) !!}" target="_blank">{!! $settings->{'address_2_' . app()->getLocale()} !!}</a></li>
							</ul>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>

	<!-- Section callback -->
	@include('frontend.components._section_callback', ['class' => 'inverse'])

	@if( $page->body )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection