@extends('frontend.layout')

@section('main')
	
	<div class="stock">
		<div class="container">
	    	<div class="row">
				<div class="col-12">
					
					<!-- Breadcrumbs -->
					@include('frontend.components._breadcrumbs')
				
					<h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
				    
				    @if( count($posts) )
					    <ul class="s-items">
					    	@foreach( $posts as $p_key => $p_value )
					    		@php
					    			if( ($p_key + 1) % 2 != 0 )
									{
										$class_li = 'si-left-direction';
										$class_tb = 'col-lg-8 offset-lg-4 col-xl-9 offset-xl-3';
									}
									else
									{
										$class_li = 'si-right-direction';
										$class_tb = 'col-lg-8 col-xl-9';
									}
					    		@endphp
						    	<li class="{{ $class_li }}">
						    		@if( $p_value->image )
						    			<img src="{{ asset('uploads/stock/' . $p_value->image) }}" alt="" />
						    		@endif
						    		<div class="si-content">
							    		<div class="row">
							    			<div class="{{ $class_tb }}">
									    		<div class="si-title">{{ $p_value->name }} @if( $p_value->discount )<span class="si-percent">{{ $p_value->discount }}%</span>@endif <span class="si-date">{{ App\Models\Stock::getDate($p_value->date) }}</span></div>
									    		@if( $p_value->annotation )
										    		<div class="si-annotation">
														{!! $p_value->annotation !!}
													</div>
												@endif
									    		<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'stock/' . $p_value->slug) }}" class="si-more">{{ trans('design.btn_more') }}</a>
									    	</div>
							    		</div>
							    	</div>
						    	</li>
						    @endforeach
					    </ul>
					    {!! $posts->render() !!}
					@endif

					@if( $page->body )
						<div class="user-content with-bottom">
							{!! $page->body !!}
						</div>
					@endif
					
				</div>
			</div>
		</div>
	</div>

@endsection