@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
				
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
		@if( count($posts) )
			<div class="posts">
				@foreach( $posts as $k_post => $v_post )
					@if( (($k_post + 1) % 4) == 1)
						<div class="row">
					@endif
					<div class="col-md-6 col-lg-3 col-xl-3">
						<div class="post">
							<div class="p-preview" @if( $v_post->image )style="background-image: url({{ asset('uploads/' . $posts_type . '/' . $v_post->image) }})"@endif>
								@if( isset($v_post->date) )
									<span class="p-date">{{ App\Models\News::getDate($v_post->date) }}</span>
								@endif
							</div>
							<div class="p-title">{{ $v_post->name }}</div>
							@if( $v_post->annotation )
								<div class="p-annotation">
									{{ str_limit($v_post->annotation, 135) }}
								</div>
							@endif
							<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), $posts_type . '/' . $v_post->slug) }}" class="p-more">{{ trans('design.btn_read_more') }}</a>
						</div>
					</div>
					@if( (($k_post + 1) % 4) == 0 || ($k_post + 1) == $posts->count() )
						</div>
					@endif
				@endforeach
				<div class="row">
					<div class="col-12">
						{!! $posts->render() !!}
					</div>
				</div>
			</div>
		@endif
	</div>

	@if( $page->body )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection