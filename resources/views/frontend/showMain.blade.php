@extends('frontend.layout')

@section('main')

    @if( $settings->{'s_what_title_' . app()->getLocale()} || $settings->{'s_what_text_' . app()->getLocale()} )
	    <!-- Section "What is kangoo jumps" -->
	    <div class="section-what align-items-center" id="section-what">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-xl-6">
						<div class="title">{!! $settings->{'s_what_title_' . app()->getLocale()} !!}</div>
						<div class="description">
							{!! $settings->{'s_what_text_' . app()->getLocale()} !!}
						</div>
						<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'about') }}" class="more">{{ trans('design.more') }}</a>
					</div>
					<div class="col-lg-6 col-xl-6">
						<img src="{{ asset('frontend/img/section_what_img.png') }}" alt="" />
					</div>
				</div>
			</div>
	    </div>
	@endif

    @if( count($services) )
	    <!-- Section services -->
	    <div class="section-services">
	    	<div class="section-title">{{ trans('design.services') }}</div>
	    	<div class="services-main">
	    		@foreach( $services as $k_service => $v_service )
		    		@php
		    			if( $v_service->background )
		    				$background = asset('uploads/services/background/' . $v_service->background);
		    			else
		    				$background = '';
		    			$slug = LaravelLocalization::getLocalizedURL(app()->getLocale(), 'services/' . $v_service->slug);
		    		@endphp
		    		<a href="{{ $slug }}" class="service-{{ (empty($k_service) ? 'left' : 'right') }}" @if( $background )style="background-image: url({{ $background }})"@endif>
		    			@if( $v_service->image_name )
		    				<img src="{{ asset('uploads/services/image_name/' . $v_service->image_name) }}" alt="{{{ $v_service->name }}}" />
		    			@endif
		    		</a>
	    		@endforeach
	    	</div>
	    	<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'services') }}" class="more">{{ trans('design.btn_services_all') }}</a>
	    </div>
    @endif

	@php
		$areas = [
					'first'	 => 350, 
					'second' => 130, 
					'third'  => 130, 
					'fourth' => 150, 
					'fifth'	 => 0, 
					'sixth'	 => 205,
					'seventh'=> 235, 
					'eighth' => 0
				 ];
	@endphp
    <!-- Section interactive shoe -->
    <div class="section-shoe">
    	<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title">{{ trans('design.section_kj_title') }}</div>
					<div class="description">{{ trans('design.section_kj_description') }}</div>
					<div class="shoe">
						@foreach( $areas as $k_area => $v_area )
							@if( $settings->{'s_area_' . $k_area . '_title_' . app()->getLocale()} && $settings->{'s_area_' . $k_area . '_text_' . app()->getLocale()} )
								<span class="area {{ $k_area }}"></span>
							@endif
						@endforeach
						@foreach( $areas as $k_area => $v_area )
							@if( $settings->{'s_area_' . $k_area . '_title_' . app()->getLocale()} && $settings->{'s_area_' . $k_area . '_text_' . app()->getLocale()} )
								<div class="{{ $k_area }}-box">
									<div class="box-title">{{ $settings->{'s_area_' . $k_area . '_title_' . app()->getLocale()} }}</div>
									<div class="box-description">
										{!! $settings->{'s_area_' . $k_area . '_text_' . app()->getLocale()} !!}
									</div>
									@if( !in_array($k_area, ['fifth', 'eighth']) )
										<canvas id="{{ $k_area }}-box-line" width="{{ $v_area }}" height="2"></canvas>
									@endif
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
    </div>

	@if( $settings->s_main_video )
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">{{ trans('design.section_video_title') }}</div>
				</div>
			</div>
		</div>
		<div class="section-video no-active">
			<div class="sv-play">
				<div class="svp-btn"></div>
				<div class="svp-text">{{ trans('design.section_video_btn_text') }}</div>
			</div>
			<iframe src="https://www.youtube.com/embed/{!! App\Models\VideoGalleryItem::videoCode( $settings->s_main_video ) !!}?rel=0&showinfo=0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	@endif

    @if( count($products) )
	    <!-- Section products -->
	    <div class="section-products">
	    	<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section-title">{{ trans('design.shoe') }} Kangoo Jump</div>
					</div>
				</div>
			</div>
			<div class="products-main">
				@foreach( $products as $p_key => $p_value )
					<div class="pm-item">
						@include('frontend.components._product_item')
					</div>
				@endforeach
			</div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="pm-controls">
							@if( count($products) > 3 )
								<div class="slide-prev"></div>
								<div class="slide-next"></div>
							@endif
							<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'products') }}" class="btn-products-all">{{ trans('design.btn_products_all') }}</a>
						</div>
					</div>
				</div>
			</div>
	    </div>
    @endif

    <!-- Section callback -->
    @include('frontend.components._section_callback')
    
	@if( count($photo_gallery) )
		<!-- Section photo gallery -->
	    @foreach( $photo_gallery as $pg )
			@if( count($pg->images) )
			    <div class="section-photo-gallery">
			    	<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="section-title">{{ trans('design.section_gallery_title') }}</div>
							</div>
						</div>
					</div>
					<div class="carousel-container">
						<div id="carousel">
							@foreach( $pg->images as $item )
								<img src="{{ asset('uploads/photo-gallery/images/' . $item->image) }}" alt="" />
							@endforeach
						</div>
						<div class="slide-prev"></div>
						<div class="slide-next"></div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'photo-gallery') }}" class="btn-all-photos">{{ trans('design.btn_gallery_all') }}</a>
							</div>
						</div>
					</div>
			    </div>
	    	@endif
		@endforeach
    @endif

    @if( $page->body )
	    <div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection