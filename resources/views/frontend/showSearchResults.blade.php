@extends('frontend.layout')

@section('main')
	
	<div class="search">
		<div class="container">
	    	<div class="row">
				<div class="col-12">
					
					<!-- Breadcrumbs -->
					@include('frontend.components._breadcrumbs')

				   	@if( $search_string = Request::get('s') )
				   		<div class="s-title">{{ trans('design.search_title') }} <span>"{{ $search_string }}"</span></div>
					@endif
				</div>
			</div>
		</div>
		@if( count($search_results) )
			<ul class="s-results">
				@foreach( $search_results as $sr_key => $sr_value )
					<li>
						<div class="container">
			    			<div class="row">
								<div class="col-12">
									<div class="sr-title">
										<span>{{ ($sr_key + 1) }}.</span> <a href="{{ $sr_value->slug }}">{{ $sr_value->name }}</a>
									</div>
									<div class="sr-text">
										{{ $sr_value->body }}
									</div>
									<a href="{{ $sr_value->slug }}" class="sr-more">{{ trans('design.btn_to_record') }}</a>
								</div>
							</div>
						</div>
					</li>	 
				@endforeach 		
			</ul>
			<div class="container">
		    	<div class="row">
					<div class="col-12">
						{!! $search_results->appends(['s' => Request::get('s')])->render() !!}
					</div>
				</div>
			</div>
		@else
			<div class="container">
		    	<div class="row">
					<div class="col-12">
						<div class="user-content with-bottom">
							{!! $page->body !!}
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>

@endsection