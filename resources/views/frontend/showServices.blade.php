@extends('frontend.layout')

@section('main')
	
	<div class="container">
    	<div class="row">
			<div class="col-12">
				
				<!-- Breadcrumbs -->
				@include('frontend.components._breadcrumbs')
			    
			    <h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
			</div>
		</div>
	</div>

	@if( $services_count = count($services) )
		<ul class="services">
			@foreach( $services as $service )
				<li id="{{ $service->slug }}">
					<div class="container">
		    			<div class="row">
							<div class="col-12">
								@php
					    			if( $service->background )
					    				$background = asset('uploads/services/background/' . $service->background);
					    			else
					    				$background = '';
					    		@endphp
								<div class="s-title" @if( $background )style="background-image: url({{ $background }})"@endif>
									@if( $service->image_name )
										<img src="{{ asset('uploads/services/image_name/' . $service->image_name) }}" alt="{{{ $service->name }}}" />
									@endif
								</div>
								@if( $service->annotation )
									<div class="s-description">
										{!! $service->annotation !!}
									</div>
								@endif
							</div>
						</div>
					</div>
					<div class="s-more">
						<a href="{{ LaravelLocalization::getLocalizedURL(app()->getLocale(), 'services/' . $service->slug) }}">{{ trans('design.more_information') }}</a>
					</div>
				</li>
			@endforeach
		</ul>
		@if( $services_count > 5 )
			<div class="container">
		    	<div class="row">
					<div class="col-12">
						{!! $services->render() !!}
					</div>
				</div>
			</div>
		@endif
	@endif

	@if( $page->body )
	    <div class="container">
			<div class="row">
				<div class="col-12">
					<div class="user-content with-bottom">
						{!! $page->body !!}
					</div>
				</div>
			</div>
		</div>
	@endif

@endsection