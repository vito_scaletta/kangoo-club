@extends('frontend.layout')

@section('main')
	
	<div class="price">
		<div class="container">
	    	<div class="row">
				<div class="col-12">
					
					<!-- Breadcrumbs -->
					@include('frontend.components._breadcrumbs')
				
			    	<h1 class="page-title">{{ (!empty($page->h1) ? $page->h1 : $page->name) }}</h1>
				    <div class="page-description">{{ trans('design.price_before') }}</div>
					@if( $page->body )
						<div class="table-responsive">
							{!! $page->body !!}
						</div>
						<div class="p-notice">{{ trans('design.price_after') }}</div>
					@endif
				</div>
			</div>
		</div>
		@if( $page->body )
			{!! Form::open(['url' => asset('feedback')]) !!}
				<input type="hidden" name="locale" value="{{ app()->getLocale() }}" />
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="p-form-text">
								{!! trans('design.price_cb_text') !!}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-xl-4">
			                <input type="text" name="name" class="input" placeholder="{{ trans('design.input_name') }}" required />
			            </div>
			            <div class="col-lg-4 col-xl-4">
			                <input type="tel" name="phone" class="input" placeholder="{{ trans('design.input_phone') }}" required />
			            </div>
			            <div class="col-lg-4 col-xl-4">
			                <button type="submit">{{ trans('design.btn_order_consultation') }}</button>
			            </div>
			        </div>
			    </div>
			{!! Form::close() !!}
		@endif
	</div>

@endsection