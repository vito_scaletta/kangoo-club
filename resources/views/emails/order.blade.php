<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	<p>Заказ #<strong>{{ $order_id }}</strong></p>
	<p>Имя: <strong>{{ $user_name }}</strong></p>
	<p>Номер телефона: <strong><a href="tel:{{ str_replace(['(', ')', ' ', '-'], '', $user_phone) }}">{{ $user_phone }}</a></strong></p>
	<p>Название товара: <strong>{{ $product_name }}</strong></p>
	<p>Цена: <strong style="color: #d9534f;">{{ $product_price }} €</strong></p>
</body>
</html>