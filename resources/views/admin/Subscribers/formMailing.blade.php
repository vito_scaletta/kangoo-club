@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/subscribers') }}">Подписчики</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( Session::has('success') )
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'url' => asset('master/subscribers')]) !!}
				<div class="form-group">
					<div class="col-sm-12">
						{!! Form::text('subject', old('subject'), ['class' => 'form-control', 'placeholder' => 'Тема письма']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						{!! Form::textarea('body', old('body'), ['class' => 'form-control editor']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-success">
							<i class="fa fa-paper-plane"></i>&ensp;Выполнить рассылку
						</button>
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>
	
@stop

@section('scripts')
    @parent
	@include('admin.tinymceInit')
@endsection