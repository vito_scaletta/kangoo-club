@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ asset('master/subscribers/create') }}" class="thumbnail text-center">
				<i class="fa fa-paper-plane fa-5x"></i>
				<div class="title">Рассылка писем</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($posts) )
				{!! Form::open(['method' => 'DELETE', 'url' => asset('master/subscribers/destroy')]) !!}
					<table class="table">
						<thead>
							<tr>
								<th width="70%">Подписчики</th>
								<th class="text-right" width="30%">Управление</th>
							</tr>
						</thead>
						<tbody>
							
							@foreach( $posts as $post )
								<tr>
									<td>
										<input type="checkbox" name="check[]" value="{{ $post->id }}" />
										&ensp;{{ $post->email }}
									</td>
									<td class="text-right">
										<div class="btn-group" role="group">
											<button type="button" class="btn {{ !empty($post->visible) ? 'btn-success' : 'btn-default' }} visible" title="{{ !empty($post->visible) ? 'Исключить подписчика из рассылки' : 'Добавить подписчика в рассылку' }}" data-id="{{ $post->id }}">
												<i class="fa {{ !empty($post->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
											</button>
											<button type="button" class="btn btn-danger delete" title="Удалить подписчика" data-id="{{ $post->id }}">
												<i class="fa fa-times"></i>
											</button>
										</div>
									</td>
								</tr>
							@endforeach

						</tbody>
					</table>
					
					{!! $posts->render() !!}
					
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>

				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Подписчиков нет :(</div>
			@endif
		</div>
	</div>
	
@stop

@section('scripts')
    @parent
	<script>
		$(function(){
			
			// Удаление сообщения
			$('.delete').click(function(){
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('tr').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').prop('selected', true);
				$(this).closest('form').submit();
			})

			// Удаление сообщений
			$('form').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});

			// Исключить/добавить подписчика из/в рассылки(у)
			$('.visible').on('click', function(event){
				
				event.preventDefault();
				
				if( $(this).hasClass('btn-success') )
				{
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
					$(this).attr('title', 'Добавить подписчика в рассылку');
				}
				else
				{
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
					$(this).attr('title', 'Исключить подписчика из рассылки');
				}
				
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id') };
				$.post('{{ asset("master/subscribers/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});
		});
	</script>
@endsection