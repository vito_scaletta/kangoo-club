@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/team') }}">Наша команда</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="alert alert-warning">
				Максимальный размер загружаемого файла: <strong>{{ ini_get('upload_max_filesize') }}</strong>
			</div>
		</div>
	</div>

	@if( $errors->any() )
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif

	@if(Session::has('success'))
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-md-12">

			{!! Form::open(['url' => $rest_api['url'], 'method' => $rest_api['method'], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true]) !!}
				<div class="form-group">
					{!! Form::label('visible', 'Показывать сотрудника', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::select('visible', [0 => 'Нет', 1 => 'Да'], $post->visible, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2 control-label">
						<strong>Фото</strong>
					</div>
					<div class="col-sm-8">
						<div class="input-group">
							<span class="input-group-btn">
								<span class="btn btn-primary btn-file">
									<i class="fa fa-picture-o"></i> <input type="file" name="image" />
								</span>
							</span>
							<input type="text" class="form-control" id="image" value="{{ $post->image }}" readonly>
						</div>
					</div>
				</div>
				
				@if( $post->image )
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-3">
							<div class="panel panel-default image-prev">
								<div class="panel-body">
									<img src="{{ asset('uploads/team/' . $post->image) }}" alt="" />
								</div>
							</div>
						</div>
					</div>
				@endif

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
										<a href="#{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
									</li>
								@endif
							@endforeach
						</ul>
					</div>
				</div>
				
				<!-- Tab panes -->
				<div class="tab-content">
					@foreach( $languages as $k_lang => $v_lang )
						@if( $v_lang->visible )
							<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="{{ $v_lang->code }}">
								<div class="form-group">
									{!! Form::label($v_lang->code . '[first_name]', 'Имя', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-8">
										{!! Form::text($v_lang->code . '[first_name]', $data[$v_lang->code]->first_name, ['class' => 'form-control']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label($v_lang->code . '[last_name]', 'Фамилия', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-8">
										{!! Form::text($v_lang->code . '[last_name]', $data[$v_lang->code]->last_name, ['class' => 'form-control']) !!}
									</div>
								</div>
								<div class="form-group">
									{!! Form::label($v_lang->code . '[role]', 'Должность', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-8">
										{!! Form::text($v_lang->code . '[role]', $data[$v_lang->code]->role, ['class' => 'form-control']) !!}
									</div>
								</div>
							</div>
						@endif
					@endforeach
				</div>
				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-2">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>

@stop

@section('scripts')
    @parent
	<script>
		// File input
		$(document).on('change', '.btn-file :file', function(){
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});
		$(document).ready(function(){
			$('.btn-file :file').on('fileselect', function(event, numFiles, label){
				var input = $(this).parents('.input-group').find(':text'),
					log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length )
					input.val(log);
				else
					if( log ) 
						alert(log);
			});
		});
	</script>
@endsection