@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/menu') }}">Меню</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			
			{!! Form::open(['url' => $rest_api['url'], 'method' => $rest_api['method'], 'class' => 'form-horizontal', 'role' => 'form']) !!}
				
				<div class="form-group">
					{!! Form::label('slug', 'URL aдрес', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						<div class="input-group">
							<span class="input-group-addon">&nbsp;/&nbsp;</span>
							{!! Form::text('slug',  $post->slug, ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('visible', 'Показывать пункт меню', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::select('visible', [0 => 'Нет', 1 => 'Да'], $post->visible, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('parent_id', 'Родительский пункт меню', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::select('parent_id', $tree, $parent_id, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2 control-label">
						<strong>Название</strong>
					</div>
					<div class="col-sm-10">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
										<a href="#{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
									</li>
								@endif
							@endforeach
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="{{ $v_lang->code }}">
										{!! Form::text($v_lang->code . '[name]', $data[$v_lang->code]->name, ['class' => 'form-control']) !!}
									</div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>

			{!! Form::close() !!}
			
		</div>
	</div>
	
@stop