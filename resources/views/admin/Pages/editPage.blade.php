@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/pages') }}">Страницы</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
		
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			@if( $post->slug == 'about' )
				<div class="alert alert-warning" role="alert">
					Эта страница имеет дополнительные поля для редактирования, <a href="{{ asset('master/settings#about') }}"><i class="fa fa-share"></i> перейти к редактированию</a>
				</div>
			@endif

			{!! Form::open(['url' => $rest_api['url'], 'method' => $rest_api['method'], 'class' => 'form-horizontal', 'role' => 'form']) !!}

				<div class="form-group">
					<div class="col-sm-12">
						<div class="input-group">
							<span class="input-group-addon">/</span>
							{!! Form::text('slug',  $post->slug, ['class' => 'form-control', 'placeholder' => 'URL aдрес']) !!}
						</div>
					</div>
				</div>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					@foreach( $languages as $k_lang => $v_lang )
						@if( $v_lang->visible )
							<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
								<a href="#{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
							</li>
						@endif
					@endforeach
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
					@foreach( $languages as $k_lang => $v_lang )
						@if( $v_lang->visible )
							<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="{{ $v_lang->code }}">
								<ul class="nav nav-tabs" role="tablist">
									<li class="active"><a href="#record_{{ $v_lang->code }}" role="tab" data-toggle="tab">Страница</a></li>
									<li><a href="#options_{{ $v_lang->code }}" role="tab" data-toggle="tab">Параметры страницы</a></li>
								</ul>
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="record_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label($v_lang->code . '[name]', 'Название', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::text($v_lang->code . '[name]', $data[$v_lang->code]->name, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label($v_lang->code . '[h1]', 'H1 заголовок', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::text($v_lang->code . '[h1]', $data[$v_lang->code]->h1, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label($v_lang->code . '[body]', 'Полное описание', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::textarea($v_lang->code . '[body]', $data[$v_lang->code]->body, ['class' => 'form-control editor']) !!}
											</div>
										</div>
									</div>
									<div role="tabpanel" class="tab-pane" id="options_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label($v_lang->code . '[meta_title]', 'Заглавие', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::text($v_lang->code . '[meta_title]', $data[$v_lang->code]->meta_title, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label($v_lang->code . '[meta_keywords]', 'Ключевые слова', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::text($v_lang->code . '[meta_keywords]', $data[$v_lang->code]->meta_keywords, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label($v_lang->code . '[meta_description]', 'Описание', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-10">
												{!! Form::textarea($v_lang->code . '[meta_description]', $data[$v_lang->code]->meta_description, ['class'=>'form-control', 'rows' => 4]) !!}
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif
					@endforeach
				</div>

				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>

@stop

@section('scripts')
    @parent
	<script>

		@if( Request::segment(3) == 'create' )
			// Генерация данных
			meta_title_touched = false;
			url_touched = false;

			$('input[name="slug"]').change(function(){ url_touched = true; });

			// RU
			$('input[name="ru[name]"]').keyup(function(){
				if( !url_touched )
					$('input[name="slug"]').val(generate_url()); 
				if( !meta_title_touched )
					$('input[name="ru[meta_title]"]').val( $('input[name="ru[name]"]').val() );
			});
			$('input[name="ru[meta_title]"]').change(function(){ meta_title_touched = true; });

			function generate_url(){
				url = $('input[name="ru[name]"]').val();
				url = url.replace(/[\s]+/gi, '-');
				url = translit(url);
				url = url.replace(/[^0-9a-z_\-]+/gi, '').toLowerCase();	
				return url;
			}

			function translit(str){
				var ru=("А-а-Б-б-В-в-Ґ-ґ-Г-г-Д-д-Е-е-Ё-ё-Є-є-Ж-ж-З-з-И-и-І-і-Ї-ї-Й-й-К-к-Л-л-М-м-Н-н-О-о-П-п-Р-р-С-с-Т-т-У-у-Ф-ф-Х-х-Ц-ц-Ч-ч-Ш-ш-Щ-щ-Ъ-ъ-Ы-ы-Ь-ь-Э-э-Ю-ю-Я-я").split("-")   
				var en=("A-a-B-b-V-v-G-g-G-g-D-d-E-e-E-e-E-e-ZH-zh-Z-z-I-i-I-i-I-i-J-j-K-k-L-l-M-m-N-n-O-o-P-p-R-r-S-s-T-t-U-u-F-f-H-h-TS-ts-CH-ch-SH-sh-SCH-sch-'-'-Y-y-'-'-E-e-YU-yu-YA-ya").split("-")   
				var res = '';
				for(var i=0, l=str.length; i<l; i++){ 
					var s = str.charAt(i), n = ru.indexOf(s);
					if(n >= 0) { res += en[n]; } 
					else { res += s; } 
				} 
				return res;
			}
		@endif

	</script>
	
	@include('admin.tinymceInit')
@endsection