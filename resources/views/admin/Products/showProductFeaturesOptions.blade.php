@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/products') }}">Товары</a></li>
				<li><a href="{{ asset('master/products/' . (int)Request::segment(3)) }}">Товар: "{{ $product_name }}"</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="alert alert-warning" role="alert">Не более <strong>3-х</strong> характеристик на единицу товара</div>
		</div>
	</div>

	@if(Session::has('success'))
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-md-12">

			@if( count($features) > 0 )
				{!! Form::open(['method' => 'PUT', 'class' => 'form-horizontal']) !!}
					@foreach( $features as $feature )
						<div class="form-group">
							<label for="select-{{ $feature->id }}" class="col-sm-2 control-label">{{ $feature->name }}</label>
							<div class="col-sm-8">
								<select multiple="multiple" name="feature[{{ $feature->id }}][]" id="select-{{ $feature->id }}" class="form-control feature-select">
									<option value=""></option>
									@if( isset($feature->parameters) && count($feature->parameters) > 0 )
										@foreach( $feature->parameters as $parameter )
											<option value="{{ $parameter->id }}" @if( isset($options[$feature->id] ) && in_array($parameter->id, $options[$feature->id]) ) selected @endif>{{ $parameter->name }}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
					@endforeach
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-8">
							<button type="submit" class="btn btn-success">Сохранить</button>
						</div>
					</div>
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Нет параметров для выбора значений</div>
			@endif

		</div>
	</div>
@stop

@section('styles')
	@parent
	<link rel="stylesheet" href="{{ asset('admin/chosen/chosen.min.css') }}">
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('admin/chosen/chosen.jquery.min.js') }}"></script>
	<script>
		$(function(){
			// Multiselect
			$('.feature-select').chosen({
				placeholder_text_multiple: 'Выберите опцию...'
			});
		});
	</script>
@endsection