@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/products-menu') }}">Магазин</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ asset('master/products-features/create') }}" class="thumbnail text-center">
				<i class="fa fa-plus-circle fa-5x"></i>
				<div class="title">Добавить характеристику</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($features) > 0 )
			
				<div class="title-module">
					<div class="pull-left">
						<strong>Название</strong>
					</div>
					<div class="pull-right">
						<strong class="text-control">Управление</strong>
					</div>
					<div class="clear"></div>
				</div>
				
				{!! Form::open(['url' => asset('master/products-features/destroy'), 'method' => 'DELETE', 'class' => 'products-list']) !!}
					<div class="list-group">
						
						@foreach( $features as $feature )
							<div class="list-group-item" id="{{ $feature->id }}">
								<div class="pull-left">
									<span class="item-element-menu menu-sortable" title="Сортировка характеристик">
										<i class="fa fa-bars"></i>
									</span>
									<span class="item-element-menu">
										<input type="checkbox" name="check[]" value="{{ $feature->id }}" />
									</span>
									<span class="item-element-menu">
										<a href="{{ asset('master/products-options/' . $feature->id) }}" title="Опции характеристики">{{ $feature->name }}</a>
									</span>
								</div>
								<div class="pull-right">
									<div class="btn-group" role="group">
										<button type="button" class="btn {{ !empty($feature->visible) ? 'btn-success' : 'btn-default' }} visible" title="Показать/скрыть характеристику" data-id="{{ $feature->id }}">
											<i class="fa {{ !empty($feature->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
										</button>
										<a href="{{ asset('master/products-options/' . $feature->id) }}" class="btn btn-primary" title="Опции характеристики"><i class="fa fa-cog"></i></a>
										<a href="{{ asset('master/products-features/' . $feature->id) }}" class="btn btn-warning" title="Редактировать характеристику"><i class="fa fa-pencil"></i></a>
										<button type="button" class="btn btn-danger delete" title="Удалить характеристику" data-id="{{ $feature->id }}"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						@endforeach
						
					</div>
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>	
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Характеристики еще не добавлены</div>
			@endif
		</div>
	</div>
@stop

@section('scripts')
    @parent
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$(function(){
			
			// Удаление характеристики
			$('.delete').click( function() {
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('.list-group-item').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').attr('selected', true);
				$(this).closest('form').submit();
			});

			// Удаление характеристик
			$('form.products-list').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Показать/скрыть характеристику
			$('.visible').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
				else
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id') };
				$.post('{{ asset("master/products-features/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});
			
			// Позиционирование характеристик
			$('.list-group').sortable({
				handle: '.menu-sortable',
				opacity: 0.7,
				stop: function(){
					var data = { _token: '{{ csrf_token() }}', position: $(this).sortable('toArray') };
					$.post('{{ asset("master/products-features/sortable") }}', data, function(data){ console.log(data) }, 'JSON');	
				}
			});
		});
	</script>
@endsection