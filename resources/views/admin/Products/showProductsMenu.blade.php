@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ asset('master/products') }}" class="thumbnail text-center">
				<i class="fa fa-shopping-basket fa-5x"></i>
				<span class="counter">{{ $products_count }}</span>
				<div class="title">Товары</div>
			</a>
		</div>
		<div class="col-md-3">
			<a href="{{ asset('master/products-features') }}" class="thumbnail text-center">
				<i class="fa fa-cogs fa-5x"></i>
				<span class="counter">{{ $products_features_count }}</span>
				<div class="title">Характеристики товаров</div>
			</a>
		</div>
		<div class="col-md-3">
			<a href="{{ asset('master/products-orders') }}" class="thumbnail text-center">
				<i class="fa fa-paper-plane fa-5x"></i>
				<span class="counter">{{ $products_orders_count }}</span>
				<div class="title">Заказы товаров</div>
			</a>
		</div>
	</div>
@stop