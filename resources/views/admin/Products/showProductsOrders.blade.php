@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/products-menu') }}">Магазин</a></li>
				<li class="active">{{ $title }}</li>
			</ol>

			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($orders) > 0 )
				{!! Form::open() !!}
					<table class="table">
						<thead>
							<tr>
								<th width="{{ (100/7) }}%">Номер заказа</th>
								<th width="{{ (100/7) }}%">Дата</th>
								<th width="{{ (100/7) }}%">Имя</th>
								<th width="{{ (100/7) }}%">Телефон</th>
								<th width="{{ (100/7) }}%">Товар</th>
								<th width="{{ (100/7) }}%">Цена</th>
								<th class="text-right" width="{{ (100/7) }}%">Управление</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $orders as $order )
								<tr>
									<td>
										<input type="checkbox" name="check[]" value="{{ $order->id }}" />
										&ensp;Заказ #{{ $order->id }}
									</td>
									<td><strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at)->format('d.m.Y, H:i') }}</strong></td>
									<td><strong>{{ $order->user_name }}</strong></td>
									<td><strong>{{ $order->user_phone }}</strong></td>
									<td><strong>{{ $order->product_name }}</strong></td>
									<td><strong class="text-danger">{{ (int)$order->product_price }} грн.</strong></td>
									<td class="text-right">
										<div class="btn-group" role="group">
											<button type="button" class="btn {{ !empty($order->visible) ? 'btn-success' : 'btn-default' }} visible" title="Отметить заказ как {{ !empty($order->visible) ? 'не просмотренный' : 'просмотренный' }}" data-id="{{ $order->id }}">
												<i class="fa {{ !empty($order->visible) ? 'fa-star' : 'fa-star-o' }}"></i>
											</button>
											<button type="button" class="btn btn-danger delete" title="Удалить заказ" data-id="{{ $order->id }}"><i class="fa fa-times"></i></button>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					
					{!! $orders->render() !!}
					
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Заказов нет :(</div>
			@endif

		</div>
	</div>
	
@stop

@section('scripts')
    @parent
	<script>
		$(function(){
			
			// Удаление опции
			$('.delete').click(function(){
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('tr').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').prop('selected', true);
				$(this).closest('form').submit();
			})

			// Удаление опций
			$('form').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});

			// Показать/скрыть услугу
			$('.visible').on('click', function(event){
				event.preventDefault();
				if( $(this).hasClass('btn-success') )
					$(this).removeClass('btn-success').addClass('btn-default').find('i').removeClass('fa-star').addClass('fa-star-o');
				else
					$(this).removeClass('btn-default').addClass('btn-success').find('i').removeClass('fa-star-o').addClass('fa-star');
				var data = { _token: '{{ csrf_token() }}', id: $(this).data('id') };
				$.post('{{ asset("master/products-orders/visible") }}', data, function(data){ console.log(data) }, 'JSON');
			});
		});
	</script>
@endsection