@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/products-menu') }}">Магазин</a></li>
				<li><a href="{{ asset('master/products-features') }}">Характеристики</a></li>
				<li><a href="{{ asset('master/products-features/' . $feature_id) }}">Характеристика: "{{ $feature_name }}"</a></li>
				<li class="active">{{ $title }}</li>
			</ol>

			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif
			
			@if( $errors->any() )
				<ul class="alert alert-danger">
					@foreach( $errors->all() as $error )
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<a href="{{ asset('master/products-options/' . $feature_id . '/add') }}" class="thumbnail text-center">
				<i class="fa fa-plus-circle fa-5x"></i>
				<div class="title">Добавить опцию</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if( count($options) > 0 )
				{!! Form::open() !!}
					<table class="table">
						<thead>
							<tr>
								<th width="70%">Название</th>
								<th class="text-right" width="30%">Управление</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $options as $option )
								<tr>
									<td>
										<input type="checkbox" name="check[]" value="{{ $option->id }}" />
										&ensp;{{ $option->name }}
									</td>
									<td class="text-right">
										<div class="btn-group" role="group">
											<a href="{{ asset('master/products-options/' . $feature_id . '/edit/' . $option->id) }}" class="btn btn-warning" title="Редактировать опцию"><i class="fa fa-pencil"></i></a>
											<button type="button" class="btn btn-danger delete" title="Удалить опцию" data-id="{{ $option->id }}"><i class="fa fa-times"></i></button>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					
					{!! $options->render() !!}
					
					<div class="select_form">
						<label id="check_all" class="link">Выбрать все</label>
						<select name="action" class="form-control">
							<option value="delete">Удалить</option>
						</select>
						<button type="submit" class="btn btn-success delete-all" disabled>Применить</button>
					</div>
				{!! Form::close() !!}
			@else
				<div class="alert alert-warning">Опции еще не добавлены</div>
			@endif

		</div>
	</div>
	
@stop

@section('scripts')
    @parent
	<script>
		$(function(){
			
			// Удаление опции
			$('.delete').click(function(){
				$('input[type="checkbox"][name*="check"]').prop('checked', false);
				$(this).closest('tr').find('input[type="checkbox"][name*="check"]').prop('checked', true);
				$(this).closest('form').find('select[name="action"] option[value=delete]').prop('selected', true);
				$(this).closest('form').submit();
			})

			// Удаление опций
			$('form').submit(function(){
				if( $('select[name="action"]').val() == 'delete' && !confirm('Подтвердите удаление') )
					return false;
			});

			// Выделить все
			$('#check_all').on('click', function(){
				$('input[type="checkbox"][name*="check"]:enabled').prop('checked', $('input[type="checkbox"][name*="check"]:enabled:not(:checked)').length > 0 );
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
			
			// Активность кнопки "Удалить выбранные" 
			$('input[type="checkbox"][name*="check"]').change(function(){
				if( $('input[type="checkbox"][name*="check"]:checked').length )
					$('.delete-all').removeAttr('disabled');
				else
					$('.delete-all').attr('disabled', 'disabled');
			});
		});
	</script>
@endsection