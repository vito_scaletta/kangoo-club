@extends('admin.layout')

@section('main')

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li><a href="{{ asset('master/photo-gallery-albums') }}">Альбомы</a></li>
				<li><a href="{{ asset('master/photo-gallery-albums/' . $album->id) }}">Альбом: "{{ $album->name }}"</a></li>
				<li><a href="{{ asset('master/photo-gallery-images/' . $album->id) }}">Изображения альбома: "{{ $album->name }}"</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>

	@if(Session::has('success'))
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			</div>
		</div>
	@endif
	
	<div class="row">
		<div class="col-md-12">
			
			{!! Form::open(['url' => asset('master/photo-gallery-images'), 'class' => 'form-horizontal', 'role' => 'form']) !!}
				<input type="hidden" name="image_id" value="{{ $image_id }}" />
				<div class="form-group">
					{!! Form::label('visible', 'Показывать изображение', ['class' => 'col-sm-2 control-label']) !!}
					<div class="col-sm-8">
						{!! Form::select('visible', [0 => 'Нет', 1 => 'Да'], $post->visible, ['class' => 'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
										<a href="#{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
									</li>
								@endif
							@endforeach
						</ul>
					</div>
				</div>
				<!-- Tab panes -->
				<div class="tab-content">
					@foreach( $languages as $k_lang => $v_lang )
						@if( $v_lang->visible )
							<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="{{ $v_lang->code }}">
								<div class="form-group">
									{!! Form::label($v_lang->code . '[name]', 'Заглавие', ['class' => 'col-sm-2 control-label']) !!}
									<div class="col-sm-8">
										{!! Form::text($v_lang->code . '[name]', $data[$v_lang->code]->name, ['class' => 'form-control']) !!}
									</div>
								</div>
							</div>
						@endif
					@endforeach
				</div>
				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-2">
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

@stop