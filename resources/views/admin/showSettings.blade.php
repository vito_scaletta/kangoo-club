@extends('admin.layout')

@section('main')
	
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				<li><a href="{{ asset('master') }}">Главная</a></li>
				<li class="active">{{ $title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			@if(Session::has('success'))
				<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
			@endif

			@if(Session::has('error'))
				<div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
			@endif

		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			
			<!-- Nav tabs -->
			<ul class="nav nav-tabs settings" role="tablist">
				<li class="active"><a href="#general" role="tab" data-toggle="tab"><i class="fa fa-cog" aria-hidden="true"></i> Основные</a></li>
				<li><a href="#languages" role="tab" data-toggle="tab"><i class="fa fa-flag" aria-hidden="true"></i> Языки</a></li>
				<li><a href="#main-page" role="tab" data-toggle="tab"><i class="fa fa-home" aria-hidden="true"></i> Главная страница</a></li>
				<li><a href="#about" role="tab" data-toggle="tab"><i class="fa fa-users" aria-hidden="true"></i> О нас</a></li>
			</ul>

		</div>
	</div>

	{!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}

		<input type="hidden" name="hash" value="" />

		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="general">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Контакты</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('phone', 'Телефон', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::text('phone', $settings->phone, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('email', 'E-mail', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::email('email', $settings->email, ['class' => 'form-control', 'required' => 'required']) !!}
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Адрес</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#address_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! empty($k_lang) ? 'active' : '' !!}" id="address_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label('address_1_' . $v_lang->code, 'Адрес #1', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::text('address_1_' . $v_lang->code, $settings->{'address_1_' . $v_lang->code}, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('address_2_' . $v_lang->code, 'Адрес #2', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::text('address_2_' . $v_lang->code, $settings->{'address_2_' . $v_lang->code}, ['class' => 'form-control']) !!}
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Социальные сети</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('viber', 'Viber', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::text('viber', $settings->viber, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('telegram', 'Telegram', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::text('telegram', $settings->telegram, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('facebook', 'Facebook', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::text('facebook', $settings->facebook, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('instagram', 'Instagram', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::text('instagram', $settings->instagram, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="languages">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<table class="table table-bordered">
									@foreach( $languages as $lang )						
										<tr>
											<td width="50%"><strong>{{ $lang->name }}</strong></td>
											<td width="50%">
												<input type="hidden" name="{{ $lang->code }}" value="0" />
												{!! Form::checkbox($lang->code, 1, $lang->visible, ['id' => 'lang_' . $lang->code]) !!}
											</td>
										</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="main-page">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Текст в шапке</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#h_content_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="h_content_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label('h_slogan_' . $v_lang->code, 'Слоган', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::text('h_slogan_' . $v_lang->code, $settings->{'h_slogan_' . $v_lang->code}, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('h_text_' . $v_lang->code, 'Текст', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::textarea('h_text_' . $v_lang->code, $settings->{'h_text_' . $v_lang->code}, ['class' => 'form-control', 'rows' => 5]) !!}
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Слайдер в шапке</h3>
								<hr/>
								<a href="{{ asset('master/slider') }}" class="btn btn-primary">
									Перейти к изображениям слайдера <span class="glyphicon glyphicon-share-alt"></span>
								</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Что такое Kangoo jumps?</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#s_what_content_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="s_what_content_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label('s_what_title_' . $v_lang->code, 'Заглавие', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::text('s_what_title_' . $v_lang->code, $settings->{'s_what_title_' . $v_lang->code}, ['class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('s_what_text_' . $v_lang->code, 'Текст', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::textarea('s_what_text_' . $v_lang->code, $settings->{'s_what_text_' . $v_lang->code}, ['class' => 'form-control', 'rows' => 5]) !!}
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Что представляют собой ботинки KJ?</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#s_area_content_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="s_area_content_{{ $v_lang->code }}">
										@php
											$areas = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth'];
										@endphp
										@foreach( $areas as $k_area => $v_area )
											<div class="form-group">
												{!! Form::label('s_area_' . $v_area . '_title_' . $v_lang->code, 'Заглавие области #' . ($k_area + 1), ['class' => 'col-sm-2 control-label']) !!}
												<div class="col-sm-8">
													{!! Form::text('s_area_' . $v_area . '_title_' . $v_lang->code, $settings->{'s_area_' . $v_area . '_title_' . $v_lang->code}, ['class' => 'form-control']) !!}
												</div>
											</div>
											<div class="form-group">
												{!! Form::label('s_area_' . $v_area . '_text_' . $v_lang->code, 'Текст области #' . ($k_area + 1), ['class' => 'col-sm-2 control-label']) !!}
												<div class="col-sm-8">
													{!! Form::textarea('s_area_' . $v_area . '_text_' . $v_lang->code, $settings->{'s_area_' . $v_area . '_text_' . $v_lang->code}, ['class' => 'form-control', 'rows' => 3]) !!}
												</div>
											</div>
										@endforeach
									</div>
								@endif
							@endforeach
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Видео презентация Kangoo Jump</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('s_main_video', 'Ссылка на YouTube', ['class' => 'col-sm-2 control-label']) !!}
							<div class="col-sm-8">
								{!! Form::url('s_main_video', $settings->s_main_video, ['class' => 'form-control', 'pattern' => 'https://www\.youtube\.com\/(.+)']) !!}
							</div>
						</div>
						@if( $settings->s_main_video )
							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-2">
									<div class="panel panel-default iframe-prev">
										<div class="panel-body">
											<iframe src="https://www.youtube.com/embed/{!! App\Models\VideoGalleryItem::videoCode( $settings->s_main_video ) !!}" allowfullscreen></iframe>
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="about">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Наши достижения</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#achievements_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="achievements_{{ $v_lang->code }}">
										@for( $i = 1; $i <= 5; $i++ )
											<div class="form-group">
												{!! Form::label('achievements_' . $i . '_' . $v_lang->code, 'Достижение #' . $i, ['class' => 'col-sm-2 control-label']) !!}
												<div class="col-sm-8">
													{!! Form::text('achievements_' . $i . '_' . $v_lang->code, $settings->{'achievements_' . $i . '_' . $v_lang->code}, ['class' => 'form-control']) !!}
												</div>
											</div>
										@endfor
									</div>
								@endif
							@endforeach
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Ещё немного о нас</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#about_text_1_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="about_text_1_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label('about_text_1_' . $v_lang->code, 'Текст', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::textarea('about_text_1_' . $v_lang->code, $settings->{'about_text_1_' . $v_lang->code}, ['class' => 'form-control editor']) !!}
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<h3>Информация</h3>
								<hr/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									@foreach( $languages as $k_lang => $v_lang )
										@if( $v_lang->visible )
											<li {!! empty($k_lang) ? 'class="active"' : '' !!}>
												<a href="#about_text_2_{{ $v_lang->code }}" role="tab" data-toggle="tab">{{ $v_lang->name }} ({{ $v_lang->code }})</a>
											</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach( $languages as $k_lang => $v_lang )
								@if( $v_lang->visible )
									<div role="tabpanel" class="tab-pane {!! (empty($k_lang) ? 'active' : '') !!}" id="about_text_2_{{ $v_lang->code }}">
										<div class="form-group">
											{!! Form::label('about_text_2_' . $v_lang->code, 'Текст', ['class' => 'col-sm-2 control-label']) !!}
											<div class="col-sm-8">
												{!! Form::textarea('about_text_2_' . $v_lang->code, $settings->{'about_text_2_' . $v_lang->code}, ['class' => 'form-control editor']) !!}
											</div>
										</div>
									</div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="submit" class="btn btn-success">Обновить</button>
			</div>
		</div>

	{!! Form::close() !!}
	
@stop

@section('styles')
	@parent
	<link href="{{ asset('admin/css/bootstrap-switch.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('admin/js/bootstrap-switch.min.js') }}"></script>
    <script>
		// Tabs
		// Store the currently selected tab in the hash value
		$('ul.nav-tabs.settings > li > a').on('shown.bs.tab', function(event){
			var id = $(event.target).attr("href").substr(1);
			window.location.hash = id;
			$('input[name="hash"]').val( id );
		});

		// On load of the page: switch to the currently selected tab
		var hash = window.location.hash;
		$('ul.nav-tabs.settings > li > a[href="' + hash + '"]').tab('show');

		// Languages
		@foreach( $languages as $language )
			$('#lang_{{ $language->code }}').bootstrapSwitch({
				size: 'normal',
				onText: 'Вкл.',
				offText: 'Выкл.',
				@if( $language->code != 'ru' )
					onColor: 'success',
					disabled: false
				@else
					onColor: 'danger',
					disabled: true
				@endif
			});
		@endforeach

		// File input
		$(document).on('change', '.btn-file :file', function(){
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});
		$(document).ready(function(){
			$('.btn-file :file').on('fileselect', function(event, numFiles, label){
				var input = $(this).parents('.input-group').find(':text'),
					log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length )
					input.val(log);
				else
					if( log ) 
						alert(log);
			});
		});
    </script>

    @include('admin.tinymceInit')
@endsection