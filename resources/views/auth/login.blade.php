<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Вход</title>
    <link href="{{ asset('favicon.ico') }}" rel="icon" type="image/x-icon">
    
    @section('styles')
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{ asset('admin/css/login.css') }}">
    @show
</head>
<body>
    <div class="wrapper">
        <a href="{{ asset('/') }}" class="logo">
            <img src="{{ asset('admin/img/logo.svg') }}" alt="" />
        </a>
        
        {!! Form::open(array('class' => 'form-login')) !!}
            
            @if( !$errors->isEmpty() )
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            
            <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required autofocus />
            <input type="password" class="form-control" name="password" placeholder="Пароль" required />      
            <button class="btn btn-lg btn-danger btn-block" type="submit">Войти <span class="glyphicon glyphicon-log-in"></span></button>  
        
        {!! Form::close() !!}
    
    </div>
</body>
</html>