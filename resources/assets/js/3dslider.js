/*
 * jQuery 3DSlider v1.0
 *
 * Copyright 2011, Oleksiy Yevdakov
 * http://www.clear-web-solutions.com
 *
 * Licensed under the GNU LGPL (http://www.gnu.org/licenses/).
*/

(function($){
	
	$.fn.dddSlider = function(params){

		//parameters adjustable on initialization
		var autoplay = false;
		var slideWidth = 960;
		var whr = 5/3; //by default the slide width/height ratio is 5x3 if needed could be changed
		var speed  = 800;
		var speedBtwnSlides = 4500;
		var incP = 60; //percentage increase for the first slide width once it fades out

		//private params
		var slideHeight = 500;
		var element = this;
		var slides = $('.item');
		var currentSlide = 0;
		var zindex = 100;
		var animating = false;
		var t;
		var tempTimer;

		//read params
		if( params )
		{
			if(params.whr!==undefined&&params.whr!==null) whr = params.whr;
			if(params.autoplay!==undefined&&params.autoplay!==null) autoplay = params.autoplay;
			if(params.slideWidth!==undefined&&params.slideWidth!==null){ slideWidth = params.slideWidth; slideHeight=slideWidth/whr;}
			if(params.speed!==undefined&&params.speed!==null) speed = params.speed;
			if(params.speedBtwnSlides!==undefined&&params.speedBtwnSlides!==null) speedBtwnSlides = params.speedBtwnSlides;
			if(params.incP!==undefined&&params.incP!==null)incP = params.incP;
		}

		//you can adjust the strechness of the slider here
		var right1 = parseInt(slideWidth*0.3);
		var top1 = parseInt(slideHeight*0.1);
		var width1 = parseInt(slideWidth*0.8);
		var right2 = parseInt(slideWidth*0.96);
		var top2 = parseInt(slideHeight*0.3);

		//initial slides positioning
		initialize();
		
		//initialize function that creates all the controls and places the initial slides where they have to be placed, adding actions to buttons
		function initialize(){
			
			//set the appropriate zIndex to each slide
			slides.each(function(){
				
				zindex--;
				
				$(this).css({
					'zIndex': zindex
				});
				
				//hide the ones we don't need to display
				if( zindex < 98 )
				{
					$(this).hide();
				}
			});
			
			//display first 2 in the exact positions they should be displayed
			$(slides[0]).css({
				right: '0px',
				top: '0px',
				width: slideWidth
			});
			
			$(slides[1]).css({
				right: right1,
				top: top1,
				width: width1
			});

			//create 1 div to hover the slides, onclick would initiate the moving to particular slide and stopping the animation
			if( slides.length >= 3 ) 
			{
				$(element).append('<div class="slide-next"></div>');
				
				if( autoplay )
					$(element).append('<div class="slide-autoplay pause"></div>');
				else
					$(element).append('<div class="slide-autoplay play"></div>');
			}
			
			// Next slide
			$('.slide-next').bind('click', function(){
				
				if( autoplay )
				{
					clearTimeout(t);
					clearTimeout(tempTimer);
					
					autoplay = false;
					
					$('.slide-autoplay').removeClass('pause').addClass('play');
				}

				sliderControl();
			});
			
			// Slide autoplay
			$('.slide-autoplay').bind('click', function(){
				
				if( $(this).hasClass('pause') )
				{
					autoplay = false;
					
					$(this).removeClass('pause').addClass('play');
					
					clearTimeout(t);
					clearTimeout(tempTimer);
				}
				else
				{
					autoplay = true;
					
					$(this).removeClass('play').addClass('pause');

					sliderControl();
				}
			});

			//playing the slides
			if( autoplay ) 
				sliderControl(); 
			
		};//end of initialize()

		function sliderControl() {
			
			var slide0W = parseInt(slideWidth*(100+incP)/100);
			var slide0T = parseInt((slideHeight*(100+incP)/100-slideHeight)/2);
			var slide1 = currentSlide;
			var slide2 = currentSlide+1==slides.length?0:currentSlide+1;
			var slide3 = slide2+1==slides.length?0:slide2+1;
			
			if( !animating )
			{
				clearTimeout(tempTimer);
				animating = true;
				zindex--;
				
				//position the last(new) slide where it should be
				$(slides[slide3]).css({
					right: right2,
					top: top2,
					width: '0px',
					zIndex: zindex
				});
				
				$(slides[slide3]).show();
					
				$(slides[slide1]).animate({
					width: slide0W,
					right: -slideWidth,
					top: -slide0T,
					opacity: '0'
				}, speed, function(){
					$(this).css({width: '0px'});
					animating = false;
				});
				
				$(slides[slide2]).animate({
					width: slideWidth,
					right: '0',
					top: '0'
				}, speed);
				
				$(slides[slide3]).animate({
					opacity: '1'
				}, 0);
				
				$(slides[slide3]).animate({
					width: width1,
					right: right1,
					top: top1
				}, speed);
				
				currentSlide = slide2;

				// Changing classes
				slides.each(function(item){
					if( item == currentSlide ) 
					{
						$(this).removeClass('before');
						$(this).addClass('current');
					}
					else
					{
						$(this).removeClass('current');
						$(this).addClass('before');
					}
				});

				if( autoplay )
					t = setTimeout(sliderControl, speedBtwnSlides);
			}
			else
				tempTimer = setTimeout(sliderControl, speed);
		};//end sliderControl()

	};//end dddSlider
})(jQuery);