<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Templates
Route::pattern('id', '[0-9]+');
Route::pattern('feature_id', '[0-9]+');
Route::pattern('option_id', '[0-9]+');
Route::pattern('page_url', '^((?!master|login|logout).)[^\p{Cyrillic}]+');

// Frontend
Route::group(['namespace' => 'Frontend', 'prefix' => LaravelLocalization::setLocale()], function() {
	
	// Main
	Route::get('/', 'HomeController');

	// Services
	Route::resource('services', 'ServicesController', ['only' => ['index', 'show']]);

	// Products
	Route::resource('products', 'ProductsController', ['only' => ['index', 'store']]);

	// News
	Route::resource('news', 'NewsController', ['only' => ['index', 'show']]);

	// Articles
	Route::resource('articles', 'ArticlesController', ['only' => ['index', 'show']]);

	// Reviews
	Route::resource('reviews', 'ReviewsController', ['only' => ['index', 'store']]);

	// Team
	Route::get('team', 'TeamController');

	// Stock
	Route::resource('stock', 'StockController', ['only' => ['index', 'show']]);

	// Photo gallery
	Route::resource('photo-gallery', 'PhotoGalleryController', ['only' => ['index', 'show']]);
	Route::post('photo-gallery', 'PhotoGalleryController@index');

	// Video gallery
	Route::resource('video-gallery', 'VideoGalleryController', ['only' => ['index', 'show']]);
	Route::post('video-gallery', 'VideoGalleryController@index');

	// Partners
	Route::get('partners', 'PartnersController');

	// Certificates
	Route::get('certificates', 'CertificatesController');

	// Sitemap
	Route::get('sitemap', 'SitemapController');

	// Search
	Route::get('search', 'SearchController@index');

	// Feedback
	Route::post('feedback', 'FeedbacksController');

	// Subscribers
	Route::post('subscribe', 'SubscribersController');

	// Pages
	Route::get('contacts', 'PagesController@getContacts');
	Route::get('price', 'PagesController@getPrice');
	Route::get('about', 'PagesController@getAbout');
	Route::get('schedule', 'PagesController@getSchedule');

	Route::get('/{page_url}', 'PagesController@getIndex');
});

// Basic auth
Route::group(['namespace' => 'Auth'], function() {
	
	// Login
	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@login');
	
	// Logout
	Route::any('logout', 'LoginController@logout')->name('logout');
});

// Dashboard
Route::group(['namespace' => 'Admin', 'prefix' => 'master', 'middleware' => ['auth:web']], function() {

	// Main
	Route::get('/', 'AdminHomeController');

	// Settings
	Route::resource('settings', 'AdminSettingsController', ['only' => ['index', 'store']]);

	// Slider
	Route::resource('slider', 'AdminSliderController', ['only' => ['index', 'store']]);
	Route::delete('slider/destroy', 'AdminSliderController@destroy');
	Route::post('slider/visible', 'AdminSliderController@visible');
	Route::post('slider/sortable', 'AdminSliderController@sortable');

	// Menu
	Route::resource('menu', 'AdminMenuController', ['except' => ['edit', 'destroy']]);
	Route::delete('menu/destroy', 'AdminMenuController@destroy');
	Route::post('menu/rebuild', 'AdminMenuController@rebuild');
	Route::post('menu/visible', 'AdminMenuController@visible');

	// Pages
	Route::resource('pages', 'AdminPagesController', ['except' => ['edit', 'destroy']]);
	Route::delete('pages/destroy', 'AdminPagesController@destroy');

	// Services
	Route::resource('services', 'AdminServicesController', ['except' => ['edit', 'destroy']]);
	Route::delete('services/destroy', 'AdminServicesController@destroy');
	Route::post('services/sortable', 'AdminServicesController@sortable');
	Route::post('services/toggle', 'AdminServicesController@toggle');

	// News
	Route::resource('news', 'AdminNewsController', ['except' => ['edit', 'destroy']]);
	Route::delete('news/destroy', 'AdminNewsController@destroy');
	Route::post('news/visible', 'AdminNewsController@visible');

	// Articles
	Route::resource('articles', 'AdminArticlesController', ['except' => ['edit', 'destroy']]);
	Route::delete('articles/destroy', 'AdminArticlesController@destroy');
	Route::post('articles/visible', 'AdminArticlesController@visible');

	// Team
	Route::resource('team', 'AdminTeamController', ['except' => ['edit', 'destroy']]);
	Route::delete('team/destroy', 'AdminTeamController@destroy');
	Route::post('team/sortable', 'AdminTeamController@sortable');
	Route::post('team/visible', 'AdminTeamController@visible');

	// Stock
	Route::resource('stock', 'AdminStockController', ['except' => ['edit', 'destroy']]);
	Route::delete('stock/destroy', 'AdminStockController@destroy');
	Route::post('stock/visible', 'AdminStockController@visible');

	/* ------------------------------ Photo gallery ----------------------------- */
	// Albums
	Route::resource('photo-gallery-albums', 'AdminPhotoGalleryAlbumsController', ['except' => ['edit', 'destroy']]);
	Route::delete('photo-gallery-albums/destroy', 'AdminPhotoGalleryAlbumsController@destroy');
	Route::post('photo-gallery-albums/sortable', 'AdminPhotoGalleryAlbumsController@sortable');
	Route::post('photo-gallery-albums/toggle', 'AdminPhotoGalleryAlbumsController@toggle');
	// Images
	Route::resource('photo-gallery-images', 'AdminPhotoGalleryImagesController', ['except' => ['index']]);
	Route::post('photo-gallery-images/sortable', 'AdminPhotoGalleryImagesController@sortable');
	Route::post('photo-gallery-images/visible', 'AdminPhotoGalleryImagesController@visible');
	/* -------------------------------------------------------------------------- */

	/* ------------------------------ Video gallery ----------------------------- */
	// Albums
	Route::resource('video-gallery-albums', 'AdminVideoGalleryAlbumsController', ['except' => ['edit', 'destroy']]);
	Route::delete('video-gallery-albums/destroy', 'AdminVideoGalleryAlbumsController@destroy');
	Route::post('video-gallery-albums/sortable', 'AdminVideoGalleryAlbumsController@sortable');
	Route::post('video-gallery-albums/visible', 'AdminVideoGalleryAlbumsController@visible');
	// Items
	Route::resource('video-gallery-items', 'AdminVideoGalleryItemsController', ['except' => ['index']]);
	Route::post('video-gallery-items/sortable', 'AdminVideoGalleryItemsController@sortable');
	Route::post('video-gallery-items/visible', 'AdminVideoGalleryItemsController@visible');
	/* -------------------------------------------------------------------------- */

	// Partners
	Route::resource('partners', 'AdminPartnersController', ['except' => ['edit', 'destroy']]);
	Route::delete('partners/destroy', 'AdminPartnersController@destroy');
	Route::post('partners/sortable', 'AdminPartnersController@sortable');
	Route::post('partners/visible', 'AdminPartnersController@visible');

	// Certificates
	Route::resource('certificates', 'AdminCertificatesController', ['except' => ['edit', 'destroy']]);
	Route::delete('certificates/destroy', 'AdminCertificatesController@destroy');
	Route::post('certificates/sortable', 'AdminCertificatesController@sortable');
	Route::post('certificates/visible', 'AdminCertificatesController@visible');

	/* ---------------------------------- Shop ---------------------------------- */
	// Menu
	Route::get('products-menu', 'AdminBaseController@getProductsMenu');
	// Products
	Route::resource('products', 'AdminProductsController', ['except' => ['edit', 'destroy']]);
	Route::delete('products/destroy', 'AdminProductsController@destroy');
	Route::post('products/sortable', 'AdminProductsController@sortable');
	Route::post('products/toggle', 'AdminProductsController@toggle');
	// Products images
	Route::resource('products-images', 'AdminProductsImagesController', ['only' => ['show', 'update']]);
	Route::delete('products-images/destroy', 'AdminProductsImagesController@destroy');
	Route::post('products-images/visible', 'AdminProductsImagesController@visible');
	Route::post('products-images/sortable', 'AdminProductsImagesController@sortable');
	// Products features
	Route::resource('products-features', 'AdminProductsFeaturesController', ['except' => ['edit', 'destroy']]);
	Route::delete('products-features/destroy', 'AdminProductsFeaturesController@destroy');
	Route::post('products-features/visible', 'AdminProductsFeaturesController@visible');
	Route::post('products-features/sortable', 'AdminProductsFeaturesController@sortable');
	// Products options
	Route::get('products-options/{feature_id}', 'AdminProductsOptionsController@getIndex');
	Route::post('products-options/{feature_id}', 'AdminProductsOptionsController@postIndex');
	Route::get('products-options/{feature_id}/add', 'AdminProductsOptionsController@getAdd');
	Route::post('products-options/{feature_id}/add', 'AdminProductsOptionsController@postAdd');
	Route::get('products-options/{feature_id}/edit/{option_id}', 'AdminProductsOptionsController@getEdit');
	Route::post('products-options/{feature_id}/edit/{option_id}', 'AdminProductsOptionsController@postEdit');
	// Products features options
	Route::resource('products-features-options', 'AdminProductsFeaturesOptionsController', ['only' => ['show', 'update']]);
	// Products orders
	Route::resource('products-orders', 'AdminProductsOrdersController', ['only' => ['index', 'store']]);
	Route::post('products-orders/visible', 'AdminProductsOrdersController@visible');
	/* -------------------------------------------------------------------------- */

	// Reviews
	Route::resource('reviews', 'AdminReviewsController', ['only' => ['index', 'store', 'show', 'update']]);
	Route::post('reviews/visible', 'AdminReviewsController@visible');

	// Subscribers
	Route::resource('subscribers', 'AdminSubscribersController', ['only' => ['index', 'create', 'store']]);
	Route::delete('subscribers/destroy', 'AdminSubscribersController@destroy');
	Route::post('subscribers/visible', 'AdminSubscribersController@visible');

	// Feedbacks
	Route::resource('feedbacks', 'AdminFeedbacksController', ['only' => ['index', 'store']]);
	Route::post('feedbacks/visible', 'AdminFeedbacksController@visible');

	// Users
	Route::resource('users', 'AdminUsersController', ['except' => ['edit', 'destroy']]);
	Route::delete('users/destroy', 'AdminUsersController@destroy');
});