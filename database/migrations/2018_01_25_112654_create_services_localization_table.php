<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesLocalizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('as_services_localization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('as_services')->onDelete('cascade')->onUpdate('cascade');
            $table->string('lang', 2);
            $table->foreign('lang')->references('code')->on('as_languages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->text('annotation');
            $table->text('body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('as_services_localization');
    }
}
