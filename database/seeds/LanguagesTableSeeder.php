<?php

use Illuminate\Database\Seeder;

use App\Models\Language;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [(object)['code' => 'ru', 'name' => 'Русский', 'alias' => 'rus'], (object)['code' => 'ua', 'name' => 'Українська', 'alias' => 'ukr'], (object)['code' => 'en', 'name' => 'English', 'alias' => 'eng']];

		foreach ($languages as $key => $value) {
			
			Language::insert([
				'code'  => $value->code,
	            'name'  => $value->name,
	            'alias' => $value->alias
	        ]);
		}
    }
}
